#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int N;
  int sum;
  int sum_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
  N = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (int i = 0; i < N; i++) 
    sum_s += i + 1;
  
#pragma omp parallel for reduction(+: sum)
  for (int i = 0; i < N; i++) 
    sum += i + 1;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
