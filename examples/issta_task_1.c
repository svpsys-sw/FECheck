#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern unsigned int __VERIFIER_nondet_uint();
extern int __VERIFIER_nondet_int();
int _size_st0;

int main()
{
  int x;
  int N;
  unsigned int *a;
  _size_st0 = __VERIFIER_nondet_int();
  a = ((unsigned int *)(malloc(_size_st0 * sizeof(unsigned int ))));
  int x_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_uint();
  }
  N = __VERIFIER_nondet_int();
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  for (int i = 0; i < N; i++) 
    if (a[i] % ((unsigned int )2) == ((unsigned int )1)) 
      x_s++;
  
#pragma omp parallel for reduction(+: x)
  for (int i = 0; i < N; i++) 
    x += a[i] % ((unsigned int )2);
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
{
    free(a);
  }
  return 0;
}
