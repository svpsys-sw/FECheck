#include <assert.h> 
extern unsigned char __VERIFIER_nondet_uchar();

int main()
{
  unsigned char N;
  N = __VERIFIER_nondet_uchar();
// (First) sequential code segment
  int j;
  int sum_s;
  sum_s = ((int )N);
  for (j = ((int )N) - 1; j >= 0; j--) {
    sum_s += j;
  }
  int i;
  int sum;
  sum = 0;
  
#pragma omp parallel for reduction (+:sum)
  for (i = 1; i <= ((int )N); i++) {
    sum += i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
