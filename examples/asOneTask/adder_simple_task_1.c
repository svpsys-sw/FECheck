#include <assert.h> 
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);

int main()
{
// (First) sequential code segment
  int sum_s = 0;
  int N_s = __VERIFIER_nondet_int_s(0);
  for (int i = 0; i < N_s; i++) 
    sum_s += i + 1;
  int sum = 0;
  int N = __VERIFIER_nondet_int_s(1);
  
#pragma omp parallel for reduction(+: sum)
  for (int i = 0; i < N; i++) 
    sum += i + 1;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}
