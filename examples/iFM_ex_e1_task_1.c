#include <assert.h> 
extern unsigned short __VERIFIER_nondet_ushort();

int main()
{
  int sum;
  int i;
  unsigned short N;
  int sum_s;
  N = __VERIFIER_nondet_ushort();
// (First) sequential code segment
  sum_s = ((int )N) * (((int )N) + 1) / 2;
  sum = 0;
  
#pragma omp parallel for reduction (+:sum)
  for (i = 1; i <= ((int )N); i++) {
    sum += i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
