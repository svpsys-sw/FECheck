# Framework for Equivalence Checking of Parallelized Code
Our framework **FECheck** provides equivalence checking methods for parallelized code.
More concretely, FECheck supports the analysis of sequential C programs and parallel C programs that are parallelized with the OpenMP API.
Currently, FECheck incorporates two checking methods: PEQcheck and PatEC.

**PEQcheck** is general purpose equivalence checking approach that encodes the equivalence checking problem into program verification tasks, which can be verified by an external verifier. If the external verifier does not detect an assertion violation, equivalence is guaranteed.

**PatEC** is a specific approach to verify parallelizations of for loops. It is designed to check parallelizations applying the DoAll or Reduction pattern, which aim at parallelization solutions for embarrassingly parallel for loops.

Both approaches analyze equivalence on the level of code segments, i.e., subprograms in the form of sequences of statements (including blocks).

## Installation
We tested our FECheck framework on an Ubuntu 18.04 and Ubuntu 20.04.

 1. Install the [ROSE compiler](https://github.com/rose-compiler/rose/wiki/How-to-Set-Up-ROSE "Wiki page explaining ROSE compiler installation."). We installed ROSE Core via apt and selected the release version, getting us ROSE Core in version 0.9.13.0.0 for Ubuntu 18.04 and ROSE Core in version 0.11.33.0.1 for Ubuntu 20.04. 
To compare our FECheck framework, especially the PatEC method, against AutoPar, we also installed the ROSE tools. However, ROSE tools is not required for our FECheck framework.

 2. Get the SMT solver [Z3](https://github.com/Z3Prover/z3, "Z3Prover project on GitHub"). We downloaded and unzipped the Ubuntu release. We tested our FECheck framework with Z3 versions 4.8.8 and 4.8.10.

 3. Set the `Z3PATH` in the [`Makefile`](Makefile) to your Z3 folder.

 4. Build `FECheck` with `make`. Execute the following command from the folder that contains this file.
    make

 5. Our FECheck framework generates the verification tasks for the PEQcheck approach, i.e., it generates the programs that encode the functional equivalence of sequential and parallelized code segments. To run the complete PEQcheck approach, you also need a verifier that inspects the generated programs. We use the verifier CIVL to check OpenMP parallelizations and the verifier CPAchecker when analyzing the equivalence of two sequential programs. Next, we describe how to get and set up them.

### Getting and Setting Up CIVL
1. Download the framework [CIVL](https://vsl.cis.udel.edu/civl/index.html). 
2. Edit the civl file in the `bin` directory to replace the absolute path to the CIVL jar with the correct absolute path on your system.
3. Add Z3 and CIVL to your PATH.

For both Z3 and CIVL, you can e.g., add the following code to your `.profile` file in the home folder. PATH_TO_BIN is a place holder for the actual absolute path.
    if [ -d "PATH_TO_BIN" ] ; then
      PATH="PATH_TO_BIN:$PATH"
    fi
4. Run civl config in your home directory.

CIVL requires at least a Java 8 JVM. We used CIVL version 1.20_5259 with an OpenJKD 11, which we installed via apt. 

### Getting CPAchecker
Download the verification framework [CPAchecker](https://cpachecker.sosy-lab.org/download.php). We use CPAchecker's binary release for Unix systems. We tested versions 1.9.1 and 2.0.

To run the tested CPAchecker versions, you require a Java 11 or higher. We installed the OpenJDK 11 via apt.

## Checking Functional Equivalence

### Preparing Programs
Before you can analyze equivalence, you need to declare the code segments that should be checked for equivalence. To this end, you need to add `#pragma scope_i` and `#pragma epocs_i` to mark the beginning and end of the ith code segment in both input files, either the sequential and parallelized program or the two sequential versions. Our framework requires that code segments are a sequence of statements (including blocks). In addition, our framework assumes that n>=1 code segments are numbered sequentially starting with 1 and ending at n.

### Running PEQcheck
To use the PEQcheck approach, you need to perform two steps.

1. Generate the verification tasks.

    `./FECheck seqProg parProg`
    
The ith generated task is named checker_i.c and can be found in the main folder of FECheck.

2. Verify each generated task progTaskI.

    `civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false nondet_funs.c progTaskI`

#### Using PEQcheck for Checking Functional Equivalence of two Sequential Versions
Using the PEQcheck approach to check equivalence of two sequential versions is similar.
You only need to use another verifier.

1.  Generate the verification tasks.

    `./FECheck seqProgV1 seqProgV2`

2. Verify each generated task progTaskI.

    `$CPAcheckerPath/scripts/cpa.sh -default -spec $CPAcheckerPath/config/specification/Assertion.spc -setprop log.consoleLevel=SEVERE -timelimit 300s -preprocess progTaskI`

### Running PatEC
PatEC considers the pattern applied during parallelization. Currently, PatEC supports the DoAll or Reduction pattern, which aim at parallelization solutions for embarrassingly parallel for loops. To check equivalence of a program parallelized with the DoAll pattern, you need to execute the following command line.

    ./FECheck -type=DOALL seqProg parProg

Similarly, to check a program parallelized with the Reduction pattern, you need to execute the folllowing command line.

    ./FECheck -type=REDUCTION seqProg parProg

In case the loop body is different in the sequential and parallelized program, you also need to verify the equivalence of the loop body. To check equivalence of loop bodies, which must be sequential to apply PatEC, PatEC follows the PEQcheck approach and already generates the verification tasks. To analyze the generated program tasks, you can again use CPAchecker.

    $CPAcheckerPath/scripts/cpa.sh -default -spec $CPAcheckerPath/config/specification/Assertion.spc -setprop log.consoleLevel=SEVERE -timelimit 300s -preprocess progTaskI

### Trouble Shouting
If you see the error message
    ... error while loading shared libraries: libz3.so: cannot open shared object file: No such file or directory
the Z3 library cannot be found, likely because you moved the executable or the library after building the executable or you want to execute our framework from a different folder than the folder of the executable. You can solve this problem temporarily by executing the following commands in your shell, where $Z3_PATH is a substitute for your path to the Z3 installation.

    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$Z3_PATH
    export LD_LIBRARY_PATH

For example, if you unpacked Z3 to /home/z3-4.8.8-x64-ubuntu-16.04/, you need to use /home/z3-4.8.8-x64-ubuntu-16.04/bin for $Z3_PATH.

## Licensing
The FECheck framework itself is licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0) with copyright by Marie-Christine Jakobs. The license for examples (folders dataracebench-1.3.2, ...) is often different. The respective **README** files in the folders with examples detail the licenses of the respective files.
