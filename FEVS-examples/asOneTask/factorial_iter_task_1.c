#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int N;
  N = __VERIFIER_nondet_int();
// (First) sequential code segment
  int t_s = 1;
  int i_s;
  for (i_s = 1; i_s <= N; i_s++) 
    t_s *= i_s;
  int t = 1;
  int i;
  
#pragma omp parallel for reduction(*: t)
  for (i = 1; i <= N; i++) 
    t *= i;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && t_s == t;
  assert(equal);
  return 0;
}
