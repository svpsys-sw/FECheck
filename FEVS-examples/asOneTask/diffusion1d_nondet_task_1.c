#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);
void update();
int init();
void update_s();
int init_s();
double *u;
int _size_st0;
int wstep;
int nsteps;
double k;
int nx;
double *u_s;
int wstep_s;
int nsteps_s;
double k_s;
int nx_s;

int main()
{
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double *)(malloc(_size_st0 * sizeof(double ))));
  u_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  k_s = __VERIFIER_nondet_double();
  k = k_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_s[_f_ct0] = __VERIFIER_nondet_double();
    u[_f_ct0] = u_s[_f_ct0];
  }
// (First) sequential code segment
  int iter_s;
  int notSuccess_s = 0;
  if (!init_s()) 
    notSuccess_s = - 1;
   else {
    for (iter_s = 1; iter_s <= nsteps_s; iter_s++) {
      update_s();
    }
  }
  int iter;
  int notSuccess = 0;
  if (!init()) 
    notSuccess = - 1;
   else {
    for (iter = 1; iter <= nsteps; iter++) {
      update();
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_s[_f_ct0] == u[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && notSuccess_s == notSuccess;
  assert(equal);
{
    free(u_s);
    free(u);
  }
  return 0;
}

int init_s()
{
{
    int i;
    nx_s = __VERIFIER_nondet_int_s(0);
    k_s = __VERIFIER_nondet_double_s(0);
    nsteps_s = __VERIFIER_nondet_int_s(0);
    wstep_s = __VERIFIER_nondet_int_s(0);
    if (nx_s < 2 || k_s <= ((double )0) || k_s >= 0.5 || nsteps_s < 1 || wstep_s < 1 || wstep_s > nsteps_s) 
      return - 1;
    u_s = ((double *)(malloc(((unsigned long )nx_s) * sizeof(double ))));
    if (!u_s) 
      return - 1;
    for (i = 0; i < nx_s; i++) {
      u_s[i] = __VERIFIER_nondet_double_s(0);
    }
    return 0;
  }
}

void update_s()
{
{
    int i;
    int size = nx_s;
    double u_new[size];
    for (i = 1; i < nx_s - 1; i++) 
      u_new[i] = u_s[i] + k_s * (u_s[i + 1] + u_s[i - 1] - ((double )2) * u_s[i]);
    for (i = 1; i < nx_s - 1; i++) 
      u_s[i] = u_new[i];
  }
}

int init()
{
{
    int i;
    nx = __VERIFIER_nondet_int_s(1);
    k = __VERIFIER_nondet_double_s(1);
    nsteps = __VERIFIER_nondet_int_s(1);
    wstep = __VERIFIER_nondet_int_s(1);
    if (nx < 2 || k <= ((double )0) || k >= 0.5 || nsteps < 1 || wstep < 1 || wstep > nsteps) 
      return - 1;
    u = ((double *)(malloc(((unsigned long )nx) * sizeof(double ))));
    if (!u) 
      return - 1;
    
#pragma omp parallel for
    for (i = 0; i < nx; i++) {
      u[i] = __VERIFIER_nondet_double_s(1);
    }
    return 0;
  }
}

void update()
{
{
    int i;
    int size = nx;
    double u_new[size];
    
#pragma omp parallel for
    for (i = 1; i < nx - 1; i++) 
      u_new[i] = u[i] + k * (u[i + 1] + u[i - 1] - ((double )2) * u[i]);
    
#pragma omp parallel for
    for (i = 1; i < nx - 1; i++) 
      u[i] = u_new[i];
  }
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
