// OpenMP parallelization of matmat_seq.c
/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

extern double __VERIFIER_nondet_double();


int main(int argc, char* argv[]) {
  int L = atoi(argv[1]);
  int M = atoi(argv[2]);
  int N = atoi(argv[3]);

  double A[L][M];
  double B[M][N];
  double C[L][N];
  #pragma scope_1

  int i, j, k;

  
  for (i = 0; i < L; i++)
    for (j = 0; j < M; j++)
      A[i][j] = __VERIFIER_nondet_double();
  for (i = 0; i < M; i++)
    for (j = 0; j < N; j++)
      B[i][j] = __VERIFIER_nondet_double();


  #pragma omp parallel for
  for (i = 0; i < L; i++) 
    #pragma omp parallel for
    for (j = 0; j < N; j++) {
      double x = 0.0;
      #pragma omp parallel for reduction(+:x)
      for (k = 0; k < M; k++)
	x += A[i][k] * B[k][j]; 
      C[i][j] = x;
    }
  #pragma epocs_1

  for (i = 0; i < L; i++) {
    for (j = 0; j < N; j++)
      printf("%f ", C[i][j]);
    printf("\n");
  }

  return 0;
}
