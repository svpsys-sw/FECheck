#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern long __VERIFIER_nondet_long();
extern int __VERIFIER_nondet_int();
void update();
void initData();
void initialization();
void update_s();
void initData_s();
void initialization_s();
double **u_next;
int _size_st3;
int _size_st2;
double **u_curr;
int _size_st1;
int _size_st0;
double k;
double initTemp;
double constTemp;
int wstep;
int nsteps;
long ny;
long nx;
double **u_next_s;
double **u_curr_s;
double k_s;
double initTemp_s;
double constTemp_s;
int wstep_s;
int nsteps_s;
long ny_s;
long nx_s;

int main()
{
  _size_st3 = __VERIFIER_nondet_int();
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double **)(malloc(_size_st2 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u_next[_f_ct0] = ((double *)(malloc(_size_st3 * sizeof(double ))));
  }
  _size_st1 = __VERIFIER_nondet_int();
  _size_st0 = __VERIFIER_nondet_int();
  u_curr = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  u_next_s = ((double **)(malloc(_size_st2 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u_next_s[_f_ct0] = ((double *)(malloc(_size_st3 * sizeof(double ))));
  }
  u_curr_s = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr_s[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  nx_s = __VERIFIER_nondet_long();
  nx = nx_s;
  ny_s = __VERIFIER_nondet_long();
  ny = ny_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  constTemp_s = __VERIFIER_nondet_double();
  constTemp = constTemp_s;
  initTemp_s = __VERIFIER_nondet_double();
  initTemp = initTemp_s;
  k_s = __VERIFIER_nondet_double();
  k = k_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      u_curr_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_curr[_f_ct0][_f_ct1] = u_curr_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st3; _f_ct1++) {
      u_next_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_next[_f_ct0][_f_ct1] = u_next_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  int i_s;
  int j_s;
  initialization_s();
  initData_s();
  for (i_s = 0; i_s <= nsteps_s; i_s++) {
    update_s();
  }
  int i;
  int j;
  initialization();
  initData();
  for (i = 0; i <= nsteps; i++) {
    update();
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && ny_s == ny;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  equal = equal && constTemp_s == constTemp;
  assert(equal);
  equal = 1;
  equal = equal && initTemp_s == initTemp;
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      equal = equal && u_curr_s[_f_ct0][_f_ct1] == u_curr[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st3; _f_ct1++) {
      equal = equal && u_next_s[_f_ct0][_f_ct1] == u_next[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_curr_s[_f_ct0]);
    }
    free(u_curr_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
      free(u_next_s[_f_ct0]);
    }
    free(u_next_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_curr[_f_ct0]);
    }
    free(u_curr);
    for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
      free(u_next[_f_ct0]);
    }
    free(u_next);
  }
  return 0;
}

void initialization_s()
{
{
    nsteps_s = 150;
    wstep_s = 10;
    nx_s = ((long )8);
    ny_s = ((long )8);
    constTemp_s = 0.0;
    initTemp_s = 100.0;
    k_s = 0.13;
    printf("Diffusion2d with k=%f, nx=%ld, ny=%ld, nsteps=%d, wstep=%d\n",k_s,nx_s,ny_s,nsteps_s,wstep_s);
    u_curr_s = ((double **)(malloc(((unsigned long )(ny_s + ((long )2))) * sizeof(double *))));
    u_next_s = ((double **)(malloc(((unsigned long )(ny_s + ((long )2))) * sizeof(double *))));
    for (int i = 0; ((long )i) < ny_s + ((long )2); i++) {
      u_curr_s[i] = ((double *)(malloc(((unsigned long )(nx_s + ((long )2))) * sizeof(double ))));
      u_next_s[i] = ((double *)(malloc(((unsigned long )(nx_s + ((long )2))) * sizeof(double ))));
    }
  }
}

void initData_s()
{
{
    for (int i = 0; ((long )i) < ny_s + ((long )2); i++) 
      for (int j = 0; ((long )j) < nx_s + ((long )2); j++) 
        if (i == 0 || j == 0 || ((long )i) == ny_s + ((long )1) || ((long )j) == nx_s + ((long )1)) 
          u_next_s[i][j] = u_curr_s[i][j] = constTemp_s;
         else 
          u_curr_s[i][j] = initTemp_s;
  }
}

void update_s()
{
{
    double **tmp;
    for (int i = 1; ((long )i) < ny_s + ((long )1); i++) 
      for (int j = 1; ((long )j) < nx_s + ((long )1); j++) 
        u_next_s[i][j] = u_curr_s[i][j] + k_s * (u_curr_s[i + 1][j] + u_curr_s[i - 1][j] + u_curr_s[i][j + 1] + u_curr_s[i][j - 1] - ((double )4) * u_curr_s[i][j]);
// swap two pointers
    tmp = u_curr_s;
    u_curr_s = u_next_s;
    u_next_s = tmp;
  }
}

void initialization()
{
{
    nsteps = 150;
    wstep = 10;
    nx = ((long )8);
    ny = ((long )8);
    constTemp = 0.0;
    initTemp = 100.0;
    k = 0.13;
    printf("Diffusion2d with k=%f, nx=%ld, ny=%ld, nsteps=%d, wstep=%d\n",k,nx,ny,nsteps,wstep);
    u_curr = ((double **)(malloc(((unsigned long )(ny + ((long )2))) * sizeof(double *))));
    u_next = ((double **)(malloc(((unsigned long )(ny + ((long )2))) * sizeof(double *))));
    for (int i = 0; ((long )i) < ny + ((long )2); i++) {
      u_curr[i] = ((double *)(malloc(((unsigned long )(nx + ((long )2))) * sizeof(double ))));
      u_next[i] = ((double *)(malloc(((unsigned long )(nx + ((long )2))) * sizeof(double ))));
    }
  }
}

void initData()
{
{
    
#pragma omp parallel for
    for (int i = 0; ((long )i) < ny + ((long )2); i++) {
      
#pragma omp parallel for
      for (int j = 0; ((long )j) < nx + ((long )2); j++) 
        if (i == 0 || j == 0 || ((long )i) == ny + ((long )1) || ((long )j) == nx + ((long )1)) 
          u_next[i][j] = u_curr[i][j] = constTemp;
         else 
          u_curr[i][j] = initTemp;
    }
  }
}

void update()
{
{
    double **tmp;
    
#pragma omp parallel for
    for (int i = 1; ((long )i) < ny + ((long )1); i++) {
      
#pragma omp parallel for
      for (int j = 1; ((long )j) < nx + ((long )1); j++) 
        u_next[i][j] = u_curr[i][j] + k * (u_curr[i + 1][j] + u_curr[i - 1][j] + u_curr[i][j + 1] + u_curr[i][j - 1] - ((double )4) * u_curr[i][j]);
    }
    tmp = u_curr;
    u_curr = u_next;
    u_next = tmp;
  }
}
