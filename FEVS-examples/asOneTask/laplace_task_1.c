#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double jacobi(double epsilon);
void initdata();
double jacobi_s(double epsilon);
void initdata_s();
double (*new)[10];
int _size_st1;
double (*old)[10];
int _size_st0;
double (*new_s)[10];
double (*old_s)[10];

int main()
{
  _size_st1 = __VERIFIER_nondet_int();
  new = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  _size_st0 = __VERIFIER_nondet_int();
  old = ((double (*)[10])(malloc(_size_st0 * sizeof(double [10]))));
  new_s = ((double (*)[10])(malloc(_size_st1 * sizeof(double [10]))));
  old_s = ((double (*)[10])(malloc(_size_st0 * sizeof(double [10]))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      old_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      old[_f_ct0][_f_ct1] = old_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      new_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      new[_f_ct0][_f_ct1] = new_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  initdata_s();
  double error_s = jacobi_s(0.000001);
  initdata();
  double error = jacobi(0.000001);
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && old_s[_f_ct0][_f_ct1] == old[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 10; _f_ct1++) {
      equal = equal && new_s[_f_ct0][_f_ct1] == new[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  equal = equal && error_s == error;
  assert(equal);
{
    free(old_s);
    free(new_s);
    free(old);
    free(new);
  }
  return 0;
}

void initdata_s()
{
{
    int row;
    int col;
    for (row = 1; row < 10 - 1; row++) 
      for (col = 1; col < 10 - 1; col++) 
        new_s[row][col] = old_s[row][col] = ((double )0);
    for (col = 0; col < 10; col++) {
      new_s[0][col] = old_s[0][col] = ((double )10);
      new_s[10 - 1][col] = old_s[10 - 1][col] = ((double )10);
    }
  }
}

double jacobi_s(double epsilon)
{
{
    double error = epsilon;
    int row;
    int col;
    int time;
    double (*tmp)[10];
    time = 0;
    while(error >= epsilon){
      error = 0.0;
      for (row = 1; row < 10 - 1; row++) {
        for (col = 1; col < 10 - 1; col++) {
          new_s[row][col] = (old_s[row - 1][col] + old_s[row + 1][col] + old_s[row][col - 1] + old_s[row][col + 1]) * 0.25;
          error += (old_s[row][col] - new_s[row][col]) * (old_s[row][col] - new_s[row][col]);
        }
      }
      time++;
// output time, new, error
      tmp = new_s;
      new_s = old_s;
      old_s = tmp;
    }
    return error;
  }
}

void initdata()
{
{
    int row;
    int col;
    
#pragma omp parallel for
    for (row = 1; row < 10 - 1; row++) {
      
#pragma omp parallel for
      for (col = 1; col < 10 - 1; col++) 
        new[row][col] = old[row][col] = ((double )0);
    }
    
#pragma omp parallel for
    for (col = 0; col < 10; col++) {
      new[0][col] = old[0][col] = ((double )10);
      new[10 - 1][col] = old[10 - 1][col] = ((double )10);
    }
  }
}

double jacobi(double epsilon)
{
{
    double error = epsilon;
    int row;
    int col;
    int time;
    double (*tmp)[10];
    time = 0;
    while(error >= epsilon){
      error = 0.0;
      
#pragma omp parallel for private(col) reduction(+: error)
      for (row = 1; row < 10 - 1; row++) {
        for (col = 1; col < 10 - 1; col++) {
          new[row][col] = (old[row - 1][col] + old[row + 1][col] + old[row][col - 1] + old[row][col + 1]) * 0.25;
          error += (old[row][col] - new[row][col]) * (old[row][col] - new[row][col]);
        }
      }
      time++;
      tmp = new;
      new = old;
      old = tmp;
    }
    return error;
  }
}
