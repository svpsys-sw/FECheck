#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);
void update(int time);
int init();
void update_s(int time);
int init_s();
double **u;
int _size_st1;
int _size_st0;
int wstep;
int nsteps;
double k;
int ny;
int nx;
double **u_s;
int wstep_s;
int nsteps_s;
double k_s;
int ny_s;
int nx_s;

int main()
{
  _size_st1 = __VERIFIER_nondet_int();
  _size_st0 = __VERIFIER_nondet_int();
  u = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  u_s = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_s[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  ny_s = __VERIFIER_nondet_int();
  ny = ny_s;
  k_s = __VERIFIER_nondet_double();
  k = k_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      u_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u[_f_ct0][_f_ct1] = u_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  int iter_s;
  int notSuccess_s = 0;
  if (!init_s()) 
    notSuccess_s = - 1;
   else {
    for (iter_s = 1; iter_s <= nsteps_s; iter_s++) {
      update_s(iter_s);
    }
  }
  int iter;
  int notSuccess = 0;
  if (!init()) 
    notSuccess = - 1;
   else {
    for (iter = 1; iter <= nsteps; iter++) {
      update(iter);
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && ny_s == ny;
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      equal = equal && u_s[_f_ct0][_f_ct1] == u[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  equal = equal && notSuccess_s == notSuccess;
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_s[_f_ct0]);
    }
    free(u_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u[_f_ct0]);
    }
    free(u);
  }
  return 0;
}

int init_s()
{
{
    int i;
    int j;
    nx_s = __VERIFIER_nondet_int_s(0);
    ny_s = __VERIFIER_nondet_int_s(0);
    k_s = __VERIFIER_nondet_double_s(0);
    nsteps_s = __VERIFIER_nondet_int_s(0);
    wstep_s = __VERIFIER_nondet_int_s(0);
    if (nx_s < 1 || ny_s < 1 || k_s <= ((double )0) || k_s >= 0.5 || nsteps_s < 1 || wstep_s < 1 || wstep_s > nsteps_s) 
      return - 1;
    u_s = ((double **)(malloc(sizeof(double *) * ((unsigned long )ny_s))));
    for (i = 0; i < ny_s; i++) 
      u_s[i] = ((double *)(malloc(sizeof(double ) * ((unsigned long )nx_s))));
    for (i = 0; i < ny_s; i++) {
      for (j = 0; j < nx_s; j++) 
        u_s[i][j] = __VERIFIER_nondet_double_s(0);
    }
    return 0;
  }
}

void update_s(int time)
{
{
    int i;
    int j;
    int sizex = nx_s;
    int sizey = ny_s;
    double u_new[sizey][sizex];
    for (i = 1; i < ny_s - 1; i++) 
      for (j = 1; j < nx_s - 1; j++) 
        u_new[i][j] = u_s[i][j] + k_s * (u_s[i + 1][j] + u_s[i - 1][j] + u_s[i][j + 1] + u_s[i][j - 1] - ((double )4) * u_s[i][j]);
    for (i = 1; i < ny_s - 1; i++) 
      memcpy((void *)(&u_s[i][1]),(const void *)(&u_new[i][1]),sizeof(double ) * ((unsigned long )(nx_s - 2)));
  }
}

int init()
{
{
    int i;
    int j;
    nx = __VERIFIER_nondet_int_s(1);
    ny = __VERIFIER_nondet_int_s(1);
    k = __VERIFIER_nondet_double_s(1);
    nsteps = __VERIFIER_nondet_int_s(1);
    wstep = __VERIFIER_nondet_int_s(1);
    if (nx < 1 || ny < 1 || k <= ((double )0) || k >= 0.5 || nsteps < 1 || wstep < 1 || wstep > nsteps) 
      return - 1;
    u = ((double **)(malloc(sizeof(double *) * ((unsigned long )ny))));
    for (i = 0; i < ny; i++) 
      u[i] = ((double *)(malloc(sizeof(double ) * ((unsigned long )nx))));
    
#pragma omp parallel for
    for (i = 0; i < ny; i++) {
      
#pragma omp parallel for
      for (j = 0; j < nx; j++) 
        u[i][j] = __VERIFIER_nondet_double_s(1);
    }
    return 0;
  }
}

void update(int time)
{
{
    int i;
    int j;
    int sizex = nx;
    int sizey = ny;
    double u_new[sizey][sizex];
    
#pragma omp parallel for
    for (i = 1; i < ny - 1; i++) {
      
#pragma omp parallel for
      for (j = 1; j < nx - 1; j++) 
        u_new[i][j] = u[i][j] + k * (u[i + 1][j] + u[i - 1][j] + u[i][j + 1] + u[i][j - 1] - ((double )4) * u[i][j]);
    }
    for (i = 1; i < ny - 1; i++) 
      memcpy((void *)(&u[i][1]),(const void *)(&u_new[i][1]),sizeof(double ) * ((unsigned long )(nx - 2)));
  }
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
