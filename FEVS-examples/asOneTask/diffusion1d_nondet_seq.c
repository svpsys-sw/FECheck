// modification of diffusion1d_spec_io.c from FEVS, replaced file I/O input by __VERIFER_nondet_int() calls and added pragma statement to mark relevant code segments for functional equivalence checking

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
#include <stdlib.h>

extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();

/* Parameters: These are defined at the beginning of the input file:
 *
 *      nx = number of points in x direction, including endpoints
 *       k = D*dt/(dx*dx) 
 *  nsteps = number of time steps
 *   wstep = write frame every this many steps
 *
 * Compiling with the flag -DDEBUG will also cause the data to be written
 * to a sequence of plain text files.
 */

/* Global variables */
int nx = -1;              /* number of discrete points including endpoints */
double k = -1;            /* D*dt/(dx*dx) */
int nsteps = -1;          /* number of time steps */
int wstep = -1;           /* write frame every this many time steps */
double *u;                /* temperature function */




/* init: initializes global variables.  read nx, k, nsteps, u*/
int init() {

  int i;

  nx = __VERIFIER_nondet_int();
  k = __VERIFIER_nondet_double();
  nsteps = __VERIFIER_nondet_int();
  wstep = __VERIFIER_nondet_int();

  if (nx<2 || k<=0 || k>=0.5 || nsteps<1 || wstep<1 || wstep>nsteps)
    return -1;

  u = (double*)malloc(nx*sizeof(double));
  if(!u)
    return -1;
  for (i = 0; i < nx; i++) {
   u[i] = __VERIFIER_nondet_double(); 
  }
  return 0;
}


/* updates u for next time step. */
void update() {
  int i;
  int size = nx;
  double u_new[size];

  for (i = 1; i < nx-1; i++)
    u_new[i] =  u[i] + k*(u[i+1] + u[i-1] -2*u[i]);
  for (i = 1; i < nx-1; i++)
    u[i] = u_new[i];
}

/* main: executes simulation, creates one output file for each time
 * step */
int main() {
  #pragma scope_1
  int iter;
  int notSuccess = 0;

  if(!init())
    notSuccess = -1;
  else
  {
    for (iter = 1; iter <= nsteps; iter++) {
      update();
    }
  }
  #pragma epocs_1
  if(notSuccess)
    return notSuccess;
  free(u);
  return 0;
}
