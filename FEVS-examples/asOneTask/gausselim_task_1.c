#include <assert.h> 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();
double __VERIFIER_nondet_double_s(int __PEC_turn);
void gausselim(double *matrix,int numRows,int numCols,int debug);
void printMatrix(char *message,double *matrix,int numRows,int numCols);
void gausselim_s(double *matrix,int numRows,int numCols,int debug);
void printMatrix_s(char *message,double *matrix,int numRows,int numCols);
int _size_st0;

int main()
{
  int debug;
  double *matrix;
  _size_st0 = __VERIFIER_nondet_int();
  matrix = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int k;
  int numCols;
  int numRows;
  double *matrix_s;
  matrix_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int k_s;
  numRows = __VERIFIER_nondet_int();
  numCols = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    matrix_s[_f_ct0] = __VERIFIER_nondet_double();
    matrix[_f_ct0] = matrix_s[_f_ct0];
  }
  debug = __VERIFIER_nondet_int();
// (First) sequential code segment
  matrix_s = ((double *)(malloc(((unsigned long )(numRows * numCols)) * sizeof(double ))));
  for (k_s = 0; k_s < numRows * numCols; k_s++) {
    matrix_s[k_s] = __VERIFIER_nondet_double_s(0);
  }
  printMatrix_s("Original matrix:\n",matrix_s,numRows,numCols);
  gausselim_s(matrix_s,numRows,numCols,debug);
  matrix = ((double *)(malloc(((unsigned long )(numRows * numCols)) * sizeof(double ))));
  for (k = 0; k < numRows * numCols; k++) {
    matrix[k] = __VERIFIER_nondet_double_s(1);
  }
  printMatrix("Original matrix:\n",matrix,numRows,numCols);
  gausselim(matrix,numRows,numCols,debug);
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && matrix_s[_f_ct0] == matrix[_f_ct0];
  }
  assert(equal);
{
    free(matrix_s);
    free(matrix);
  }
  return 0;
}

void printMatrix_s(char *message,double *matrix,int numRows,int numCols)
{
{
    int k;
    printf("%s",message);
    for (k = 0; k < numRows * numCols; k++) {
      printf("%lf ",matrix[k]);
      if ((k + 1) % numCols == 0) {
        printf("\n");
      }
    }
    printf("\n");
  }
}

void gausselim_s(double *matrix,int numRows,int numCols,int debug)
{
{
    int top = 0;
// index of current top row
    int col = 0;
// index of current left column
    int pivotRow = 0;
// index of row containing the pivot
    double pivot = 0.0;
// the value of the pivot
    int i = 0;
// loop variable over rows of matrix
    int j = 0;
// loop variable over columns of matrix
    double tmp = 0.0;
// temporary double variable
    for (top = col = 0; top < numRows && col < numCols; (top++ , col++)) {
/* At this point we know that the submatarix consisting of the
     * first top rows of A is in reduced row-echelon form.  We will
     * now consider the submatrix B consisting of the remaining rows.
     * We know, additionally, that the first col columns of B are all
     * zero.
     */
      if (debug) 
        printf("Top: %d\n\n",top);
/* Step 1: Locate the leftmost column of B that does not consist
     * entirely of zeros, if one exists.  The top nonzero entry of
     * this column is the pivot. */
      pivot = 0.0;
      for (; col < numCols; col++) {
        for (pivotRow = top; pivotRow < numRows; pivotRow++) {
          pivot = matrix[pivotRow * numCols + col];
          if (pivot) 
            break; 
        }
        if (pivot) 
          break; 
      }
      if (col >= numCols) {
        break; 
      }
/* At this point we are guaranteed that pivot = A[pivotRow,col] is
     * nonzero.  We also know that all the columns of B to the left of
     * col consist entirely of zeros. */
      if (debug) {
        printf("Step 1 result: col=%d, pivotRow=%d, pivot=%lf.\n\n",col,pivotRow,pivot);
      }
/* Step 2: Interchange the top row with the pivot row, if
     * necessary, so that the entry at the top of the column found in
     * Step 1 is nonzero. */
      if (pivotRow != top) {
        for (j = 0; j < numCols; j++) {
          tmp = matrix[top * numCols + j];
          matrix[top * numCols + j] = matrix[pivotRow * numCols + j];
          matrix[pivotRow * numCols + j] = tmp;
        }
      }
      if (debug) {
        printMatrix_s("Step 2 result:\n",matrix,numRows,numCols);
      }
/* At this point we are guaranteed that A[top,col] = pivot is
     * nonzero. Also, we know that (i>=top and j<col) implies
     * A[i,j] = 0. */
/* Step 3: Divide the top row by pivot in order to introduce a
     * leading 1. */
      for (j = col; j < numCols; j++) {
        matrix[top * numCols + j] /= pivot;
      }
      if (debug) {
        printMatrix_s("Step 3 result:\n",matrix,numRows,numCols);
      }
/* At this point we are guaranteed that A[top,col] is 1.0,
     * assuming that floating point arithmetic guarantees that a/a
     * equals 1.0 for any nonzero double a. */
/* Step 4: Add suitable multiples of the top row to all other rows
     * so that all entries above and below the leading 1 become
     * zero. */
      for (i = 0; i < numRows; i++) {
        if (i != top) {
          tmp = matrix[i * numCols + col];
          for (j = col; j < numCols; j++) {
            matrix[i * numCols + j] -= matrix[top * numCols + j] * tmp;
          }
        }
      }
      if (debug) {
        printMatrix_s("Step 4 result:\n",matrix,numRows,numCols);
      }
    }
  }
}

void printMatrix(char *message,double *matrix,int numRows,int numCols)
{
{
    int k;
    printf("%s",message);
    for (k = 0; k < numRows * numCols; k++) {
      printf("%lf ",matrix[k]);
      if ((k + 1) % numCols == 0) {
        printf("\n");
      }
    }
    printf("\n");
  }
}

void gausselim(double *matrix,int numRows,int numCols,int debug)
{
{
    int top = 0;
    int col = 0;
    int pivotRow = 0;
    double pivot = 0.0;
    int i = 0;
    int j = 0;
    double tmp = 0.0;
    for (top = col = 0; top < numRows && col < numCols; (top++ , col++)) {
      if (debug) 
        printf("Top: %d\n\n",top);
      pivot = 0.0;
      for (; col < numCols; col++) {
        for (pivotRow = top; pivotRow < numRows; pivotRow++) {
          pivot = matrix[pivotRow * numCols + col];
          if (pivot) 
            break; 
        }
        if (pivot) 
          break; 
      }
      if (col >= numCols) {
        break; 
      }
      if (debug) {
        printf("Step 1 result: col=%d, pivotRow=%d, pivot=%lf.\n\n",col,pivotRow,pivot);
      }
      if (pivotRow != top) {
        
#pragma omp parallel for private(tmp)
        for (j = 0; j < numCols; j++) {
          tmp = matrix[top * numCols + j];
          matrix[top * numCols + j] = matrix[pivotRow * numCols + j];
          matrix[pivotRow * numCols + j] = tmp;
        }
      }
      if (debug) {
        printMatrix("Step 2 result:\n",matrix,numRows,numCols);
      }
      
#pragma omp parallel for
      for (j = col; j < numCols; j++) {
        matrix[top * numCols + j] /= pivot;
      }
      if (debug) {
        printMatrix("Step 3 result:\n",matrix,numRows,numCols);
      }
      
#pragma omp parallel for private(tmp)
      for (i = 0; i < numRows; i++) {
        if (i != top) {
          tmp = matrix[i * numCols + col];
          
#pragma omp parallel for
          for (j = col; j < numCols; j++) {
            matrix[i * numCols + j] -= matrix[top * numCols + j] * tmp;
          }
        }
      }
      if (debug) {
        printMatrix("Step 4 result:\n",matrix,numRows,numCols);
      }
    }
  }
}

double __VERIFIER_nondet_double_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static double arr[4294967295U];
  double res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_double();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_double();
    }
  }
  return res;
}
