#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int_s(int __PEC_turn);

int main()
{
  int N;
  N = __VERIFIER_nondet_int();
// (First) sequential code segment
  int i_s;
  int sum_s = 0;
  int a_s[N];
  for (i_s = 0; i_s < N; i_s++) 
    a_s[i_s] = __VERIFIER_nondet_int_s(0);
  for (i_s = 0; i_s < N; i_s++) 
    sum_s += a_s[i_s];
  int i;
  int sum = 0;
  int a[N];
  for (i = 0; i < N; i++) 
    a[i] = __VERIFIER_nondet_int_s(1);
  
#pragma omp parallel for reduction(-: sum)
  for (i = 0; i < N; i++) 
    sum -= a[i];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}

int __VERIFIER_nondet_int_s(int __PEC_turn)
{
  static unsigned int indexS = 0U;
  static unsigned int indexP = 0U;
  static int arr[4294967295U];
  int res;
  if (__PEC_turn == 0) {
    res = __VERIFIER_nondet_int();
    if (indexS != 4294967295U) {
      arr[indexS] = res;
      indexS++;
    }
  }
   else {
    if (indexP < indexS) {
      res = arr[indexP];
      indexP++;
    }
     else {
      res = __VERIFIER_nondet_int();
    }
  }
  return res;
}
