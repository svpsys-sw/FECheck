#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
void update();
void write_frame(int time);
void initData();
void initialization();
void update_s();
void write_frame_s(int time);
void initData_s();
void initialization_s();
double k;
double *u_next;
int _size_st2;
double *u_curr;
int _size_st1;
double *u_prev;
int _size_st0;
double c;
int wstep;
int nsteps;
int width_init;
int height_init;
int nx;
int signgam;
double k_s;
double *u_next_s;
double *u_curr_s;
double *u_prev_s;
double c_s;
int wstep_s;
int nsteps_s;
int width_init_s;
int height_init_s;
int nx_s;
int signgam_s;

int main()
{
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double *)(malloc(_size_st2 * sizeof(double ))));
  _size_st1 = __VERIFIER_nondet_int();
  u_curr = ((double *)(malloc(_size_st1 * sizeof(double ))));
  _size_st0 = __VERIFIER_nondet_int();
  u_prev = ((double *)(malloc(_size_st0 * sizeof(double ))));
  u_next_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  u_curr_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  u_prev_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  signgam_s = __VERIFIER_nondet_int();
  signgam = signgam_s;
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  height_init_s = __VERIFIER_nondet_int();
  height_init = height_init_s;
  width_init_s = __VERIFIER_nondet_int();
  width_init = width_init_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  c_s = __VERIFIER_nondet_double();
  c = c_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_prev_s[_f_ct0] = __VERIFIER_nondet_double();
    u_prev[_f_ct0] = u_prev_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr_s[_f_ct0] = __VERIFIER_nondet_double();
    u_curr[_f_ct0] = u_curr_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    u_next_s[_f_ct0] = __VERIFIER_nondet_double();
    u_next[_f_ct0] = u_next_s[_f_ct0];
  }
  k_s = __VERIFIER_nondet_double();
  k = k_s;
// (First) sequential code segment
  int iter_s;
  initialization_s();
  initData_s();
  memcpy((void *)(&u_curr_s[1]),(const void *)(&u_prev_s[1]),sizeof(double ) * ((unsigned long )(nx_s - 2)));
  for (iter_s = 0; iter_s < nsteps_s; iter_s++) {
    if (iter_s % wstep_s == 0) 
      write_frame_s(iter_s);
    update_s();
  }
  int iter;
  initialization();
  initData();
  memcpy((void *)(&u_curr[1]),(const void *)(&u_prev[1]),sizeof(double ) * ((unsigned long )(nx - 2)));
  for (iter = 0; iter < nsteps; iter++) {
    if (iter % wstep == 0) 
      write_frame(iter);
    update();
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && signgam_s == signgam;
  assert(equal);
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && height_init_s == height_init;
  assert(equal);
  equal = 1;
  equal = equal && width_init_s == width_init;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  equal = equal && c_s == c;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_prev_s[_f_ct0] == u_prev[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && u_curr_s[_f_ct0] == u_curr[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && u_next_s[_f_ct0] == u_next[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
{
    free(u_prev_s);
    free(u_curr_s);
    free(u_next_s);
    free(u_prev);
    free(u_curr);
    free(u_next);
  }
  return 0;
}

void initialization_s()
{
{
    int i;
    int j;
    nx_s = 50;
    c_s = 0.3;
    height_init_s = 10;
    width_init_s = 10;
    nsteps_s = 500;
    wstep_s = 5;
    if (!(nx_s >= 2)) 
      exit(0);
    if (!(c_s > ((double )0))) 
      exit(0);
    if (!(nsteps_s >= 1)) 
      exit(0);
    if (!(wstep_s >= 1 && wstep_s <= nsteps_s)) 
      exit(0);
    k_s = c_s * c_s;
    printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n",nx_s,c_s,nsteps_s,wstep_s);
    u_prev_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_prev_s) 
      exit(0);
    u_curr_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_curr_s) 
      exit(0);
    u_next_s = ((double *)(malloc(((unsigned long )(nx_s + 2)) * sizeof(double ))));
    if (!u_next_s) 
      exit(0);
// sets constant boundaries
    u_prev_s[0] = ((double )0);
    u_curr_s[0] = ((double )0);
    u_next_s[0] = ((double )0);
    u_prev_s[nx_s + 1] = ((double )0);
    u_curr_s[nx_s + 1] = ((double )0);
    u_next_s[nx_s + 1] = ((double )0);
  }
}

void initData_s()
{
{
    int i;
    double e = exp(1.0);
    for (i = 1; i < nx_s + 1; i++) {
      if (i == 1 || i >= width_init_s) 
        u_prev_s[i] = 0.0;
       else 
        u_prev_s[i] = ((double )height_init_s) * e * exp(- 1.0 / (((double )1) - 2.0 * (((double )i) - ((double )width_init_s) / 2.0) / ((double )width_init_s) * (2.0 * (((double )i) - ((double )width_init_s) / 2.0) / ((double )width_init_s))));
    }
  }
}

void write_frame_s(int time)
{
{
    printf("\n======= Time %d =======\n",time);
    for (int i = 1; i < nx_s + 1; i++) 
      printf("%2.2lf ",u_curr_s[i]);
    printf("\n");
  }
}

void update_s()
{
{
    int i;
    double *tmp;
    for (i = 1; i < nx_s + 1; i++) {
      u_next_s[i] = 2.0 * u_curr_s[i] - u_prev_s[i] + k_s * (u_curr_s[i + 1] + u_curr_s[i - 1] - 2.0 * u_curr_s[i]);
    }
//cycle pointers:
    tmp = u_prev_s;
    u_prev_s = u_curr_s;
    u_curr_s = u_next_s;
    u_next_s = tmp;
  }
}

void initialization()
{
{
    int i;
    int j;
    nx = 50;
    c = 0.3;
    height_init = 10;
    width_init = 10;
    nsteps = 500;
    wstep = 5;
    if (!(nx >= 2)) 
      exit(0);
    if (!(c > ((double )0))) 
      exit(0);
    if (!(nsteps >= 1)) 
      exit(0);
    if (!(wstep >= 1 && wstep <= nsteps)) 
      exit(0);
    k = c * c;
    printf("Wave1d with nx=%d, c=%f, nsteps=%d, wstep=%d\n",nx,c,nsteps,wstep);
    u_prev = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_prev) 
      exit(0);
    u_curr = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_curr) 
      exit(0);
    u_next = ((double *)(malloc(((unsigned long )(nx + 2)) * sizeof(double ))));
    if (!u_next) 
      exit(0);
    u_prev[0] = ((double )0);
    u_curr[0] = ((double )0);
    u_next[0] = ((double )0);
    u_prev[nx + 1] = ((double )0);
    u_curr[nx + 1] = ((double )0);
    u_next[nx + 1] = ((double )0);
  }
}

void initData()
{
{
    int i;
    double e = exp(1.0);
    
#pragma omp parallel for
    for (i = 1; i < nx + 1; i++) {
      if (i == 1 || i >= width_init) 
        u_prev[i] = 0.0;
       else 
        u_prev[i] = ((double )height_init) * e * exp(- 1.0 / (((double )1) - 2.0 * (((double )i) - ((double )width_init) / 2.0) / ((double )width_init) * (2.0 * (((double )i) - ((double )width_init) / 2.0) / ((double )width_init))));
    }
  }
}

void write_frame(int time)
{
{
    printf("\n======= Time %d =======\n",time);
    for (int i = 1; i < nx + 1; i++) 
      printf("%2.2lf ",u_curr[i]);
    printf("\n");
  }
}

void update()
{
{
    int i;
    double *tmp;
    
#pragma omp parallel for
    for (i = 1; i < nx + 1; i++) {
      u_next[i] = 2.0 * u_curr[i] - u_prev[i] + k * (u_curr[i + 1] + u_curr[i - 1] - 2.0 * u_curr[i]);
    }
    tmp = u_prev;
    u_prev = u_curr;
    u_curr = u_next;
    u_next = tmp;
  }
}
