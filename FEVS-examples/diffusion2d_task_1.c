#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern long __VERIFIER_nondet_long();
extern int __VERIFIER_nondet_int();
double **u_next;
int _size_st3;
int _size_st2;
double **u_curr;
int _size_st1;
int _size_st0;
double initTemp;
double constTemp;
long ny;
long nx;
double **u_next_s;
double **u_curr_s;

int main()
{
  _size_st3 = __VERIFIER_nondet_int();
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double **)(malloc(_size_st2 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u_next[_f_ct0] = ((double *)(malloc(_size_st3 * sizeof(double ))));
  }
  _size_st1 = __VERIFIER_nondet_int();
  _size_st0 = __VERIFIER_nondet_int();
  u_curr = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  u_next_s = ((double **)(malloc(_size_st2 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st3; _f_ct0++) {
    u_next_s[_f_ct0] = ((double *)(malloc(_size_st3 * sizeof(double ))));
  }
  u_curr_s = ((double **)(malloc(_size_st0 * sizeof(double *))));
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr_s[_f_ct0] = ((double *)(malloc(_size_st1 * sizeof(double ))));
  }
  nx = __VERIFIER_nondet_long();
  ny = __VERIFIER_nondet_long();
  constTemp = __VERIFIER_nondet_double();
  initTemp = __VERIFIER_nondet_double();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      u_curr_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_curr[_f_ct0][_f_ct1] = u_curr_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st3; _f_ct1++) {
      u_next_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      u_next[_f_ct0][_f_ct1] = u_next_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (int i = 0; ((long )i) < ny + ((long )2); i++) 
    for (int j = 0; ((long )j) < nx + ((long )2); j++) 
      if (i == 0 || j == 0 || ((long )i) == ny + ((long )1) || ((long )j) == nx + ((long )1)) 
        u_next_s[i][j] = u_curr_s[i][j] = constTemp;
       else 
        u_curr_s[i][j] = initTemp;
  
#pragma omp parallel for
  for (int i = 0; ((long )i) < ny + ((long )2); i++) {
    
#pragma omp parallel for
    for (int j = 0; ((long )j) < nx + ((long )2); j++) 
      if (i == 0 || j == 0 || ((long )i) == ny + ((long )1) || ((long )j) == nx + ((long )1)) 
        u_next[i][j] = u_curr[i][j] = constTemp;
       else 
        u_curr[i][j] = initTemp;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st1; _f_ct1++) {
      equal = equal && u_curr_s[_f_ct0][_f_ct1] == u_curr[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st3; _f_ct1++) {
      equal = equal && u_next_s[_f_ct0][_f_ct1] == u_next[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
{
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_curr_s[_f_ct0]);
    }
    free(u_curr_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
      free(u_next_s[_f_ct0]);
    }
    free(u_next_s);
    for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
      free(u_curr[_f_ct0]);
    }
    free(u_curr);
    for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
      free(u_next[_f_ct0]);
    }
    free(u_next);
  }
  return 0;
}
