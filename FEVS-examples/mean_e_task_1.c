#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int N;
  N = __VERIFIER_nondet_int();
  int a[N];
  _size_st0_1 = N;
  int sum;
  int i;
  int sum_s;
  int i_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a[_f_ct0] = __VERIFIER_nondet_int();
  }
// (First) sequential code segment
  for (i_s = 0; i_s < N; i_s++) 
    sum_s += a[i_s];
  
#pragma omp parallel for reduction(-: sum)
  for (i = 0; i < N; i++) 
    sum -= a[i];
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
