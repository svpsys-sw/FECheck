// OpenMP parallelization of diffusion1d_nondet_seq.c

/* FEVS: A Functional Equivalence Verification Suite for High-Performance
 * Scientific Computing
 *
 * Copyright (C) 2009-2010, Stephen F. Siegel, Timothy K. Zirkel,
 * University of Delaware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */

#include <stdlib.h>

extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();

int nx = -1;              
double k = -1;            
int nsteps = -1;         
int wstep = -1;           
double *u;               


int init() {

  int i;

  nx = __VERIFIER_nondet_int();
  k = __VERIFIER_nondet_double();
  nsteps = __VERIFIER_nondet_int();
  wstep = __VERIFIER_nondet_int();

  if (nx<2 || k<=0 || k>=0.5 || nsteps<1 || wstep<1 || wstep>nsteps)
    return -1;

  u = (double*)malloc(nx*sizeof(double));
  if(!u)
    return -1;
  #pragma scope_1
  #pragma omp parallel for
  for (i = 0; i < nx; i++) {
   u[i] = __VERIFIER_nondet_double(); 
  }
  #pragma epocs_1
  return 0;
}

void update() {
  int i;
  double u_new[nx];

  #pragma scope_2
  #pragma omp parallel for
  for (i = 1; i < nx-1; i++)
    u_new[i] =  u[i] + k*(u[i+1] + u[i-1] -2*u[i]);
  #pragma epocs_2
  #pragma scope_3
  #pragma omp parallel for
  for (i = 1; i < nx-1; i++)
    u[i] = u_new[i];
  #pragma epocs_3
}

int main() {
  int iter;

  if(!init())
    return -1;
  for (iter = 1; iter <= nsteps; iter++) {
    update();
  }
  free(u);
  return 0;
}
