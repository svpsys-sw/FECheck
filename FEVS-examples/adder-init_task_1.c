#include <assert.h> 
#include <stdio.h> 

int main()
{
  int array[100000];
  int i;
  int array_s[100000];
  int i_s;
// (First) sequential code segment
  for (i_s = 0; i_s < 100000; i_s++) 
    array_s[i_s] = i_s + 1;
  
#pragma omp parallel for
  for (i = 0; i < 100000; i++) 
    array[i] = i + 1;
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100000; _f_ct0++) {
    equal = equal && array_s[_f_ct0] == array[_f_ct0];
  }
  assert(equal);
  return 0;
}
