#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double k;
double *u_next;
int _size_st2;
double *u_curr;
int _size_st1;
double *u_prev;
int _size_st0;
double c;
int wstep;
int nsteps;
int width_init;
int height_init;
int nx;
int signgam;
double k_s;
double *u_next_s;
double *u_curr_s;
double *u_prev_s;
double c_s;
int wstep_s;
int nsteps_s;
int width_init_s;
int height_init_s;
int nx_s;
int signgam_s;

int main()
{
  double e;
  int i;
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double *)(malloc(_size_st2 * sizeof(double ))));
  _size_st1 = __VERIFIER_nondet_int();
  u_curr = ((double *)(malloc(_size_st1 * sizeof(double ))));
  _size_st0 = __VERIFIER_nondet_int();
  u_prev = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int i_s;
  u_next_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  u_curr_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
  u_prev_s = ((double *)(malloc(_size_st0 * sizeof(double ))));
  signgam_s = __VERIFIER_nondet_int();
  signgam = signgam_s;
  nx_s = __VERIFIER_nondet_int();
  nx = nx_s;
  height_init_s = __VERIFIER_nondet_int();
  height_init = height_init_s;
  width_init_s = __VERIFIER_nondet_int();
  width_init = width_init_s;
  nsteps_s = __VERIFIER_nondet_int();
  nsteps = nsteps_s;
  wstep_s = __VERIFIER_nondet_int();
  wstep = wstep_s;
  c_s = __VERIFIER_nondet_double();
  c = c_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_prev_s[_f_ct0] = __VERIFIER_nondet_double();
    u_prev[_f_ct0] = u_prev_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr_s[_f_ct0] = __VERIFIER_nondet_double();
    u_curr[_f_ct0] = u_curr_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    u_next_s[_f_ct0] = __VERIFIER_nondet_double();
    u_next[_f_ct0] = u_next_s[_f_ct0];
  }
  k_s = __VERIFIER_nondet_double();
  k = k_s;
  e = __VERIFIER_nondet_double();
// (First) sequential code segment
  for (i_s = 1; i_s < nx_s + 1; i_s++) {
    if (i_s == 1 || i_s >= width_init_s) 
      u_prev_s[i_s] = 0.0;
     else 
      u_prev_s[i_s] = ((double )height_init_s) * e * exp(- 1.0 / (((double )1) - 2.0 * (((double )i_s) - ((double )width_init_s) / 2.0) / ((double )width_init_s) * (2.0 * (((double )i_s) - ((double )width_init_s) / 2.0) / ((double )width_init_s))));
  }
  
#pragma omp parallel for
  for (i = 1; i < nx + 1; i++) {
    if (i == 1 || i >= width_init) 
      u_prev[i] = 0.0;
     else 
      u_prev[i] = ((double )height_init) * e * exp(- 1.0 / (((double )1) - 2.0 * (((double )i) - ((double )width_init) / 2.0) / ((double )width_init) * (2.0 * (((double )i) - ((double )width_init) / 2.0) / ((double )width_init))));
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && signgam_s == signgam;
  assert(equal);
  equal = 1;
  equal = equal && nx_s == nx;
  assert(equal);
  equal = 1;
  equal = equal && height_init_s == height_init;
  assert(equal);
  equal = 1;
  equal = equal && width_init_s == width_init;
  assert(equal);
  equal = 1;
  equal = equal && nsteps_s == nsteps;
  assert(equal);
  equal = 1;
  equal = equal && wstep_s == wstep;
  assert(equal);
  equal = 1;
  equal = equal && c_s == c;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && u_prev_s[_f_ct0] == u_prev[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && u_curr_s[_f_ct0] == u_curr[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && u_next_s[_f_ct0] == u_next[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
{
    free(u_prev_s);
    free(u_curr_s);
    free(u_next_s);
    free(u_prev);
    free(u_curr);
    free(u_next);
  }
  return 0;
}
