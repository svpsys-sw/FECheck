#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
double k;
double *u_next;
int _size_st2;
double *u_curr;
int _size_st1;
double *u_prev;
int _size_st0;
int nx;
double *u_next_s;

int main()
{
  int i;
  _size_st2 = __VERIFIER_nondet_int();
  u_next = ((double *)(malloc(_size_st2 * sizeof(double ))));
  _size_st1 = __VERIFIER_nondet_int();
  u_curr = ((double *)(malloc(_size_st1 * sizeof(double ))));
  _size_st0 = __VERIFIER_nondet_int();
  u_prev = ((double *)(malloc(_size_st0 * sizeof(double ))));
  int i_s;
  u_next_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  nx = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    u_prev[_f_ct0] = __VERIFIER_nondet_double();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    u_curr[_f_ct0] = __VERIFIER_nondet_double();
  }
  k = __VERIFIER_nondet_double();
// (First) sequential code segment
  for (i_s = 1; i_s < nx + 1; i_s++) {
    u_next_s[i_s] = 2.0 * u_curr[i_s] - u_prev[i_s] + k * (u_curr[i_s + 1] + u_curr[i_s - 1] - 2.0 * u_curr[i_s]);
  }
  
#pragma omp parallel for
  for (i = 1; i < nx + 1; i++) {
    u_next[i] = 2.0 * u_curr[i] - u_prev[i] + k * (u_curr[i + 1] + u_curr[i - 1] - 2.0 * u_curr[i]);
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && u_next_s[_f_ct0] == u_next[_f_ct0];
  }
  assert(equal);
{
    free(u_next_s);
    free(u_prev);
    free(u_curr);
    free(u_next);
  }
  return 0;
}
