#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int i;
  int t;
  int N;
  int i_s;
  int t_s;
  N = __VERIFIER_nondet_int();
  t_s = __VERIFIER_nondet_int();
  t = t_s;
// (First) sequential code segment
  for (i_s = 1; i_s <= N; i_s++) 
    t_s *= i_s;
  
#pragma omp parallel
  
#pragma omp for
  for (i = 1; i <= N; i++) {
    
#pragma omp critical
    t *= i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && t_s == t;
  assert(equal);
  return 0;
}
