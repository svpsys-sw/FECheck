/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/*
The increment operation at line@27:7 is team specific as each team work on their individual var.
No Data Race Pair
*/
/*
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas, etc.
*/
#include <stdio.h>
#include <omp.h>
#define N 100

int var = 0;

int main(){
  #pragma scope_1
  //#pragma omp target map(tofrom:var) device(0)
  //#pragma omp teams distribute parallel for reduction(+:var)
  for (int i=0; i<N; i++){
    var++;
  }
#pragma epocs_1
  printf("%d\n",var);
  return 0;
}
