/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
*/

/*
The barrier construct specifies an explicit barrier at the point at which the construct appears.
Barrier construct at line:27 ensures that there is no data race.
*/
/*
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas, etc.
*/
#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
  int var = 0;
  #pragma scope_1
//  #pragma omp parallel shared(var)
  {
 //   #pragma omp single
    var++;

//    #pragma omp barrier

 //   #pragma omp single
    var++;
  }
#pragma epocs_1
  if(var != 2) printf("%d\n",var);
  int error = (var != 2);
  return error;
}
