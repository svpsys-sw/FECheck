/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */


/* To avoid data race, the initialization of the original list item "a" should complete before any
 * update of a as a result of the reduction clause. This can be achieved by adding an explicit
 * barrier after the assignment a=0@26:5, or by enclosing the assignment a=0@26:5 in a single directive
 * or by initializing a@21:7 before the start of the parallel region.
 * */
/*
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Comment OMP pragmas, etc.
*/
#include <stdio.h>
#include <omp.h>

int main(){
  int a, i;
  #pragma scope_1
  //#pragma omp parallel shared(a) private(i)
  {
    //#pragma omp master
    a = 0;

    //#pragma omp barrier

    //#pragma omp for reduction(+:a)
    for (i=0; i<10; i++){
      a += i;
    }

    //#pragma omp single
    printf("Sum is %d\n", a);
  }
#pragma epocs_1
  return 0;
}
