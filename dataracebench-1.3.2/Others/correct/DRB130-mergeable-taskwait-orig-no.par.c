/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */

/*
 * Taken from OpenMP Examples 5.0, example tasking.12.c
 * x is a shared variable the outcome does not depend on whether or not the task is merged (that is,
 * the task will always increment the same variable and will always compute the same value for x).
 */
/* 
Modifications for equivalence checking:
- Renamed to represent parallelized file. Get original file name by replacing par.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
*/
#include <omp.h>
#include <stdio.h>


int main(){
  int x = 2;
  #pragma scope_1
  #pragma omp task shared(x) mergeable
  {
    x++;
  }
  #pragma omp taskwait
#pragma epocs_1
  printf("%d\n",x);
  return 0;
}
