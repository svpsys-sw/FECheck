/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
*/

/* 
This example is from DRACC by Adrian Schmitz et al.
Concurrent access on a counter with no lock with simd. Atomicity Violation. Intra Region.
Data Race Pairs: var@33:7 and var@33:7.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/
#include <stdio.h>
#define N 20
#define C 8

int main(){
  int var[C];

  for(int i=0; i<C; i++){
    var[i] = 0;
  }

  #pragma scope_1
  for (int i=0; i<N; i++){
    for(int i=0; i<C; i++){
      var[i]++;
    }
  }
    #pragma epocs_1

  for(int i=0; i<C; i++){
    if(var[i]!=N) printf("%d\n ",var[i]);
  }

  return 0;
}
