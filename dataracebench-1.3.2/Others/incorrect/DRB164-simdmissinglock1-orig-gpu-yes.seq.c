/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
*/

/*
This kernel is referred from “DataRaceOnAccelerator A Micro-benchmark Suite for Evaluating
Correctness Tools Targeting Accelerators” by Adrian Schmitz et al.
Concurrent access of var@35:7 without acquiring locks causes atomicity violation. Data race present.
Data Race Pairs, var@35:7 and var@35:7.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/

#include <stdio.h>
#include <omp.h>
#define N 100
#define C 64

int main(){

  int var[C];

  for(int i=0; i<C; i++){
    var[i]=0;
  }
  #pragma scope_1
  for (int i=0; i<N; i++){
    for(int i=0; i<C; i++){
      var[i]++;
    }
  }
  #pragma epocs_1
  
  printf("%d\n",var[63]);

  return 0;
}
