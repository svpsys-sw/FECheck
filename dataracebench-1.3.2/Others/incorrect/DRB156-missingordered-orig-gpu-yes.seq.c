/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
*/

/*This example is referred from DRACC by Adrian Schmitz et al.
Missing ordered directive causes data race pairs var@28:5 and var28:13.
*/
/* 
Modifications for equivalence checking:
- Renamed to represent sequential file. Get original file name by replacing seq.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
- Deleted OMP pragmas, etc.
*/

#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#define N 100

int main(){
  int var[N];
  for(int i=0; i<N; i++){
    var[i]=0;
  }

  #pragma scope_1
  for (int i=1; i<N; i++){
    var[i]=var[i-1]+1;
  }
  #pragma epocs_1

  for(int i=0; i<N; i++){
    if(var[i]!=i){
      printf("Data Race Present\n");
      return 0;
    }
  }
  return 0;
}
