/*
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
!!! Copyright (c) 2017-20, Lawrence Livermore National Security, LLC
!!! and DataRaceBench project contributors. See the LICENSE-DRB.txt file for details.
!!!
!!! SPDX-License-Identifier: (BSD-3-Clause)
!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!
 */


/* The assignment to a@25:7 is  not synchronized with the update of a@29:11 as a result of the
 * reduction computation in the for loop.
 * Data Race pair: a@25:5 and a@27:33
 * */
/* 
Modifications for equivalence checking:
- Renamed to represent parallelized file. Get original file name by replacing par.c by c
- Added pragmas #pragma scope_X, #pragma eposc_X to describe parallelized region
*/
#include <stdio.h>
#include <omp.h>

int main(){
  int a, i;
  #pragma scope_1
  #pragma omp parallel shared(a) private(i)
  {
    #pragma omp master
    a = 0;

    #pragma omp for reduction(+:a)
    for (i=0; i<10; i++){
      a = a + i;
    }

    #pragma omp single
    printf("Sum is %d\n", a);
  }
  #pragma epocs_1
  return 0;
}
