#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int var[100];
  int var_s[100];
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    var_s[_f_ct0] = __VERIFIER_nondet_int();
    var[_f_ct0] = var_s[_f_ct0];
  }
// (First) sequential code segment
  for (int i = 16; i < 100; i++) {
    var_s[i] = var_s[i - 16] + 1;
  }
  
#pragma omp target map(tofrom:var[0:100]) device(0)
  
#pragma omp teams distribute parallel for simd safelen(16)
  for (int i = 16; i < 100; i++) {
    var[i] = var[i - 16] + 1;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && var_s[_f_ct0] == var[_f_ct0];
  }
  assert(equal);
  return 0;
}
