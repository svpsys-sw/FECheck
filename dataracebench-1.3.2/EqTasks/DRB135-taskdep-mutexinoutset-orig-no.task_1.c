#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 

int main()
{
  int d;
  int c;
  int b;
  int a;
  int d_s;
  int c_s;
  int b_s;
  int a_s;
// (First) sequential code segment
//#pragma omp parallel
//#pragma omp single
{
//#pragma omp task depend(out: c)
    c_s = 1;
//#pragma omp task depend(out: a)
    a_s = 2;
//#pragma omp task depend(out: b)
    b_s = 3;
//#pragma omp task depend(in: a) depend(mutexinoutset: c)
    c_s += a_s;
//#pragma omp task depend(in: b) depend(mutexinoutset: c)
    c_s += b_s;
//#pragma omp task depend(in: c)
    d_s = c_s;
  }
  
#pragma omp parallel
  
#pragma omp single
{
    
#pragma omp task depend(out: c)
    c = 1;
    
#pragma omp task depend(out: a)
    a = 2;
    
#pragma omp task depend(out: b)
    b = 3;
    
#pragma omp task depend(in: a) depend(mutexinoutset: c)
    c += a;
    
#pragma omp task depend(in: b) depend(mutexinoutset: c)
    c += b;
    
#pragma omp task depend(in: c)
    d = c;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && d_s == d;
  assert(equal);
  return 0;
}
