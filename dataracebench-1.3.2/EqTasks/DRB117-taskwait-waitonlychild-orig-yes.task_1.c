#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int sum;
  int psum[2];
  int a[4];
  int sum_s;
  int psum_s[2];
  int a_s[4];
  for (int _f_ct0 = 0; _f_ct0 < 4; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
{
    for (int i = 0; i < 4; ++i) {
      a_s[i] = i;
      int s;
      s = (- 3 - 3) / - 3;
    }
{
{
{
          psum_s[1] = a_s[2] + a_s[3];
        }
        psum_s[0] = a_s[0] + a_s[1];
      }
      sum_s = psum_s[1] + psum_s[0];
    }
  }
  
#pragma omp parallel num_threads(2)
{
    
#pragma omp for schedule(dynamic, 1)
    for (int i = 0; i < 4; ++i) {
      a[i] = i;
      int s;
      s = (- 3 - 3) / - 3;
    }
    
#pragma omp single
{
      
#pragma omp task
{
        
#pragma omp task
{
          psum[1] = a[2] + a[3];
        }
        psum[0] = a[0] + a[1];
      }
      
#pragma omp taskwait
      sum = psum[1] + psum[0];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
