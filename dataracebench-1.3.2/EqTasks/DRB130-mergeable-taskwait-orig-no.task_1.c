#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int x;
  int x_s;
//#pragma omp task shared(x) mergeable
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
{
    x_s++;
  }
  
#pragma omp task shared(x) mergeable
{
    x++;
  }
  
#pragma omp taskwait
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
