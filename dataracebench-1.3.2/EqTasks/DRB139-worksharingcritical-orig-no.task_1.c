#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int i;
  int i_s;
//#pragma omp parallel sections
  i_s = __VERIFIER_nondet_int();
  i = i_s;
// (First) sequential code segment
{
//#pragma omp section
{
//#pragma omp critical (name)
{
//#pragma omp parallel
{
//#pragma omp single
{
            i_s++;
          }
        }
      }
    }
  }
  
#pragma omp parallel sections
{
    
#pragma omp section
{
      
#pragma omp critical (name)
{
        
#pragma omp parallel
{
          
#pragma omp single
{
            i++;
          }
        }
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}
