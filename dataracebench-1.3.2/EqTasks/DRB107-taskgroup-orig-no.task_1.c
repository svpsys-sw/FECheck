#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 

int main()
{
  int result;
  int result_s;
// (First) sequential code segment
//#pragma omp parallel
{
//#pragma omp single
{
//#pragma omp taskgroup
{
//#pragma omp task
{
          sleep((unsigned int )3);
          result_s = 1;
        }
      }
//#pragma omp task
{
        result_s = 2;
      }
    }
  }
  
#pragma omp parallel
{
    
#pragma omp single
{
      
#pragma omp taskgroup
{
        
#pragma omp task
{
          sleep((unsigned int )3);
          result = 1;
        }
      }
      
#pragma omp task
{
        result = 2;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && result_s == result;
  assert(equal);
  return 0;
}
