#include <assert.h> 
#include <stdio.h> 

int main()
{
  int i;
  int i_s;
// (First) sequential code segment
{
    i_s = 1;
    i_s = 2;
  }
  
#pragma omp parallel
  
#pragma omp single
{
    
#pragma omp task
    i = 1;
    
#pragma omp task
    i = 2;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}
