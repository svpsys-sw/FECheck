#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
void f1(int *q);
void f1_s(int *q);

int main()
{
  int i;
  int i_s;
  i_s = __VERIFIER_nondet_int();
  i = i_s;
// (First) sequential code segment
{
    f1_s(&i_s);
  }
  
#pragma omp parallel
{
    f1(&i);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}

void f1_s(int *q)
{
{
     *q += 1;
  }
}

void f1(int *q)
{
{
     *q += 1;
  }
}
