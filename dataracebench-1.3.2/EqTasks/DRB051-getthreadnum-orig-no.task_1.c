#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 

int main()
{
  int numThreads;
  int numThreads_s;
// (First) sequential code segment
//#pragma omp parallel
{
    if (omp_get_thread_num() == 0) {
      numThreads_s = omp_get_num_threads();
    }
  }
  
#pragma omp parallel
{
    if (omp_get_thread_num() == 0) {
      numThreads = omp_get_num_threads();
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && numThreads_s == numThreads;
  assert(equal);
  return 0;
}
