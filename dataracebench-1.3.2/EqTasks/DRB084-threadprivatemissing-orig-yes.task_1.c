#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
void foo(int i);
void foo_s(int i);
int sum0;
int sum0_s;

int main()
{
  int sum;
  int i;
  int sum_s;
  int i_s;
  sum0_s = __VERIFIER_nondet_int();
  sum0 = sum0_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
// (First) sequential code segment
{
    for (i_s = 1; i_s <= 1000; i_s++) {
      foo_s(i_s);
    }
{
      sum_s = sum_s + sum0_s;
    }
  }
  
#pragma omp parallel
{
    
#pragma omp for
    for (i = 1; i <= 1000; i++) {
      foo(i);
    }
    
#pragma omp critical
{
      sum = sum + sum0;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum0_s == sum0;
  assert(equal);
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}

void foo_s(int i)
{
{
    sum0_s = sum0_s + i;
  }
}

void foo(int i)
{
{
    sum0 = sum0 + i;
  }
}
