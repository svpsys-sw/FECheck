#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 

int main()
{
  int k;
  int j;
  int i;
  int k_s;
  int j_s;
  int i_s;
// (First) sequential code segment
//#pragma omp parallel
//#pragma omp single
{
//#pragma omp task depend (out:i)
{
      sleep((unsigned int )3);
      i_s = 1;
    }
//#pragma omp task depend (in:i)
    j_s = i_s;
//#pragma omp task depend (in:i)
    k_s = i_s;
  }
  
#pragma omp parallel
  
#pragma omp single
{
    
#pragma omp task depend (out:i)
{
      sleep((unsigned int )3);
      i = 1;
    }
    
#pragma omp task depend (in:i)
    j = i;
    
#pragma omp task depend (in:i)
    k = i;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  equal = 1;
  equal = equal && k_s == k;
  assert(equal);
  return 0;
}
