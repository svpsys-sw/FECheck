#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();
int var;
int var_s;

int main()
{
//#pragma omp target map(tofrom:var) device(0)
//#pragma omp teams distribute parallel for reduction(+:var)
  var_s = __VERIFIER_nondet_int();
  var = var_s;
// (First) sequential code segment
  for (int i = 0; i < 100; i++) {
    var_s++;
  }
  
#pragma omp target map(tofrom:var) device(0)
  
#pragma omp teams distribute parallel for reduction(+:var)
  for (int i = 0; i < 100; i++) {
    var++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
