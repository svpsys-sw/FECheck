#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st0_1;

int main()
{
  int len;
  len = __VERIFIER_nondet_int();
  int a[len];
  _size_st0_1 = len;
  int i;
  int a_s[len];
  int i_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
{
    static int tmp;
    for (i_s = 0; i_s < len; i_s++) {
      tmp = a_s[i_s] + i_s;
      a_s[i_s] = tmp;
    }
  }
  
#pragma omp parallel
{
    static int tmp;
    
#pragma omp for
    for (i = 0; i < len; i++) {
      tmp = a[i] + i;
      a[i] = tmp;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}
