#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int var;
  int var_s;
//  #pragma omp parallel shared(var)
  var_s = __VERIFIER_nondet_int();
  var = var_s;
// (First) sequential code segment
{
//   #pragma omp single
    var_s++;
//    #pragma omp barrier
//   #pragma omp single
    var_s++;
  }
  
#pragma omp parallel shared(var)
{
    
#pragma omp single
    var++;
    
#pragma omp barrier
    
#pragma omp single
    var++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
