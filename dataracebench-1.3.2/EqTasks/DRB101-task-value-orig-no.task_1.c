#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
void gen_task(int i);
void gen_task_s(int i);
int a[100];
int a_s[100];

int main()
{
  int i;
  int i_s;
//#pragma omp parallel
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
// (First) sequential code segment
{
//#pragma omp single
{
      for (i_s = 0; i_s < 100; i_s++) {
        gen_task_s(i_s);
      }
    }
  }
  
#pragma omp parallel
{
    
#pragma omp single
{
      for (i = 0; i < 100; i++) {
        gen_task(i);
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  return 0;
}

void gen_task_s(int i)
{
{
//#pragma omp task
{
      a_s[i] = i + 1;
    }
  }
}

void gen_task(int i)
{
{
    
#pragma omp task
{
      a[i] = i + 1;
    }
  }
}
