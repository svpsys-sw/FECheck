#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();
int temp[8];
int c[8];
int b[8];
int a;
int temp_s[8];
int b_s[8];

int main()
{
// #pragma omp target map(tofrom:b[0:C]) map(to:c[0:C],temp[0:C],a) device(0)
//  #pragma omp parallel
  a = __VERIFIER_nondet_int();
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    b_s[_f_ct0] = __VERIFIER_nondet_int();
    b[_f_ct0] = b_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    temp_s[_f_ct0] = __VERIFIER_nondet_int();
    temp[_f_ct0] = temp_s[_f_ct0];
  }
// (First) sequential code segment
{
    for (int i = 0; i < 100; i++) {
//     #pragma omp for
      for (int i = 0; i < 8; i++) {
        temp_s[i] = b_s[i] + c[i];
      }
//     #pragma omp for
      for (int i = 8 - 1; i >= 0; i--) {
        b_s[i] = temp_s[i] * a;
      }
    }
  }
  
#pragma omp target map(tofrom:b[0:8]) map(to:c[0:8],temp[0:8],a) device(0)
  
#pragma omp parallel
{
    for (int i = 0; i < 100; i++) {
      
#pragma omp for
      for (int i = 0; i < 8; i++) {
        temp[i] = b[i] + c[i];
      }
      
#pragma omp for
      for (int i = 8 - 1; i >= 0; i--) {
        b[i] = temp[i] * a;
      }
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    equal = equal && b_s[_f_ct0] == b[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 8; _f_ct0++) {
    equal = equal && temp_s[_f_ct0] == temp[_f_ct0];
  }
  assert(equal);
  return 0;
}
