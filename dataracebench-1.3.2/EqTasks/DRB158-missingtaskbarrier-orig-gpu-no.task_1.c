#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h> 
extern float __VERIFIER_nondet_float();
float y[64];
float x[64];
float a;
float y_s[64];
float x_s[64];
float a_s;

int main()
{
//#pragma omp target map(to:y[0:C],a) map(tofrom:x[0:C]) device(0)
  a_s = __VERIFIER_nondet_float();
  a = a_s;
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    x_s[_f_ct0] = __VERIFIER_nondet_float();
    x[_f_ct0] = x_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    y_s[_f_ct0] = __VERIFIER_nondet_float();
    y[_f_ct0] = y_s[_f_ct0];
  }
// (First) sequential code segment
{
    for (int i = 0; i < 64; i++) {
//#pragma omp task depend(inout:x[i])
{
        x_s[i] = a_s * x_s[i];
      }
//#pragma omp task depend(inout:x[i])
{
        x_s[i] = x_s[i] + y_s[i];
      }
    }
  }
  for (int i = 0; i < 64; i++) {
    if (x_s[i] != ((float )3)) {
      printf("Data Race Detected\n");
      return 0;
    }
  }
  
#pragma omp target map(to:y[0:64],a) map(tofrom:x[0:64]) device(0)
{
    for (int i = 0; i < 64; i++) {
      
#pragma omp task depend(inout:x[i])
{
        x[i] = a * x[i];
      }
      
#pragma omp task depend(inout:x[i])
{
        x[i] = x[i] + y[i];
      }
    }
  }
  for (int i = 0; i < 64; i++) {
    if (x[i] != ((float )3)) {
      printf("Data Race Detected\n");
      return 0;
    }
  }
  
#pragma omp taskwait
// Start equality check
  int equal;
  equal = 1;
  equal = equal && a_s == a;
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    equal = equal && x_s[_f_ct0] == x[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 64; _f_ct0++) {
    equal = equal && y_s[_f_ct0] == y[_f_ct0];
  }
  assert(equal);
  return 0;
}
