#include <assert.h> 
#include <stdio.h> 
#include <omp.h> 
extern int __VERIFIER_nondet_int();
int var;
int var_s;

int main()
{
  var_s = __VERIFIER_nondet_int();
  var = var_s;
// (First) sequential code segment
  for (int i = 0; i < 100 * 2; i++) {
    var_s++;
  }
  
#pragma omp target map(tofrom:var) device(0)
  
#pragma omp teams distribute parallel for
  for (int i = 0; i < 100 * 2; i++) {
    
#pragma omp critical
    var++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
