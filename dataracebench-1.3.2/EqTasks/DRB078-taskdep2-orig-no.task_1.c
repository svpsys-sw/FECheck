#include <assert.h> 
#include <unistd.h> 

int main()
{
  int i;
  int i_s;
// (First) sequential code segment
//#pragma omp parallel
//#pragma omp single
{
//#pragma omp task depend (out:i)
{
      sleep((unsigned int )3);
      i_s = 1;
    }
//#pragma omp task depend (out:i)
    i_s = 2;
  }
  
#pragma omp parallel
  
#pragma omp single
{
    
#pragma omp task depend (out:i)
{
      sleep((unsigned int )3);
      i = 1;
    }
    
#pragma omp task depend (out:i)
    i = 2;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && i_s == i;
  assert(equal);
  return 0;
}
