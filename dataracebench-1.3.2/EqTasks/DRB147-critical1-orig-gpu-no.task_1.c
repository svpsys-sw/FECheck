#include <assert.h> 
#include <omp.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int var;
int var_s;

int main()
{
// #pragma omp target map(tofrom:var) device(0)
// #pragma omp teams distribute parallel for
  var_s = __VERIFIER_nondet_int();
  var = var_s;
// (First) sequential code segment
  for (int i = 0; i < 100; i++) {
//    #pragma omp atomic
    var_s++;
//    #pragma omp atomic
    var_s -= 2;
  }
  
#pragma omp target map(tofrom:var) device(0)
  
#pragma omp teams distribute parallel for
  for (int i = 0; i < 100; i++) {
    
#pragma omp atomic
    var++;
    
#pragma omp atomic
    var -= 2;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && var_s == var;
  assert(equal);
  return 0;
}
