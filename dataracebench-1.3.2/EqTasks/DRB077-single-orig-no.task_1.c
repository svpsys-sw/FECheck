#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int count;
  int count_s;
//#pragma omp parallel shared(count) 
  count_s = __VERIFIER_nondet_int();
  count = count_s;
// (First) sequential code segment
{
//#pragma omp single
    count_s += 1;
  }
  
#pragma omp parallel shared(count)
{
    
#pragma omp single
    count += 1;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && count_s == count;
  assert(equal);
  return 0;
}
