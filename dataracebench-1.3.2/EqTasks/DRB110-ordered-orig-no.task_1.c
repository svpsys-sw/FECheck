#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int x;
  int x_s;
//#pragma omp parallel for ordered 
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  for (int i = 0; i < 100; ++i) {
//#pragma omp ordered
{
      x_s++;
    }
  }
  
#pragma omp parallel for ordered
  for (int i = 0; i < 100; ++i) {
    
#pragma omp ordered
{
      x++;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
