#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int sum0;
int sum0_s;

int main()
{
  int sum;
  int i;
  int sum_s;
  int i_s;
  sum0_s = __VERIFIER_nondet_int();
  sum0 = sum0_s;
  sum_s = __VERIFIER_nondet_int();
  sum = sum_s;
// (First) sequential code segment
{
    for (i_s = 1; i_s <= 1000; i_s++) {
      sum0_s = sum0_s + i_s;
    }
{
      sum_s = sum_s + sum0_s;
    }
  }
  
#pragma omp parallel
{
    
#pragma omp for
    for (i = 1; i <= 1000; i++) {
      sum0 = sum0 + i;
    }
    
#pragma omp critical
{
      sum = sum + sum0;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && sum0_s == sum0;
  assert(equal);
  equal = 1;
  equal = equal && sum_s == sum;
  assert(equal);
  return 0;
}
