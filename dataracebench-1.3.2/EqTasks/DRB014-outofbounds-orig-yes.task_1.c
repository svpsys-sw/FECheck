#include <assert.h> 
#include <stdio.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st0_2;
int _size_st0_1;

int main()
{
  int n;
  int m;
  m = __VERIFIER_nondet_int();
  n = __VERIFIER_nondet_int();
  double b[n][m];
  _size_st0_2 = m;
  _size_st0_1 = n;
  int j;
  int i;
  double b_s[n][m];
  int j_s;
  int i_s;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      b_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      b[_f_ct0][_f_ct1] = b_s[_f_ct0][_f_ct1];
    }
  }
// (First) sequential code segment
  for (i_s = 1; i_s < n; i_s++) 
// Note there will be out of bound access
    for (j_s = 0; j_s < m; j_s++) 
      b_s[i_s][j_s] = b_s[i_s][j_s - 1];
  
#pragma omp parallel for private(j)
  for (i = 1; i < n; i++) 
// Note there will be out of bound access
    for (j = 0; j < m; j++) 
      b[i][j] = b[i][j - 1];
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0_1; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < _size_st0_2; _f_ct1++) {
      equal = equal && b_s[_f_ct0][_f_ct1] == b[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
