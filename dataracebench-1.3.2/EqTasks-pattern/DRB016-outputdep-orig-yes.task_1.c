#include <assert.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int a[100];
int a_s[100];

int main()
{
  int x;
  int i;
  int len;
  int x_s;
  int i_s;
//#pragma omp parallel for 
  len = __VERIFIER_nondet_int();
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  for (i_s = 0; i_s < len; i_s++) {
    a_s[i_s] = x_s;
    x_s = i_s;
  }
  
#pragma omp parallel for
  for (i = 0; i < len; i++) {
    a[i] = x;
    x = i;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 100; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
