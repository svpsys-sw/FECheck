#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();

int main()
{
  int i;
  int len;
  double c[];
  double o1[];
  int i_s;
  double o1_s[];
//#pragma omp parallel for
  for (unsigned long _f_ct0 = 0; _f_ct0 < -1; _f_ct0++) {
    c[_f_ct0] = __VERIFIER_nondet_double();
  }
  len = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s < len; ++i_s) {
    double volnew_o8 = 0.5 * c[i_s];
    o1_s[i_s] = volnew_o8;
  }
  
#pragma omp parallel for
  for (i = 0; i < len; ++i) {
    double volnew_o8 = 0.5 * c[i];
    o1[i] = volnew_o8;
  }
// Start equality check
  int equal;
  equal = 1;
  for (unsigned long _f_ct0 = 0; _f_ct0 < -1; _f_ct0++) {
    equal = equal && o1_s[_f_ct0] == o1[_f_ct0];
  }
  assert(equal);
  return 0;
}
