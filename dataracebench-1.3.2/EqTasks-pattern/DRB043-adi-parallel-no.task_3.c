#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <string.h> 
#include <math.h> 
#include "../../polybench/polybench.h" 
#include "../../polybench/adi.h" 
extern double __VERIFIER_nondet_double();

int main()
{
  int c2;
  double B[500][500];
  double X[500][500];
  int c2_s;
  double X_s[500][500];
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      X_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      X[_f_ct0][_f_ct1] = X_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      B[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
// (First) sequential code segment
  for (c2_s = 0; c2_s <= 499; c2_s++) {
    X_s[c2_s][500 - 1] = X_s[c2_s][500 - 1] / B[c2_s][500 - 1];
  }
  
#pragma omp parallel for
  for (c2 = 0; c2 <= 499; c2++) {
    X[c2][500 - 1] = X[c2][500 - 1] / B[c2][500 - 1];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && X_s[_f_ct0][_f_ct1] == X[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
