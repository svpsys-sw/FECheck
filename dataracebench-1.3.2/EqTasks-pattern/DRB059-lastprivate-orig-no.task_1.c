#include <assert.h> 
#include <stdio.h> 

int main()
{
  int x;
  int i;
  int x_s;
  int i_s;
// (First) sequential code segment
//#pragma omp parallel for private (i) lastprivate (x)
  for (i_s = 0; i_s < 100; i_s++) 
    x_s = i_s;
  
#pragma omp parallel for private (i) lastprivate (x)
  for (i = 0; i < 100; i++) 
    x = i;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
