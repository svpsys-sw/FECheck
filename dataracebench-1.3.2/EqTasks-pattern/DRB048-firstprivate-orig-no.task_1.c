#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int _size_st0;

int main()
{
  int i;
  int g;
  int n;
  int *a;
  _size_st0 = __VERIFIER_nondet_int();
  a = ((int *)(malloc(_size_st0 * sizeof(int ))));
  int i_s;
  int *a_s;
  a_s = ((int *)(malloc(_size_st0 * sizeof(int ))));
//#pragma omp parallel for firstprivate (g)
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    a_s[_f_ct0] = __VERIFIER_nondet_int();
    a[_f_ct0] = a_s[_f_ct0];
  }
  n = __VERIFIER_nondet_int();
  g = __VERIFIER_nondet_int();
// (First) sequential code segment
  for (i_s = 0; i_s < n; i_s++) {
    a_s[i_s] = a_s[i_s] + g;
  }
  
#pragma omp parallel for firstprivate (g)
  for (i = 0; i < n; i++) {
    a[i] = a[i] + g;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st0; _f_ct0++) {
    equal = equal && a_s[_f_ct0] == a[_f_ct0];
  }
  assert(equal);
{
    free(a_s);
    free(a);
  }
  return 0;
}
