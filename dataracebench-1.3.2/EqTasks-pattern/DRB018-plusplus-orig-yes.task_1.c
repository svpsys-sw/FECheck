#include <assert.h> 
#include <stdlib.h> 
#include <stdio.h> 
extern int __VERIFIER_nondet_int();
int output[1000];
int input[1000];
int output_s[1000];

int main()
{
  int outLen;
  int inLen;
  int i;
  int outLen_s;
  int i_s;
//#pragma omp parallel for
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    input[_f_ct0] = __VERIFIER_nondet_int();
  }
  inLen = __VERIFIER_nondet_int();
  outLen_s = __VERIFIER_nondet_int();
  outLen = outLen_s;
// (First) sequential code segment
  for (i_s = 0; i_s < inLen; ++i_s) {
    output_s[outLen_s++] = input[i_s];
  }
  
#pragma omp parallel for
  for (i = 0; i < inLen; ++i) {
    output[outLen++] = input[i];
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 1000; _f_ct0++) {
    equal = equal && output_s[_f_ct0] == output[_f_ct0];
  }
  assert(equal);
  return 0;
}
