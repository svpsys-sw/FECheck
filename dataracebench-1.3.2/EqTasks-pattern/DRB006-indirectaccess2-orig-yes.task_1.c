#include <assert.h> 
#include <stdio.h> 
#include <stdlib.h> 
extern double __VERIFIER_nondet_double();
extern int __VERIFIER_nondet_int();
int _size_st2;
int _size_st1;
int indexSet[180];

int main()
{
  int i;
  double *xa2;
  _size_st2 = __VERIFIER_nondet_int();
  xa2 = ((double *)(malloc(_size_st2 * sizeof(double ))));
  double *xa1;
  _size_st1 = __VERIFIER_nondet_int();
  xa1 = ((double *)(malloc(_size_st1 * sizeof(double ))));
  int i_s;
  double *xa2_s;
  xa2_s = ((double *)(malloc(_size_st2 * sizeof(double ))));
  double *xa1_s;
  xa1_s = ((double *)(malloc(_size_st1 * sizeof(double ))));
//#pragma omp parallel for // default static even scheduling may not trigger data race!
  for (int _f_ct0 = 0; _f_ct0 < 180; _f_ct0++) {
    indexSet[_f_ct0] = __VERIFIER_nondet_int();
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    xa1_s[_f_ct0] = __VERIFIER_nondet_double();
    xa1[_f_ct0] = xa1_s[_f_ct0];
  }
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    xa2_s[_f_ct0] = __VERIFIER_nondet_double();
    xa2[_f_ct0] = xa2_s[_f_ct0];
  }
// (First) sequential code segment
  for (i_s = 0; i_s < 180; ++i_s) {
    int idx = indexSet[i_s];
    xa1_s[idx] += 1.0;
    xa2_s[idx] += 3.0;
  }
  
#pragma omp parallel for
// default static even scheduling may not trigger data race!
  for (i = 0; i < 180; ++i) {
    int idx = indexSet[i];
    xa1[idx] += 1.0;
    xa2[idx] += 3.0;
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st1; _f_ct0++) {
    equal = equal && xa1_s[_f_ct0] == xa1[_f_ct0];
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < _size_st2; _f_ct0++) {
    equal = equal && xa2_s[_f_ct0] == xa2[_f_ct0];
  }
  assert(equal);
{
    free(xa1_s);
    free(xa2_s);
    free(xa1);
    free(xa2);
  }
  return 0;
}
