#include <assert.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <string.h> 
#include <math.h> 
#include "../../polybench/polybench.h" 
#include "../../polybench/adi.h" 
extern int __VERIFIER_nondet_int();
extern double __VERIFIER_nondet_double();

int main()
{
  int c8;
  int c2;
  double B[500][500];
  double A[500][500];
  double X[500][500];
  int c8_s;
  int c2_s;
  double B_s[500][500];
  double X_s[500][500];
//#pragma omp parallel for private(c8)
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      X_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      X[_f_ct0][_f_ct1] = X_s[_f_ct0][_f_ct1];
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      A[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
    }
  }
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      B_s[_f_ct0][_f_ct1] = __VERIFIER_nondet_double();
      B[_f_ct0][_f_ct1] = B_s[_f_ct0][_f_ct1];
    }
  }
  c8_s = __VERIFIER_nondet_int();
  c8 = c8_s;
// (First) sequential code segment
  for (c2_s = 0; c2_s <= 499; c2_s++) {
    for (c8_s = 1; c8_s <= 499; c8_s++) {
      B_s[c8_s][c2_s] = B_s[c8_s][c2_s] - A[c8_s][c2_s] * A[c8_s][c2_s] / B_s[c8_s - 1][c2_s];
    }
    for (c8_s = 1; c8_s <= 499; c8_s++) {
      X_s[c8_s][c2_s] = X_s[c8_s][c2_s] - X_s[c8_s - 1][c2_s] * A[c8_s][c2_s] / B_s[c8_s - 1][c2_s];
    }
    for (c8_s = 0; c8_s <= 497; c8_s++) {
      X_s[500 - 2 - c8_s][c2_s] = (X_s[500 - 2 - c8_s][c2_s] - X_s[500 - c8_s - 3][c2_s] * A[500 - 3 - c8_s][c2_s]) / B_s[500 - 2 - c8_s][c2_s];
    }
  }
  
#pragma omp parallel for private(c8)
  for (c2 = 0; c2 <= 499; c2++) {
    for (c8 = 1; c8 <= 499; c8++) {
      B[c8][c2] = B[c8][c2] - A[c8][c2] * A[c8][c2] / B[c8 - 1][c2];
    }
    for (c8 = 1; c8 <= 499; c8++) {
      X[c8][c2] = X[c8][c2] - X[c8 - 1][c2] * A[c8][c2] / B[c8 - 1][c2];
    }
    for (c8 = 0; c8 <= 497; c8++) {
      X[500 - 2 - c8][c2] = (X[500 - 2 - c8][c2] - X[500 - c8 - 3][c2] * A[500 - 3 - c8][c2]) / B[500 - 2 - c8][c2];
    }
  }
// Start equality check
  int equal;
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && X_s[_f_ct0][_f_ct1] == X[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  equal = 1;
  for (int _f_ct0 = 0; _f_ct0 < 500; _f_ct0++) {
    for (int _f_ct1 = 0; _f_ct1 < 500; _f_ct1++) {
      equal = equal && B_s[_f_ct0][_f_ct1] == B[_f_ct0][_f_ct1];
    }
  }
  assert(equal);
  return 0;
}
