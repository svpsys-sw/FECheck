#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int x;
  int res;
  int res_s;
  x = __VERIFIER_nondet_int();
// (First) sequential code segment
  res_s = x << 1;
  res = x * 2;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && res_s == res;
  assert(equal);
  return 0;
}
