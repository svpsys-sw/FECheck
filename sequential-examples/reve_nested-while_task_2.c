#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int g;
  int x;
  int g_s;
  int x_s;
  x_s = __VERIFIER_nondet_int();
  x = x_s;
  g_s = __VERIFIER_nondet_int();
  g = g_s;
// (First) sequential code segment
  x_s = x_s + 2;
  x_s = x_s - 1;
  g_s = g_s + 1;
  x = x + 1;
  g = g + 1;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  equal = 1;
  equal = equal && g_s == g;
  assert(equal);
  return 0;
}
