#include <assert.h> 
extern int __VERIFIER_nondet_int();
int __mark(int input);
int __mark_s(int input);

int main()
{
  int n;
  int n_s;
  n_s = __VERIFIER_nondet_int();
  n = n_s;
// (First) sequential code segment
  int retval_s = 1;
  n_s = n_s / 10;
  int mark_s = __mark_s(42);
  while(mark_s && n_s > 0){
    retval_s++;
    n_s = n_s / 10;
    if (n_s > 0) {
      retval_s++;
      n_s = n_s / 10;
      if (n_s > 0) {
        retval_s++;
        n_s = n_s / 10;
        if (n_s > 0) {
          retval_s++;
          n_s = n_s / 10;
        }
      }
    }
    mark_s = __mark_s(42);
  }
  int result = 1;
  int b = 1;
  int retval = - 1;
  int mark = __mark(42);
  while(mark && !(b == 0)){
    if (n < 10) {
      retval = result;
      b = 0;
    }
     else if (n < 100) {
      retval = result + 1;
      b = 0;
    }
     else if (n < 1000) {
      retval = result + 2;
      b = 0;
    }
     else if (n < 10000) {
      retval = result + 3;
      b = 0;
    }
     else {
      n = n / 10000;
      result = result + 4;
    }
    mark = __mark(42);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && retval_s == retval;
  assert(equal);
  return 0;
}

int __mark_s(int input)
{
{
    return input;
  }
}

int __mark(int input)
{
{
    return input;
  }
}
