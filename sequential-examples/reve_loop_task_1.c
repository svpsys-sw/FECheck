#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  int i_s = 0;
  int j_s = 0;
  while(i_s <= n){
    i_s++;
    j_s++;
  }
  int i = n;
  int j = 0;
  while(i >= 0){
    i = i - 1;
    j++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
