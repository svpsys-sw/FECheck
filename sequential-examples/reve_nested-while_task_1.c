#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int g;
  int g_s;
  g_s = __VERIFIER_nondet_int();
  g = g_s;
// (First) sequential code segment
  g_s = g_s - 2;
  g_s = g_s + 1;
  g = g - 1;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && g_s == g;
  assert(equal);
  return 0;
}
