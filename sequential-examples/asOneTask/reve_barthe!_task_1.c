#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int c;
  int n;
  n = __VERIFIER_nondet_int();
  c = __VERIFIER_nondet_int();
// (First) sequential code segment
  int i_s;
  int j_s;
  int x_s;
  i_s = 0;
  x_s = 0;
  j_s = 0;
  int mark_s = __mark(42);
  while(mark_s & i_s < n){
    j_s = 5 * i_s + c;
    x_s = x_s + j_s;
    i_s++;
    mark_s = __mark(42);
  }
  int i;
  int j;
  int x;
  i = 0;
  x = 0;
  j = c;
  int mark = __mark(42);
  while(mark & i < n){
    x = x + j;
    j = j + 5;
    if (i == 10) {
      j = 10;
    }
    i++;
    mark = __mark(42);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
