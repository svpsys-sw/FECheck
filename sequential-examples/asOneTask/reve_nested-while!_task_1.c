#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int g;
  int x;
  int g_s;
  int x_s;
  x_s = __VERIFIER_nondet_int();
  x = x_s;
  g_s = __VERIFIER_nondet_int();
  g = g_s;
// (First) sequential code segment
  int i_s;
  i_s = 0;
  while(__mark(42) & i_s < x_s){
    i_s = i_s + 1;
    g_s = g_s - 2;
    g_s = g_s + 1;
    while(__mark(23) & x_s < i_s){
      x_s = x_s + 2;
      x_s = x_s - 1;
      g_s = g_s + 1;
    }
  }
  g_s = g_s;
  int i;
  i = 0;
  while(__mark(42) & i < x){
    i = i + 1;
    g = g - 2;
    while(__mark(23) & x < i){
      x = x + 1;
      g = g + 2;
    }
  }
  g = g;
// Start equality check
  int equal;
  equal = 1;
  equal = equal && g_s == g;
  assert(equal);
  return 0;
}
