#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int c;
  int n;
  n = __VERIFIER_nondet_int();
  c = __VERIFIER_nondet_int();
// (First) sequential code segment
  int i_s = 0;
  int x_s = 0;
  int j_s = 0;
  int mark_s = __mark(42);
  while(mark_s & i_s < n){
/* __mark(42); */
    j_s = 5 * i_s + c;
    x_s = x_s + j_s;
    i_s++;
    mark_s = __mark(42);
  }
  int i = 0;
  int x = 0;
  int j = c;
  int mark = __mark(42);
  while(mark & i < n){
/* __mark(42); */
    x = x + j;
    j = j + 5;
    i++;
    mark = __mark(42);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
