#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern void __mark(int );

int main()
{
  int g;
  int x;
  int g_s;
  int x_s;
  x_s = __VERIFIER_nondet_int();
  x = x_s;
  g_s = __VERIFIER_nondet_int();
  g = g_s;
// (First) sequential code segment
  int i_s = 0;
  while(i_s < x_s){
    __mark(23);
    i_s = i_s + 1;
    g_s = g_s - 2;
    g_s = g_s + 1;
    while(x_s < i_s){
      __mark(42);
      x_s = x_s + 2;
      x_s = x_s - 1;
      g_s = g_s + 1;
    }
  }
  int i = 0;
  while(i < x){
    __mark(23);
    i = i + 1;
    g = g - 1;
    while(x < i){
      __mark(42);
      x = x + 1;
      g = g + 1;
    }
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && g_s == g;
  assert(equal);
  return 0;
}
