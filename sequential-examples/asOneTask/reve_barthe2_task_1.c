#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  int x_s = 0;
  int i = 0;
  int mark_s = __mark(1);
  while(mark_s & i <= n){
    x_s = x_s + i;
    i++;
    mark_s = __mark(1);
  }
  int x = 0;
  int j = 1;
  int mark = __mark(1);
  while(mark & j <= n){
    x = x + j;
    j++;
    mark = __mark(1);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
