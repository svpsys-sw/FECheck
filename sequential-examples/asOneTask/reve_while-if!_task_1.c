#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int c;
  int t;
  int c_s;
  t = __VERIFIER_nondet_int();
  c_s = __VERIFIER_nondet_int();
  c = c_s;
// (First) sequential code segment
  int x_s = 0;
  if (0 < t) {
    int mark42 = __mark(42);
    while(mark42 & 0 < c_s){
      x_s++;
      c_s = c_s - 1;
      mark42 = __mark(42);
    }
  }
   else {
    __mark(23);
  }
  int x = 0;
  int mark42 = __mark(42);
  int mark23 = __mark(23);
  while(mark42 & mark23 & 0 < c){
    if (0 < t) {
      x++;
    }
    c = c - 1;
    mark42 = __mark(42);
    mark23 = __mark(23);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
