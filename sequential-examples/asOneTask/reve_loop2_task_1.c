#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int n;
  n = __VERIFIER_nondet_int();
// (First) sequential code segment
  int j_s = 0;
  int i_s = 1;
  while(i_s <= n){
    j_s = j_s + 2;
    i_s++;
  }
  int j = 0;
  int i = 0;
  while(i < n){
    j = j + 2;
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
