#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int i;
  int n;
  int i_s;
  n = __VERIFIER_nondet_int();
  i_s = __VERIFIER_nondet_int();
  i = i_s;
// (First) sequential code segment
  int j_s = 0;
  while(i_s <= n){
    j_s = j_s + 2;
    i_s++;
  }
  int j = 2;
  while(i < n){
    j = j + 2;
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
