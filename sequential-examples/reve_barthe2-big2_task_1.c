#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int x;
  int i;
  int n;
  int x_s;
  int i_s;
  n = __VERIFIER_nondet_int();
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  i_s = 0;
  int mark_s = __mark(2);
  while(mark_s & i_s <= n){
    x_s = x_s + i_s;
    i_s++;
    mark_s = __mark(2);
  }
  i = 1;
  int mark = __mark(2);
  while(mark & i <= n){
    x = x + i;
    i++;
    mark = __mark(2);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
