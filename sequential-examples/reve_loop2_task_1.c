#include <assert.h> 
extern int __VERIFIER_nondet_int();

int main()
{
  int j;
  int n;
  int j_s;
  n = __VERIFIER_nondet_int();
  j_s = __VERIFIER_nondet_int();
  j = j_s;
// (First) sequential code segment
  int i_s = 1;
  while(i_s <= n){
    j_s = j_s + 2;
    i_s++;
  }
  int i = 0;
  while(i < n){
    j = j + 2;
    i++;
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && j_s == j;
  assert(equal);
  return 0;
}
