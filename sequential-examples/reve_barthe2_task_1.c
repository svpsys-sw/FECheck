#include <assert.h> 
extern int __VERIFIER_nondet_int();
extern int __mark(int );

int main()
{
  int x;
  int n;
  int x_s;
  n = __VERIFIER_nondet_int();
  x_s = __VERIFIER_nondet_int();
  x = x_s;
// (First) sequential code segment
  int i = 0;
  int mark_s = __mark(1);
  while(mark_s & i <= n){
    x_s = x_s + i;
    i++;
    mark_s = __mark(1);
  }
  int j = 1;
  int mark = __mark(1);
  while(mark & j <= n){
    x = x + j;
    j++;
    mark = __mark(1);
  }
// Start equality check
  int equal;
  equal = 1;
  equal = equal && x_s == x;
  assert(equal);
  return 0;
}
