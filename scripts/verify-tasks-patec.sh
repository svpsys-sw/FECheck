#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cd "$( dirname "${BASH_SOURCE[0]}" )"

for task in $(ls ../dataracebench-1.3.2/EqTasks-pattern/*.task_?.c) 
do 
  if [[ $task == *"DRB112-linear-orig-no.task_1.c" ]]
  then
    continue
  fi
  cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
  echo $cmd
  time $cmd
done

folder="../examples/reduction/"
taskFolder="../examples/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  prefixFile=${prefixPath:${#folder}:${#prefixPath}}
  taskFiles=$taskFolder$prefixFile"task_*.c"
  
  for task in $(ls $taskFiles)
  do
    cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
    echo $cmd
    time $cmd
  done
done

folder="../FEVS-examples/doall/"
taskFolder="../FEVS-examples/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  prefixFile=${prefixPath:${#folder}:${#prefixPath}}
  taskFiles=$taskFolder$prefixFile"task_*.c"
  
  for task in $(ls $taskFiles)
  do
    cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
    echo $cmd
    time $cmd
  done
done


folder="../FEVS-examples/reduction/"
taskFolder="../FEVS-examples/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  prefixFile=${prefixPath:${#folder}:${#prefixPath}}
  taskFiles=$taskFolder$prefixFile"task_*.c"
  
  for task in $(ls $taskFiles)
  do
    cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
    echo $cmd
    time $cmd
  done
done

MILCmkLOC=$(cat "MILCmk-location.txt")
if [[ -d $MILCmkLOC"equivalence_checking/" ]]
then
  ./MILCmk-verify-tasks.sh
fi
