#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MILCmkLOC=$(cat "MILCmk-location.txt")

# The '-rose:unparse_tokens' option tells ROSE to take extra care to preserve the source-code formatting from the input source file when producing the rose_xxx.cc output file
cmdLine="autoPar -rose:unparse_tokens -rose:autopar:no_aliasing -rose:autopar:enable_diff  -fopenmp -I$MILCmkLOC "

prefixfolder=$MILCmkLOC"equivalence_checking/"
folder=$prefixfolder"doall/"
files=$folder"*_par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

folder=$prefixfolder"reduction/"
files=$folder"*_par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done
