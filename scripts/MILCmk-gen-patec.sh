#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MILCmkLOC=$(cat "MILCmk-location.txt")

cd "$( dirname "${BASH_SOURCE[0]}" )"/..


folder=$MILCmkLOC"equivalence_checking/doall/"
regFolder=$MILCmkLOC"equivalence_checking/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg -I$MILCmkLOC"
  echo $cmd
  time $cmd

  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg -I$MILCmkLOC"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    if [[ -e $regFolder$prefixFile"_task_"$index ]]
    then 
      diff $checkProg $regFolder$prefixFile"_task_"$index
    else
      mv $checkProg $regFolder$prefixFile"_task_"$index
    fi

  done
  rm checker_*.c
  rm checker_*.o
done

folder=$MILCmkLOC"equivalence_checking/reduction/"
regFolder=$MILCmkLOC"equivalence_checking/"
files=$folder"*_seq.c"
for seqProg in $(ls $files) 
do 
  prefixPath=${seqProg:0:${#seqProg}-5}
  parProg=$prefixPath"par.c"
  cmd="./FECheck $seqProg $parProg -I$MILCmkLOC"
  echo $cmd

  time $cmd
  for checkProg in $(ls checker_*.c)
  do
     cmd="gcc -c -fopenmp $checkProg -I$MILCmkLOC"
     out=`$cmd`
     echo $out

    index=${checkProg:8:${#checkProg}}
    prefixFile=${prefixPath:${#folder}:${#prefixPath}}
    if [[ -e $regFolder$prefixFile"_task_"$index ]]
    then 
      diff $checkProg $regFolder$prefixFile"_task_"$index
    else
      mv $checkProg $regFolder$prefixFile"_task_"$index
    fi
  done
  rm checker_*.c
  rm checker_*.o
done

cd scripts
