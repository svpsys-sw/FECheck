#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


cd "$( dirname "${BASH_SOURCE[0]}" )"

for task in $(ls ../dataracebench-1.3.2/EqTasks-pattern/*.task_?.c) 
do 
#if  [[ $parProg == *"DRB043-adi-parallel-no.task_"* ]] ||  [[ $parProg == *"DRB045-doall1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB046-doall2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB047-doallchar-orig-no.task_"* ]] ||  [[ $parProg == *"DRB048-firstprivate-orig-no.task_"* ]] ||  [[ $parProg == *"DRB050-functionparameter-orig-no.task_"* ]] ||  [[ $parProg == *"DRB052-indirectaccesssharebase-orig-no.task_"* ]] ||  [[ $parProg == *"DRB053-inneronly1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB054-inneronly2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB057-jacobiinitialize-orig-no.task_"* ]] ||  [[ $parProg == *"DRB059-lastprivate-orig-no.task_"* ]] ||  [[ $parProg == *"DRB060-matrixmultiply-orig-no.task_"* ]] ||  [[ $parProg == *"DRB061-matrixvector1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB063-outeronly1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB064-outeronly2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB067-restrictpointer1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB068-restrictpointer2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB112-linear-orig-no.task_"* ]] ||  [[ $parProg == *"DRB113-default-orig-no.task_"* ]] ||  [[ $parProg == *"DRB170-nestedloops-orig-no.task_"* ]] || [[ $parProg == *"DRB062-matrixvector2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB065-pireduction-orig-no.task_"* ]] # only equivalent live vars
 # then
 #   continue
 # fi
 
  # if [[ $parProg == *"DRB049-fprintf-orig-no.task_"* ]]  # fprintf order
 # then
#    continue
#  fi

  cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
  echo $cmd
  time $cmd
done

for task in $(ls ../dataracebench-1.3.2/EqTasks/*.task_?.c) 
do 

 #  if [[ $parProg == *"DRB042-3mm-tile-no.task_"* ]] || [[ $parProg == *"DRB044-adi-tile-no.task_"* ]] || [[ $parProg == *"DRB056-jacobi2d-tile-no.task_"* ]] ||  [[ $parProg == *"DRB058-jacobikernel-orig-no.task_"* ]] ||  [[ $parProg == *"DRB070-simd1-orig-no.task_"* ]] ||  [[ $parProg == *"DRB071-targetparallelfor-orig-no.task_"* ]] ||  [[ $parProg == *"DRB093-doall2-collapse-orig-no.task_"* ]] ||  [[ $parProg == *"DRB097-target-teams-distribute-orig-no.task_"* ]] ||  [[ $parProg == *"DRB098-simd2-orig-no.task_"* ]]  ||  [[ $parProg == *"DRB099-targetparallelfor2-orig-no.task_"* ]] ||  [[ $parProg == *"DRB104-nowait-barrier-orig-no.task_"* ]] ||  [[ $parProg == *"DRB121-reduction-orig-no.task_"* ]] ||  [[ $parProg == *"DRB137-simdsafelen-orig-no.task_"* ]] ||  [[ $parProg == *"DRB141-reduction-barrier-orig-no.task_"* ]] ||  [[ $parProg == *"DRB172-critical2-orig-no.task_"* ]] # only equivalent live vars
 # then
 #   continue
 # fi
 
# if [[ $parProg == *"DRB075-getthreadnum-orig-yes.task_"* ]] || [[ $parProg == *"DRB103-master-orig-no.task_"* ]] || [[ $parProg == *"DRB126-firstprivatesections-orig-no.task_"* ]] || [[ $parProg == *"DRB131-taskdep4-orig-omp45-yes.task_"* ]] || [[ $parProg == *"DRB134-taskdep5-orig-omp45-yes.task_"* ]] || [[ $parProg == *"DRB142-acquirerelease-orig-yes.task_"* ]] || [[ $parProg == *"DRB143-acquirerelease-orig-no.task_"* ]] || [[ $parProg == *"DRB165-taskdep4-orig-omp50-yes.task_"* ]] || [[ $parProg == *"DRB168-taskdep5-orig-omp50-yes.task_"* ]]  || [[ $parProg == *"DRB094-doall2-ordered-orig-no.task_"* ]] || [[ $parProg == *"DRB132-taskdep4-orig-omp45-no.task_"* ]] || [[ $parProg == *"DRB133-taskdep5-orig-omp45-no.task_"* ]] || [[ $parProg == *"DRB141-reduction-barrier-orig-no.task_"* ]] || [[ $parProg == *"DRB158-missingtaskbarrier-orig-gpu-no.task_"* ]] || [[ $parProg == *"DRB166-taskdep4-orig-omp50-no.task_"* ]] || [[ $parProg == *"DRB167-taskdep4-orig-omp50-no.task_"* ]] # I/O (printf, fprint)
#  then
#    continue
#  fi

  cmd="civl verify -input_omp_thread_max=2 -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
  echo $cmd
  time $cmd
done
