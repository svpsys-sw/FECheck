#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MILCmkLOC=$(cat "MILCmk-location.txt")

taskExpr=$MILCmkLOC"equivalence_checking/*_task_*.c"
for task in $(ls $taskExpr) 
do 
  if [[ $parProg == *"QLA_D3_c1_veq_V_dot_V__task"* ]]
  then
    continue
  fi
  if [[ $parProg == *"QLA_F3_c1_veq_V_dot_V__task"* ]]
  then
    continue
  fi
  cmd="civl verify -input_omp_thread_max=2 -sysIncludePath=/usr/include/sys/:$MILCmkLOC -checkDivisionByZero=false -checkMemoryLeak=false -timeout=300 ../nondet_funs.c $task"
  echo $cmd
  time $cmd
done


