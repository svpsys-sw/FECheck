#!/bin/bash
# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# The '-rose:unparse_tokens' option tells ROSE to take extra care to preserve the source-code formatting from the input source file when producing the rose_xxx.cc output file
cmdLine="autoPar -rose:unparse_tokens -rose:autopar:no_aliasing -rose:autopar:enable_diff  -fopenmp "

cd "$( dirname "${BASH_SOURCE[0]}" )"

prefixfolder="../examples/"
folder=$prefixfolder"reduction/"
files=$folder"*_par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

prefixfolder="../FEVS-examples/"
folder=$prefixfolder"doall/"
files=$folder"*_par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

folder=$prefixfolder"reduction/"
files=$folder"*_par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

prefixfolder="../dataracebench-1.3.2/"

folder=$prefixfolder"DoAll/correct/"
files=$folder"*.par.c"
for prog in $(ls $files) 
do 
  if [[ $prog == *"DRB041-3mm-parallel-no.par.c" ]]
  then
    continue
  fi
  if [[ $prog == *"DRB112-linear-orig-no.par.c" ]]
  then
    continue
  fi
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

folder=$prefixfolder"DoAll/incorrect/"
files=$folder"*.par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

folder=$prefixfolder"Reduction/"
files=$folder"*.par.c"
for prog in $(ls $files) 
do 
  cmd="$cmdLine$prog"
  echo $cmd
  time $cmd
  rm "rose_"${prog:${#folder}:${#prog}}
rm *par.o
done

MILCmkLOC=$(cat "MILCmk-location.txt")
if [[ -d $MILCmkLOC"equivalence_checking/" ]]
then
  ./MILCmk-autopar.sh
fi
