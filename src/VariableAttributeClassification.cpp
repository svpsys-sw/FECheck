//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VariableAttributeClassification.h"
#include <assert.h>

  void VariableAttributeClassification::set_sharing_type(SharingType newSharing)
  {
    assert(newSharing != UNSET);
    if(sharingAttr == UNSET)
    {
      sharingAttr = newSharing;
    }
    else if(newSharing != sharingAttr)
    {
      if((sharingAttr == FIRSTPRIVATE && newSharing == LASTPRIVATE) || (sharingAttr == LASTPRIVATE && newSharing == FIRSTPRIVATE))
      {
        newSharing = FIRSTLASTPRIVATE;
      }
      else if(sharingAttr == SHARED)
      {
        sharingAttr = newSharing;
      } 
      else
      {
        // if more combinations or different data-sharing attributes are supported extend this case
        assert(0); 
      }
    }
  }

  SharingType VariableAttributeClassification::get_sharing_type()
  {
    assert(sharingAttr != UNSET);
    return sharingAttr;
  }

  bool VariableAttributeClassification::is_accessed_in_called_function()
  {
    return accessInCalledFunction;
  }

  bool VariableAttributeClassification::is_loop_counter()
  {
    return is_counter;
  }

  LoopType VariableAttributeClassification::get_loop_type()
  {
    assert(is_counter);
    return loopType;
  }

  void VariableAttributeClassification::mark_as_loop_counter(LoopType pLoopType)
  {
    assert(loopType == UNDETERMINED);
    loopType = pLoopType;
    is_counter = true;
  }

  void VariableAttributeClassification::set_access_in_called_function()
  {
    accessInCalledFunction = true;
  }

  void VariableAttributeClassification::set_used()
  {
    isRead = true;
  }

  void VariableAttributeClassification::set_reduction_op(ReductionOp op)
  {
    if(op != INVALID && red_op == INVALID)
    {
      red_op = op;
    }
  }

  ReductionOp VariableAttributeClassification::get_reduction_operation()
  {
    assert(red_op != INVALID);
    return red_op;
  }
