//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "DoAllChecker.h"
#include "GeneralCheckGen.h"
#include "PECConstants.h"
#include "DefUseAnalysis.h"
#include "DefUseAnalysis_perFunction.h"
#include <assert.h>
#include <ctype.h>
#include <sstream>

  void DoAllChecker::perform_pattern_checks(SgSourceFile* seqProg, SgSourceFile* parProg)
  {
    int numChecks = get_num_checks(seqProg);
    int nextID = numChecks + 1;
    std::string checkFile;
    GeneralCheckGen genCheck = GeneralCheckGen(require_output_vars());
    genCheck.init_include_Mgr(seqProg, parProg);
    
    std::set<std::string> non_shared_vars;
    genCheck.extract_nonshared_vars(non_shared_vars, seqProg, parProg);

    SgForStatement* seqFor;
    SgForStatement* parFor;

    std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> seqSeg, parSeg;
    std::vector<SgPragma*> omp_pragmas;

    for(int i = 1; i<=numChecks; i++)
    {
      std::cout<<"Start checking segment " + std::to_string(i) +"."<<std::endl;
      omp_pragmas.clear();

      seqSeg = get_start_end_pragmas(seqProg, i);
      parSeg = get_start_end_pragmas(parProg, i);
      parFor = NULL;

      if(!is_parallelized_for(seqSeg, parSeg, &parFor, omp_pragmas))
      {
        std::cout<<"Segment " + std::to_string(i) + " is not a " + get_pattern_name() + " pattern. You require a different checking method." <<std::endl; 
        continue;
      }

      assert(parFor);

      seqFor = isSgForStatement(SageInterface::getNextStatement(seqSeg.first));
      assert(seqFor);

      if(!is_proper_parallelization_setup(seqFor, seqSeg.second, parFor, parSeg.second, omp_pragmas)) 
      {
        std::cout<<"Failed to detect iteration independence of segment " + std::to_string(i) + ". You require a different checking method."<<std::endl; 
        continue;
      }

      // equivalence of loop iteration
      if(!is_syntactically_equal(seqFor->get_loop_body(), parFor->get_loop_body()))
      { 
        insert_eq_pragmas(seqFor->get_loop_body(), nextID);
        insert_eq_pragmas(parFor->get_loop_body(), nextID);

        /*// check pragma insertion
        AstTests::runAllTests(seqProg->get_project());   
        AstTests::runAllTests(parProg->get_project());*/

        checkFile = "bodyCheck_"+ std::to_string(nextID-numChecks) + ".c";
        genCheck.generate_single_checker_program(seqProg, parProg, non_shared_vars, checkFile, nextID);

        std::cout<<"Loop body of sequential and parallelized segment " + std::to_string(i) + " are syntactically different. You need to prove correctness of verification task " +  checkFile + " to prove equivalence of them."<<std::endl; 

        nextID++;
      }
      else
      {
        std::cout<<"Segment " + std::to_string(i) + " successfully checked."<<std::endl; 
      }
    }

  }

  bool DoAllChecker::contains_unallowed_loop_exits(SgForStatement* forStmt)
  {
    SgStatement* body = forStmt->get_loop_body();
    Rose_STL_Container<SgNode*> stmts;

    // check for return statements
    stmts = NodeQuery::querySubTree(body, V_SgReturnStmt); 
    if(stmts.size()>0)
    {
      return true;
    }

    // check for break statements
    stmts = NodeQuery::querySubTree(body, V_SgBreakStmt); 
    SgStatement* ancestorStmt;
    SgStatement* breakStmt;
    for (Rose_STL_Container<SgNode*>::iterator i = stmts.begin(); i != stmts.end(); i++)
    {
      breakStmt = isSgBreakStmt(*i);

      ancestorStmt = SageInterface::findEnclosingSwitch(breakStmt);
      if(ancestorStmt && SageInterface::isAncestor(body, ancestorStmt))
      {
        continue;
      }

      ancestorStmt = SageInterface::findEnclosingLoop(breakStmt); 
      if(ancestorStmt && SageInterface::isAncestor(body, ancestorStmt))
      {
        continue;
      }
  
      return true;
    }

    // check for goto statements
    SgLabelStatement* gotoGoal;
    stmts = NodeQuery::querySubTree(body, V_SgGotoStatement); 
    for (Rose_STL_Container<SgNode*>::iterator i = stmts.begin(); i != stmts.end(); i++)
    {
      gotoGoal = isSgGotoStatement(*i)->get_label();
      if(!(SageInterface::isAncestor(body, gotoGoal)))
      {
        return true;
      }
    }

    return false;
  }

  bool DoAllChecker::is_proper_parallelization_setup(SgForStatement* seqFor, SgPragmaDeclaration* seqEnd, SgForStatement* parFor, SgPragmaDeclaration* parEnd, std::vector<SgPragma*>& omp_pragmas)
  { 

    std::map<SgInitializedName*, VariableAttributeClassification> varsSeq;
    std::map<SgInitializedName*, VariableAttributeClassification> varsPar;

    // gather usage information
    if(!inspect_loop_code(seqFor, seqEnd, varsSeq, false) || !inspect_loop_code(parFor, parEnd, varsPar, true))
    {
      std::cout<<"Loop counter is live afterwards."<<std::endl;
      return false; // loop counter is live afterwards
    }

    std::map<std::string, SgInitializedName*> parVarNameToDecl;
    std::string varName;
    SgInitializedName* loop_counter = SageInterface::getLoopIndexVariable(parFor);
    assert(loop_counter);
    std::string counter_name = loop_counter->get_name().getString();
    for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = varsPar.begin(); it!=varsPar.end(); it++)
    {
      if(it->second.must_declare())
      {
        varName = it->first->get_name().getString();
        assert(parVarNameToDecl.find(varName)==parVarNameToDecl.end() && (it->first == loop_counter || varName.compare(counter_name) != 0));
        parVarNameToDecl[varName] = it->first;
      }
    }

    for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = varsPar.begin(); it!=varsPar.end(); it++)
    {
      if(!(it->second.must_declare()))
      {
        varName = it->first->get_name().getString();
        if(parVarNameToDecl.find(varName)!=parVarNameToDecl.end() || (it->first != loop_counter && varName.compare(counter_name) == 0))
        {
          std::cout<<"Do not support redeclaration of variable "<<varName<<" in parallelized program."<<std::endl; 
          return false; // do not allow to redeclare a variable outside of the scope of the loop body, might make independence check invalid
        }
      }
    }

    std::set<std::string> seqVarsForOutput; 
    for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = varsSeq.begin(); it!=varsSeq.end(); it++)
    {
      if(it->second.is_live() && it->second.must_declare())
      {
        varName = it->first->get_name().getString();
        if(parVarNameToDecl.find(varName) == parVarNameToDecl.end())
        {
          std::cout<<"Parallelized program misses output variable "<<varName<<" of the sequential program."<<std::endl;
          return false;
        }
        seqVarsForOutput.insert(varName);
      }
    }

    // add data sharing attributes from pragma
    for(std::vector<SgPragma*>::iterator it = omp_pragmas.begin(); it != omp_pragmas.end(); it++)
    {
      determine_data_sharing_from_pragma(*it, varsPar, parVarNameToDecl);
    }

    // check for unallowed break and return
    if(contains_unallowed_loop_exits(seqFor) || contains_unallowed_loop_exits(parFor))
    {
      std::cout<<"For loop can be exited before all iterations are performed, e.g., via break, return or goto statement."<<std::endl;
      return false;
    }

    // check that iteration not dependent on loop execution
    if(!(is_fixed_loop_iteration(seqFor, varsSeq) && is_fixed_loop_iteration(parFor, varsPar)))
    {
      return false;
    }

    return check_parallelized_loop(parFor, varsPar, seqVarsForOutput);
  }

  bool DoAllChecker::check_parallelized_loop(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, std::set<std::string>& seqVarsForOutput)
  {
    // check iteration independence 
    if(!are_iterations_independent(parFor, varsPar)) 
    {
      return false;
    }

    // check loop iteration input and loop output availability
    bool is_avail = true;
    for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = varsPar.begin(); it!=varsPar.end(); it++)
    {
      is_avail = ensures_same_input(it->second);
      is_avail = is_avail && ensures_same_output(it->first, it->second, seqVarsForOutput.find(it->first->get_name().getString())!=seqVarsForOutput.end());
      if(!is_avail)
      {
        std::cout<<"Wrong data sharing attribute for variable "<<it->first->unparseToString()<<" because it does not provide proper input or output values to the loop iteration."<<std::endl;
        return false;
      }
    }
    return true;
  }


  bool DoAllChecker::are_iterations_independent(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar)
  {
   bool collectedReferenceInfo = false;
   std::map<SgInitializedName*, std::vector<SgPntrArrRefExp*>> arrIndices;
   std::map<SgInitializedName*, bool> onlyArrayRefs;

   for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator varIt = varsPar.begin(); varIt != varsPar.end(); varIt++)
   {
     if(varIt->second.is_written() && varIt->second.must_declare())
     {
       if(helper.is_global(varIt->first->get_scope()) && varIt->second.is_accessed_in_called_function())
       {
           std::cout<<"Independence check fails due to modified global variable "<<varIt->first->unparseToString()<<" accessed in function."<<std::endl;
           return false; 
       }
       if(SageInterface::isScalarType(varIt->first->get_type()))
       {
         if(varIt->second.get_sharing_type() == SHARED) // TODO
         {
           std::cout<<"Wrong data sharing attribute for variable "<<varIt->first->unparseToString()<<" because it cannot be accessed independently in the different loop iterations."<<std::endl;
           return false; 
         }
       } 
       else
       {
         if(isSgArrayType(varIt->first->get_type()))
         { 
           // non-shared variable length arrays (arrays that use an integer expression, which can not be determined at compile time) may not be supported
           if(isSgArrayType(varIt->first->get_type())->get_is_variable_length_array() || varIt->second.get_sharing_type() == SHARED)
           {
             if(!collectedReferenceInfo)
             {
               collectIndices(parFor, arrIndices);
               collectedReferenceInfo = true;
             }

             if(arrIndices.find(varIt->first) == arrIndices.end()) // e.g., complete array is passed as pointer
             {
               if(varIt->second.is_written())
               {
                 std::cout<<"Failed to show that accesses to array variable "<<varIt->first->unparseToString()<<" are independent between loop iterations."<<std::endl;
                 return false;
               }
             }
             else if(!are_array_accesses_independent(arrIndices[varIt->first], varsPar, parFor))
             {
               std::cout<<"Failed to show that accesses to array variable "<<varIt->first->unparseToString()<<" are independent between loop iterations."<<std::endl;
               return false;
             }
           }
         }
         else if(SageInterface::isPointerType(varIt->first->get_type()))
         {
           if(onlyArrayRefs.size() == 0)
           {
             inspect_pointer_usage(parFor, onlyArrayRefs);
           }

           if(!onlyArrayRefs[varIt->first])
           {
             std::cout<<"Pointer variable "<<varIt->first->unparseToString()<<" accessed in a way not supported by the independence check."<<std::endl;
             return false;
           }

           if(!collectedReferenceInfo)
           {
              collectIndices(parFor, arrIndices);
              collectedReferenceInfo = true;          
           }

           assert(arrIndices.find(varIt->first) != arrIndices.end());
           if(!are_array_accesses_independent(arrIndices[varIt->first], varsPar, parFor))
           {
             std::cout<<"Failed to show that accesses to pointer variable "<<varIt->first->unparseToString()<<" are independent between loop iterations."<<std::endl;
             return false;
           }
         }
         else
         {
           // currently, only scalar variables, arrays and pointers are supported
           // TODO need to additional code to support typedefs, structs, etc. if needed
           assert(0);
           return false; 
         }
       }
     }
   }

    return true; 
  }

  bool DoAllChecker::are_array_accesses_independent(std::vector<SgPntrArrRefExp*>& varArrIndices, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgForStatement* parFor)
  { 
    SgInitializedName* loop_counter = SageInterface::getLoopIndexVariable(parFor);
    assert(loop_counter);

    if(varArrIndices.size() < 1)
    {
      return true;
    }

    if(varArrIndices.size() == 1)
    {
      return differentAccessesPerIteration(varArrIndices[0], varsPar, loop_counter);
    }
    else
    {
      std::set<std::pair<SgPntrArrRefExp*, SgPntrArrRefExp*>> comparedIndices;
      SgPntrArrRefExp* arrO ;
      SgPntrArrRefExp* arrI;
      SgExpression* indexO;
      SgExpression* indexI;
      bool are_independent;

      for(std::vector<SgPntrArrRefExp*>::iterator oIt = varArrIndices.begin(); oIt != varArrIndices.end(); oIt++)
      {
        for(std::vector<SgPntrArrRefExp*>::iterator iIt = varArrIndices.begin(); iIt != varArrIndices.end(); iIt++)
        {
          if(comparedIndices.find(std::pair<SgPntrArrRefExp*, SgPntrArrRefExp*>(*oIt, *iIt)) != comparedIndices.end())
          {
            continue;
          }
          comparedIndices.insert(std::pair<SgPntrArrRefExp*, SgPntrArrRefExp*>(*oIt, *iIt));
          comparedIndices.insert(std::pair<SgPntrArrRefExp*, SgPntrArrRefExp*>(*iIt, *oIt));

          if(*iIt == *oIt)
          {
            if(!(differentAccessesPerIteration(*iIt, varsPar, loop_counter)))
            {
              return false;
            }
          }
          else
          { 
            arrO = *oIt;
            arrI = *iIt;

            if(count_reference_depth(arrO) != count_reference_depth(arrI)) // check for same size, currently do not support different depths
            {
              return false;
            }

            are_independent = false;
            do
            {
              indexO = arrO->get_rhs_operand_i();
              indexI = arrI->get_rhs_operand_i();

              if(indexO->unparseToString().compare(indexI->unparseToString())==0)
              {
                if(checkIndependenceOfIdenticalIndices(indexO, varsPar, loop_counter))
                {
                  are_independent = true;
                }
              }
              else
              {
                if(checkIndependenceOfIndexExpressions(indexO, indexI, varsPar, loop_counter, parFor))
                {
                  are_independent = true;
                }
              }
            }
            while((arrO = isSgPntrArrRefExp(arrO->get_lhs_operand())) && (arrI = isSgPntrArrRefExp(arrI->get_lhs_operand())) && !are_independent);

            if(!are_independent)
            {
              std::cout<< "Accesses " << (*oIt)->unparseToString() << " and " <<(*iIt)->unparseToString() << " are not independent." <<std::endl;
              return false;
            }
          }
        }
      }
    }
    return true;
  }

  unsigned int DoAllChecker::count_reference_depth(SgPntrArrRefExp* arr)
  {
    unsigned int counter = 0;

    while(arr)
    {
      counter++;
      arr = isSgPntrArrRefExp(arr->get_lhs_operand());
    }

    return counter;
  }

  bool DoAllChecker::differentAccessesPerIteration(SgPntrArrRefExp* arr, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter)
  {
    assert(arr);
    
    do 
    {
      if(checkIndependenceOfIdenticalIndices(arr->get_rhs_operand_i(), varsPar, loop_counter))
      {
        return true;
      }
    } 
    while((arr = isSgPntrArrRefExp(arr->get_lhs_operand())));
      
    return false;
  }

  bool DoAllChecker::checkIndependenceOfIdenticalIndices(SgExpression* index, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter)
  {
    std::vector<SgExpression*> stack;
    SgExpression* expr;
    SgInitializedName* var;

    stack.push_back(index);
    bool contains_counter = false;
    bool not_modified_others = true;
    bool only_lina = true;
    bool counter_multiple = false;
       
    while(stack.size() > 0 && not_modified_others && only_lina && !(counter_multiple))
    {
      expr = stack.back();
      stack.pop_back();
      if(isSgValueExp(expr))
      {
        continue;
      }
      else if(isSgVarRefExp(expr))
      {
        var = isSgVarRefExp(expr)->get_symbol()->get_declaration();
        if(var == loop_counter)
        {
          if(contains_counter)
          { // loop counter occurs multiple times, currently not supported because expressions like i-i can lead to same values of index expressions
            counter_multiple = true;
            break;
          }
          contains_counter = true;
        }
        else
        {
          if(varsPar[var].is_written())
          {
            not_modified_others = false;
          }
        }
      }
      else if(isSgBinaryOp(expr))
      {
        if(!isSgAddOp(expr) && !isSgSubtractOp(expr))
        {
          only_lina = false;
        }
        stack.push_back(isSgBinaryOp(expr)->get_lhs_operand());
        stack.push_back(isSgBinaryOp(expr)->get_rhs_operand());
      }
      else if(isSgMinusOp(expr) || isSgUnaryAddOp(expr) || isSgMinusMinusOp(expr) || isSgPlusPlusOp(expr))
      {
        stack.push_back(isSgUnaryOp(expr)->get_operand());
      }
      else
      {
        // currently only support expressions that are constructed from variables and values using + and i connectivities
        assert(0); 
      }
    }

    if(contains_counter)
    {
      if((only_lina && not_modified_others && !(counter_multiple)) || checkIndependenceOfIndexExpressions(index, index, varsPar, loop_counter, NULL))
      {
        return true;
      }
    }

    return false;
  }

  z3::expr DoAllChecker::translateIntoZ3BoolExpression(z3::context& ctx, SgInitializedName* loop_counter, SgExpression* codeExpr, bool rename)
  {
    assert(SageInterface::isStrictIntegerType(loop_counter->get_type())); // currently only support integer variable types in index expressions
    z3::expr var = ctx.bv_const(rename? (loop_counter->get_name().getString() + "@r").c_str(): loop_counter->get_name().getString().c_str(), arch); 
    SgValueExp* valExp = NULL;

    if(isSgBinaryOp(codeExpr))
    {
      if(isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(codeExpr)->get_rhs_operand()) && isSgVarRefExp(isSgBinaryOp(codeExpr)->get_rhs_operand())->get_symbol()->get_declaration() == loop_counter)
      {
        valExp = isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand());
      }
      else if(isSgValueExp(isSgBinaryOp(codeExpr)->get_rhs_operand()) && isSgVarRefExp(isSgBinaryOp(codeExpr)->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(codeExpr)->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
      {
        valExp = isSgValueExp(isSgBinaryOp(codeExpr)->get_rhs_operand());
      }
      else
      {
        return ctx.bool_val(true); // only works properly if negation is not supported
      }

      assert(valExp);
      z3::expr val = translateIntoZ3ValExpression(ctx, valExp);

      if(isSgEqualityOp(codeExpr))
      {
        return var == val;
      }
      if(isSgGreaterOrEqualOp(codeExpr))
      {
        if(isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand()))
        {
          return val >= var;
        }
        else
        {
          return var >= val;
        }
      }
      if(isSgGreaterThanOp(codeExpr))
      {
        if(isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand()))
        {
          return val > var;
        }
        else
        {
          return var > val;
        }
      }
      if(isSgLessOrEqualOp(codeExpr))
      {
        if(isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand()))
        {
          return val <= var;
        }
        else
        {
          return var <= val;
        }
      }
      if(isSgLessThanOp(codeExpr))
      {
        if(isSgValueExp(isSgBinaryOp(codeExpr)->get_lhs_operand()))
        {
          return val < var;
        }
        else
        {
          return var < val;
        }
      }
      if(isSgNotEqualOp(codeExpr))
      {
        return var != val;
      }
    }
    return ctx.bool_val(true); // only works properly if negation is not supported
  }

  z3::expr DoAllChecker::translateIntoZ3ValExpression(z3::context& ctx, SgValueExp* codeExpr)
  {
    if(isSgIntVal(codeExpr) || isSgLongIntVal(codeExpr) || isSgLongLongIntVal(codeExpr) || isSgShortVal(codeExpr))
    {
      int64_t val;
      if(isSgIntVal(codeExpr))
      {
        val = isSgIntVal(codeExpr)->get_value();
      }
      else if(isSgLongIntVal(codeExpr))
      {
        val = isSgLongIntVal(codeExpr)->get_value();
      }
      else if(isSgLongLongIntVal(codeExpr))
      {
        val = isSgLongLongIntVal(codeExpr)->get_value();
      }
      else if(isSgShortVal(codeExpr))
      {
        val = isSgShortVal(codeExpr)->get_value();
      }

      return ctx.bv_val(val, arch);
    }
    else
    {
      uint64_t val;
      if(isSgUnsignedIntVal(codeExpr))
      {
        val = isSgUnsignedIntVal(codeExpr)->get_value();
      }
      else if(isSgUnsignedLongLongIntVal(codeExpr))
      {
        val = isSgUnsignedLongLongIntVal(codeExpr)->get_value();
      }
      else if(isSgUnsignedLongVal(codeExpr))
      {
        val = isSgUnsignedLongVal(codeExpr)->get_value();
      }
      else if(isSgUnsignedShortVal(codeExpr))
      {
        val = isSgUnsignedShortVal(codeExpr)->get_value();
      }
      else
      {
        assert(0); // not supported
      }

      return ctx.bv_val(val, arch);
    }
  }

  z3::expr DoAllChecker::translateIntoZ3Expression(z3::context& ctx, SgExpression* codeExpr, SgInitializedName* loop_counter, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, bool renameIfModified)
  {
    if(isSgValueExp(codeExpr))
    {
      return translateIntoZ3ValExpression(ctx, isSgValueExp(codeExpr));
    }
    else if(isSgVarRefExp(codeExpr))
    {
      std::string varName;
      SgInitializedName* var = isSgVarRefExp(codeExpr)->get_symbol()->get_declaration();
      if(renameIfModified && (var == loop_counter || varsPar[var].is_written()))
      {
        varName = var->get_name().getString() + "@r";
      }
      else
      {
        varName = var->get_name().getString();
      }
      if(SageInterface::isStrictIntegerType(var->get_type()))
      {
        return ctx.bv_const(varName.c_str(), arch); 
      }
      else
      {
        // currently only support integer variable types in index expressions
        assert(0);
      }
    }
    else if(isSgBinaryOp(codeExpr))
    {
      if(isSgAddOp(codeExpr) || isSgSubtractOp(codeExpr) || isSgMultiplyOp(codeExpr) || isSgDivideOp(codeExpr) || isSgModOp(codeExpr) || isSgBitAndOp(codeExpr) || isSgBitOrOp(codeExpr) || isSgBitXorOp(codeExpr) || isSgLshiftOp(codeExpr) ||isSgRshiftOp(codeExpr) || isSgIntegerDivideOp(codeExpr) || isSgLeftDivideOp(codeExpr))
      {
        z3::expr operandL = translateIntoZ3Expression(ctx, isSgBinaryOp(codeExpr)->get_lhs_operand(), loop_counter, varsPar, renameIfModified);
        z3::expr operandR = translateIntoZ3Expression(ctx, isSgBinaryOp(codeExpr)->get_rhs_operand(), loop_counter, varsPar, renameIfModified);

        if(isSgAddOp(codeExpr))
        {
          return operandL + operandR;
        }
        else if(isSgSubtractOp(codeExpr))
        {
          return operandL - operandR;
        }
        else if(isSgMultiplyOp(codeExpr))  
        {
          return operandL * operandR;
        }
        else if(isSgDivideOp(codeExpr)) 
        {
          return operandL / operandR;
        }
        else if(isSgModOp(codeExpr))
        {
          return smod(operandL, operandR);
        }
        else if(isSgBitAndOp(codeExpr)) 
        {
          return operandL & operandR;
        }
        else if(isSgBitOrOp(codeExpr)) 
        {
          return operandL | operandR;
        }
        else if(isSgBitXorOp(codeExpr)) 
        {
          return operandL ^ operandR;
        }
        else if(isSgLshiftOp(codeExpr)) 
        {
          return operandL < operandR;
        }
        else if(isSgRshiftOp(codeExpr)) 
        {
          return operandL > operandR;
        }
        else if(isSgIntegerDivideOp(codeExpr)) // div operator?
        {
          return operandL / operandR;
        }
        else //isSgLeftDivideOp(codeExp) remainder ?
        {
          return rem(operandL, operandR);
        }
      } 
      else
      {
        // need to extend if other binary expressions should be suppported
        assert(0); 
      }
    }
    else if(isSgMinusOp(codeExpr))
    {
      return -translateIntoZ3Expression(ctx, isSgUnaryOp(codeExpr)->get_operand(), loop_counter, varsPar, renameIfModified);
    }
    else if(isSgUnaryAddOp(codeExpr))
    {
      return translateIntoZ3Expression(ctx, isSgUnaryOp(codeExpr)->get_operand(), loop_counter, varsPar, renameIfModified); 
    }
    /*else if(isSgMinusMinusOp(codeExpr) || isSgPlusPlusOp(codeExpr)) // ++i, i++, --i, i--, required that it occurs at most once per variable
    {
       z3::expr operand = translateIntoZ3Expression(ctx, isSgUnaryOp(codeExpr)->get_operand(), loop_counter, varsPar, renameIfModified); 
       if(isSgUnaryOp(codeExpr)->get_mode() == SgUnaryOp::postfix)
       {
         z3::expr oneCst = ctx.bv_val(1, arch);
         if(isSgPlusPlusOp(codeExpr))
         {
           return operand + 1;
         }
         else
         {
           return operand - 1;
         }
       }
       else
       {
         return operand;
       }   
    }*/
    // extend if further expression types should be supported
    assert(0); 
    return ctx.bool_val(false);
  }

  bool DoAllChecker::checkIndependenceOfIndexExpressions(SgExpression* index1, SgExpression* index2, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter, SgForStatement* parFor)
  {
    assert(index1 && index2);

    // check that i++, etc. only occurs once per variable, does not work with aliasing
    std::set<SgExpression*> seenOperands;
    SgExpression* operand;
    Rose_STL_Container<SgNode*> stmts;

    stmts = NodeQuery::querySubTree(index1, V_SgMinusMinusOp); 
    for(Rose_STL_Container<SgNode*>::iterator it = stmts.begin(); it != stmts.end(); it++)
    {
      operand = isSgMinusMinusOp(*it)->get_operand();
      if(seenOperands.find(operand) == seenOperands.end())
      {
        seenOperands.insert(operand);
      }
      else
      {
        std::cout<<"Multiple usages of "<<operand->unparseToString()<<" in array index not supported."<<std::endl;
        return false;
      }
    }

    stmts = NodeQuery::querySubTree(index1, V_SgPlusPlusOp); 
    for(Rose_STL_Container<SgNode*>::iterator it = stmts.begin(); it != stmts.end(); it++)
    {
      operand = isSgPlusPlusOp(*it)->get_operand();
      if(seenOperands.find(operand) == seenOperands.end())
      {
        seenOperands.insert(operand);
      }
      else
      {
        std::cout<<"Multiple usages of "<<operand->unparseToString()<<" in array index not supported."<<std::endl;
        return false;
      }
    }

    seenOperands.clear();

    stmts = NodeQuery::querySubTree(index2, V_SgMinusMinusOp); 
    for(Rose_STL_Container<SgNode*>::iterator it = stmts.begin(); it != stmts.end(); it++)
    {
      operand = isSgMinusMinusOp(*it)->get_operand();
      if(seenOperands.find(operand) == seenOperands.end())
      {
        seenOperands.insert(operand);
      }
      else
      {
        std::cout<<"Multiple usages of "<<operand->unparseToString()<<" in array index not supported."<<std::endl;
        return false;
      }
    }

    stmts = NodeQuery::querySubTree(index2, V_SgPlusPlusOp); 
    for(Rose_STL_Container<SgNode*>::iterator it = stmts.begin(); it != stmts.end(); it++)
    {
      operand = isSgPlusPlusOp(*it)->get_operand();
      if(seenOperands.find(operand) == seenOperands.end())
      {
        seenOperands.insert(operand);
      }
      else
      {
        std::cout<<"Multiple usages of "<<operand->unparseToString()<<" in array index not supported."<<std::endl;
        return false;
      }
    }

    z3::context ctx;
    z3::solver sol = z3::solver(ctx);

    // convert into formula 
    sol.add(translateIntoZ3Expression(ctx, index1, loop_counter, varsPar, false) == translateIntoZ3Expression(ctx, index2, loop_counter, varsPar, true));
    if(SageInterface::isStrictIntegerType(loop_counter->get_type()))
    {
      sol.add(ctx.bv_const(loop_counter->get_name().getString().c_str(), arch) != ctx.bv_const((loop_counter->get_name().getString()+ "@r").c_str(), arch));
    }
    // TODO improvement: add restriction for loop due to initialization and test expression (more cases, more precise)
    if(parFor) 
    {
      if(parFor->get_init_stmt().size()==1)
      {        
        SgValueExp* valExp = NULL;
        if(isSgVariableDeclaration((parFor->get_init_stmt())[0]) && isSgAssignInitializer(loop_counter->get_initializer()) && SageInterface::isAncestor((parFor->get_init_stmt())[0], loop_counter->get_initializer()))
        {
          valExp = isSgValueExp(isSgAssignInitializer(loop_counter->get_initializer())->get_operand());
        } 
        else if(isSgExprStatement((parFor->get_init_stmt())[0]) && isSgAssignOp(isSgExprStatement((parFor->get_init_stmt())[0])->get_expression()) && isSgVarRefExp(isSgBinaryOp(isSgExprStatement((parFor->get_init_stmt())[0])->get_expression())->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(isSgExprStatement((parFor->get_init_stmt())[0])->get_expression())->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
        {
          valExp = isSgValueExp(isSgBinaryOp(isSgExprStatement((parFor->get_init_stmt())[0])->get_expression())->get_rhs_operand());
        }

        if(valExp)
        { 
          if(varsPar[loop_counter].get_loop_type() == INCREASE)
          {
            sol.add(ctx.bv_const(loop_counter->get_name().getString().c_str(), arch) >= translateIntoZ3ValExpression(ctx, valExp));
            sol.add(ctx.bv_const((loop_counter->get_name().getString()+"@r").c_str(), arch) >= translateIntoZ3ValExpression(ctx, valExp));
          }
          if(varsPar[loop_counter].get_loop_type() == DECREASE)
          {
            sol.add(ctx.bv_const(loop_counter->get_name().getString().c_str(), arch) <= translateIntoZ3ValExpression(ctx, valExp));
            sol.add(ctx.bv_const((loop_counter->get_name().getString() + "@r").c_str(), arch) <= translateIntoZ3ValExpression(ctx, valExp));
          }
        }
      }

      SgExpression* test_expr = parFor->get_test_expr();// TODO store this information elsewhere?
      if(isSgBinaryOp(test_expr) && ((isSgVarRefExp(isSgBinaryOp(test_expr)->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(test_expr)->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter && isSgValueExp(isSgBinaryOp(test_expr)->get_rhs_operand())) || (isSgVarRefExp(isSgBinaryOp(test_expr)->get_rhs_operand()) && isSgVarRefExp(isSgBinaryOp(test_expr)->get_rhs_operand())->get_symbol()->get_declaration() == loop_counter && isSgValueExp(isSgBinaryOp(test_expr)->get_lhs_operand()))))
      { 
        sol.add(translateIntoZ3BoolExpression(ctx, loop_counter, test_expr, true));
        sol.add(translateIntoZ3BoolExpression(ctx, loop_counter, test_expr, false));
      }
    }

    bool result;

    switch (sol.check()) {
      case z3::unsat: 
        result = true;
        break;
      case z3::sat:  
        result = false;   
        //std::cout << "Index expressions " << index1->unparseToString() << " and " <<index2->unparseToString() << " are not independent." <<std::endl; break;
      case z3::unknown: 
        result = false;
        std::cout << "SMT solver failed to detect independence." <<std::endl; break;
    }

    return result;
  }

  void DoAllChecker::collectIndices(SgForStatement* parFor, std::map<SgInitializedName*, std::vector<SgPntrArrRefExp*>>& allArrIndices)
  {
    std::map<SgInitializedName*, std::set<std::string>> seenIndices;

    Rose_STL_Container<SgNode*> arrayRefs = NodeQuery::querySubTree(parFor->get_loop_body(), V_SgPntrArrRefExp); 
    SgPntrArrRefExp* ref;
    SgInitializedName* var;
    for (Rose_STL_Container<SgNode*>::iterator i = arrayRefs.begin(); i != arrayRefs.end(); i++)
    {
      ref = isSgPntrArrRefExp(*i);
      if(ref->get_parent() && isSgPntrArrRefExp(ref->get_parent()))
      {
        continue;
      }

      var = SageInterface::convertRefToInitializedName(ref);
      assert(var); 
      if(seenIndices.find(var) == seenIndices.end())
      {
        seenIndices[var] = std::set<std::string>();
        allArrIndices[var] = std::vector<SgPntrArrRefExp*>();
      }

      if(seenIndices[var].find(ref->unparseToString()) == seenIndices[var].end())
      {
        allArrIndices[var].push_back(ref);
        seenIndices[var].insert(ref->unparseToString());
      }
    }
  }

  void DoAllChecker::inspect_pointer_usage(SgForStatement* parFor, std::map<SgInitializedName*, bool>& onlyArrayRefs)
  { // parent of varRef seems to be the declaration
    SgNode* ancestor;
    SgInitializedName* var;
    std::vector<SgVarRefExp*> varRefs;
    SageInterface::collectVarRefs (parFor, varRefs); 
    for(std::vector<SgVarRefExp*>::iterator refIt = varRefs.begin(); refIt != varRefs.end(); refIt++)
    {
      var = (*refIt)->get_symbol()->get_declaration();
      if(SageInterface::isPointerType(var->get_type()))
      {
        if(onlyArrayRefs.find(var) == onlyArrayRefs.end())
        {
          onlyArrayRefs[var] = true;
        }
        ancestor = (*refIt)->get_parent();
        while(ancestor)
        {
          if(isSgPntrArrRefExp(ancestor))
          {
            if(!SageInterface::isPointerType(isSgPntrArrRefExp(ancestor)->get_type()))
            {
              ancestor = NULL;
            }
            else
            {
              ancestor = ancestor->get_parent();
            }
          }
          else
          {
            // TODO be more precise here
            onlyArrayRefs[var] = false;
            ancestor = NULL;
          }
        }
      }
    }
  }

  bool DoAllChecker::inspect_loop_code(SgForStatement* forStmt, SgPragmaDeclaration* end, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized)
  {
    assert(forStmt);
    std::map<std::string, SgInitializedName*> nameToDecl;
    bool result = true;

    DefUseAnalysis* defuse = new DefUseAnalysis(SageInterface::getProject(forStmt));
    DefUseAnalysisPF* defusePF = new DefUseAnalysisPF(false, defuse);
    bool abort = false;
    SgFunctionDefinition* funDef = SageInterface::getEnclosingFunctionDefinition(forStmt, true);	
    defusePF->run(funDef, abort);
    assert(!abort);

    LivenessAnalysis* liveA = new LivenessAnalysis(false, defuse);
    abort = false;
    liveA->run(funDef, abort);
    assert(!abort);

    // inspect loop body 
    SgStatement* loop_body = forStmt->get_loop_body(); 
    // check if variable used before definition in code fragement
    std::vector<SgVarRefExp*> varRefs;
    // only references, not declarations
    SageInterface::collectVarRefs (loop_body, varRefs); 
    analyze_vars(varRefs, vars, isParallelized, forStmt, defuse, liveA);

    // handle for head
    SgInitializedName* loop_counter = SageInterface::getLoopIndexVariable(forStmt);
    if(vars.find(loop_counter) != vars.end())
    {
      vars[loop_counter].unset_used_before();
    }

    std::map<std::string, std::set<int>> calledFuns;
    bool externalCall = false;
    analyze_function_calls(forStmt, vars, calledFuns, &externalCall, true, defuse, isParallelized);
   
    if(externalCall)
    {
      insert_all_global_vars(SageInterface::getGlobalScope(forStmt), vars, isParallelized); // TODO more precise here
      result = false; // external functions may not be executed in parallel 
    }
 
    SgFunctionDefinition* encloseFun = SageInterface::getEnclosingFunctionDefinition(forStmt);
    std::set<SgInitializedName*> pointerParam;
    if(encloseFun)
    {
      for(std::vector<SgInitializedName*>::iterator paramIt = encloseFun->get_declaration()->get_args().begin(); paramIt != encloseFun->get_declaration()->get_args().end(); paramIt++)
      {
        if(SageInterface::isPointerType((*paramIt)->get_type()))
        {
          pointerParam.insert(*paramIt);
        }
      }
    }
    for(std::map<SgInitializedName*, VariableAttributeClassification>::iterator it = vars.begin(); it != vars.end(); it++)
    {
      // conservatively, make all global variables live 
      if(helper.is_global(it->first->get_scope()))
      {
        it->second.set_live_after();
      }
      // conservatively, make parameters that are pointers live 
      if(pointerParam.find(it->first)!=pointerParam.end()) 
      {
        it->second.set_live_after();
      }
      // conservatively treat all types that we know of are not properly handed by live variable analysis
      if(missed_by_LV_analysis(it->first->get_type()))
      {
        it->second.set_live_after();
      }
      
    }

    // set live after segment (only local information, global variables and parameters passed might be live and are considered elsewhere)
    std::vector<SgInitializedName*> liveAfter = liveA->getOut(end); 
    result = consider_live_vars(vars, liveAfter, loop_counter);

    // declaration handling
    std::vector<SgInitializedName*> declaredVars;
    Rose_STL_Container<SgNode*> decls = NodeQuery::querySubTree (forStmt, V_SgVariableDeclaration);
    for (Rose_STL_Container<SgNode*>::iterator it = decls.begin(); it != decls.end(); it++)
    {
      declaredVars = isSgVariableDeclaration(*it)->get_variables();
      for(std::vector<SgInitializedName*>::iterator declIt = declaredVars.begin(); declIt != declaredVars.end(); declIt++)
      {
        vars[(*declIt)].unset_declaration();
        if(*declIt != loop_counter)
        {
          vars[(*declIt)].set_modified();
        }
      }
    }

    delete defusePF;
    delete defuse;
    delete liveA;

    if(loop_counter && (helper.is_global(loop_counter->get_scope()) || pointerParam.find(loop_counter)!=pointerParam.end())) // loop counter is always modified and if global or is pointer parameter we assume that it is live afterwards
    {
      return false;
    }

    return result;
  }

  void DoAllChecker::insert_all_global_vars(SgGlobal* global_scope, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized)
  {
    SgVariableDeclaration* varDecl;
    assert(global_scope);

    for(std::vector<SgDeclarationStatement*>::iterator globalIt = global_scope->get_declarations().begin(); globalIt != global_scope->get_declarations().end(); globalIt++)
    { 
      varDecl = isSgVariableDeclaration(*globalIt);
      if(!varDecl)
      {
        continue;
      }
      for(std::vector<SgInitializedName*>::iterator varIt = varDecl->get_variables().begin(); varIt != varDecl->get_variables().end(); varIt++) 
      { 
        if(!helper.shouldIgnoreGlobal((*varIt)->get_name().getString())) // filter global system variables, e.g., introduced by stdio.h
        { 
          /* conservative assumptions
           * (a) global variables might be modified by any function (internal or external)
           * (b) global variables are live, i.e., might be used by another function afterwards
           * (c) global variables are used before, i.e., do not check that code segments defines global variable before any function is called
           */
          if(vars.find(*varIt) == vars.end())
          {
            vars[*varIt] =  VariableAttributeClassification();
            if(isParallelized)
            {
              determine_default_data_sharing(NULL, vars, *varIt);
            }
          }
          vars[*varIt].set_modified();
          vars[*varIt].set_live_after();
          vars[*varIt].set_used_before();
          vars[*varIt].set_access_in_called_function();
        }
      }
    }
  }

  bool DoAllChecker::consider_live_vars(std::map<SgInitializedName*, VariableAttributeClassification>& vars, std::vector<SgInitializedName*>& liveVars, SgInitializedName* loop_counter)
  {
    for(std::vector<SgInitializedName*>::iterator liveIt = liveVars.begin(); liveIt != liveVars.end(); liveIt++) // live variable analysis does not work properly sometimes
    { 
      if(*liveIt == loop_counter)
      {
        return false;
      }
      /* loop_counter == NULL, check which variables are live for next iteration, ignore those that are known to be declared before used (workaround for problems with for loops) */
      if(vars.find(*liveIt) != vars.end() && (loop_counter != NULL || vars[*liveIt].must_initialize())) 
      {
        vars[*liveIt].set_live_after();
      } 
    }
    return true;
  }

  void DoAllChecker::analyze_vars(std::vector<SgVarRefExp*>& varRefs, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized, SgForStatement* forStmt, DefUseAnalysis* defuse, LivenessAnalysis* liveA)
  {   
    std::vector<SgNode*> usesNodes, defsNodes;
    std::vector<std::pair<SgInitializedName*, SgNode*>> defs = defuse->getDefMultiMapFor(forStmt->get_loop_body());
    std::set<std::pair<SgInitializedName*, SgNode*>> defsBefore;
    defsBefore.insert(defs.begin(), defs.end());

    std::vector<SgInitializedName*> liveAtLoopIterationStart = liveA->getIn(forStmt->get_loop_body()); // use liveA->getOut(forStmt->get_loop_body()) instead?
    std::set<SgInitializedName*> liveSet;
    liveSet.insert(liveAtLoopIterationStart.begin(), liveAtLoopIterationStart.end());

    std::set<SgInitializedName*> loopCounters;
    Rose_STL_Container<SgNode*> forLoops = NodeQuery::querySubTree (forStmt->get_loop_body(), V_SgForStatement);
    for (Rose_STL_Container<SgNode*>::iterator i = forLoops.begin(); i != forLoops.end(); i++)
    {
      loopCounters.insert(SageInterface::getLoopIndexVariable(isSgForStatement(*i)));
    }

    SgInitializedName* varDecl;
    for(std::vector<SgVarRefExp*>::iterator refIt = varRefs.begin(); refIt != varRefs.end(); refIt++)
    {
      varDecl = (*refIt)->get_symbol()->get_declaration();
      if(vars.find(varDecl) == vars.end())
      {
        vars[varDecl] =  VariableAttributeClassification(); 
        if(isParallelized)
        {
          determine_default_data_sharing(forStmt, vars, varDecl);
        }
      }

      // internally modified variables      
      if(!vars[varDecl].is_written())
      {
        defsNodes = defuse->getDefFor(*refIt, varDecl);
        for(std::vector<SgNode*>::iterator defIt = defsNodes.begin(); defIt != defsNodes.end(); defIt++)
        {
          if(SageInterface::isAncestor(forStmt->get_loop_body(), *defIt) || *defIt == forStmt->get_loop_body())
          {
            vars[varDecl].set_modified();
          }
        }
      }

      // used before definition
      if(!(vars[varDecl].must_initialize()))
      {
        usesNodes = defuse->getUseFor(*refIt, varDecl);
        defsNodes = defuse->getDefFor(*refIt, varDecl);
        for(std::vector<SgNode*>::iterator useIt = usesNodes.begin(); useIt != usesNodes.end(); useIt++)
        {
          if((*useIt) == (*refIt)) 
          { // *refIt is use node (see LiveVariableAnalysis implementation)
            vars[varDecl].set_used();
            if(defsNodes.size() == 0)
            {
              vars[varDecl].set_used_before();
            }
            for(std::vector<SgNode*>::iterator defIt = defsNodes.begin(); defIt != defsNodes.end(); defIt++) 
            {
              if(defsBefore.find(std::pair<SgInitializedName*, SgNode*>(varDecl, *defIt)) != defsBefore.end() && !useOfDefinitionAfterRedefinedEachIteration(varDecl, *defIt, forStmt->get_loop_body(), liveSet) && (loopCounters.find(varDecl) == loopCounters.end() || !useOfInitializedForLoopCounter(*refIt, forStmt->get_loop_body())))
              {
                vars[varDecl].set_used_before();
                break;
              }
            }
            break;
          }
        }
      }
    }
  }

  bool DoAllChecker::useOfDefinitionAfterRedefinedEachIteration(SgInitializedName* var, SgNode* definition, SgStatement* loop_body_pattern, std::set<SgInitializedName*>& liveAtIterationStart)
  {
    if(SageInterface::isAncestor(loop_body_pattern, definition) && liveAtIterationStart.find(var) == liveAtIterationStart.end())
    {
      return true;
    }
    return false;
  }

  bool DoAllChecker::useOfInitializedForLoopCounter(SgVarRefExp* varRef, SgStatement* loop_body_pattern)
  {
    SgStatement* stmtVar = SageInterface::getEnclosingStatement(varRef);
    if(stmtVar)
    {
      SgStatement* stmt=SageInterface::findEnclosingLoop(stmtVar);
      while(stmt && (stmt==loop_body_pattern || SageInterface::isAncestor(loop_body_pattern, stmt)))
      {
        if(isSgForStatement(stmt))
        {
          SgForStatement* forLoop = isSgForStatement(stmt);
          if(SageInterface::getLoopIndexVariable(forLoop) == varRef->get_symbol()->get_declaration() && stmtVar != forLoop->get_for_init_stmt() && !SageInterface::isAncestor(forLoop->get_for_init_stmt(), stmtVar))
          {
            std::vector<SgStatement*> initStmts = forLoop->get_init_stmt();
            for(std::vector<SgStatement*>::iterator initIt = initStmts.begin(); initIt != initStmts.end(); initIt++)
            {
              SgExpression* lhs = NULL;
              if(SageInterface::isAssignmentStatement(*initIt, &lhs) && isSgVarRefExp(lhs) && isSgVarRefExp(lhs)->get_symbol()->get_declaration() == varRef->get_symbol()->get_declaration())
              {
                return true; 
              }
            }
          }
        }
        if(isSgStatement(stmt->get_parent()))
        {
          stmt = SageInterface::findEnclosingLoop(isSgStatement(stmt->get_parent()));
        }
        else
        {
          stmt = NULL;
        }
      }
    }

    return false;
  }

  void DoAllChecker::analyze_function_calls(SgStatement* stmt, std::map<SgInitializedName*, VariableAttributeClassification>& vars, std::map<std::string, std::set<int>>& seenFun, bool* globalModFun, bool firstCall, DefUseAnalysis* defuse, bool isParallelized)
  {
    SgFunctionCallExp* call;
    SgFunctionDeclaration* funDecl;
    SgFunctionDeclaration* funDeclWD;

    Rose_STL_Container<SgNode*> funCalls = NodeQuery::querySubTree (stmt, V_SgFunctionCallExp );
    for (Rose_STL_Container<SgNode*>::iterator i = funCalls.begin(); i != funCalls.end(); i++)
    {
      call = isSgFunctionCallExp(*i); 
      if(!call) continue;

      funDecl = SageInterface::getFunctionDeclaration(call); // typically a prototype
      if(funDecl) // funDecl == NULL should be a function pointer
      {

        if(seenFun.find(funDecl->get_name().getString()) != seenFun.end())
        {
          continue;
        }
        seenFun[funDecl->get_name().getString()] = std::set<int>();
        if(SageInterface::isExtern(funDecl))
        {
          if(!acceptable_external_function(funDecl->get_name().getString()))
          {
            *globalModFun = true;
          }
        }
        else
        {
          
          if(funDecl->get_definition() == NULL)
          { // function calls refers to prototype 
            funDeclWD = SageInterface::findFunctionDeclaration(SageInterface::getProject(funDecl), funDecl->get_name().getString(), NULL, true);
          }
          else
          {
            funDeclWD = funDecl;
          }

          // check modified vars
          DefUseAnalysisPF* defusePF = new DefUseAnalysisPF(false, defuse);
          bool abort = false;
          defusePF->run(funDeclWD->get_definition(), abort);
          assert(!abort);

          int paramIndex;
          std::vector<SgNode*> defsNodes; 
          std::vector<SgVarRefExp*> varRefs;
          std::set<int>& modifiedParams = seenFun[funDeclWD->get_name().getString()];
          std::vector<SgInitializedName*> params = funDeclWD->get_args();
          SgInitializedName* decl;
          SageInterface::collectVarRefs (funDeclWD->get_definition()->get_body(), varRefs);
          for(std::vector<SgVarRefExp*>::iterator bodyIt = varRefs.begin(); bodyIt != varRefs.end(); bodyIt++)
          {
            decl = (*bodyIt)->get_symbol()->get_declaration();
            if(helper.is_global(decl->get_scope())) // TODO handle global variables more precisely?
            {
              if(vars.find(decl) == vars.end())
              {
                vars[decl] =  VariableAttributeClassification();
                if(isParallelized)
                {
                  determine_default_data_sharing(NULL, vars, decl);
                }
              }
              if(!vars[decl].is_written())
              {
                defsNodes = defuse->getDefFor(*bodyIt, decl);
                for(std::vector<SgNode*>::iterator defIt = defsNodes.begin(); defIt != defsNodes.end(); defIt++)
                {
                  if(SageInterface::isAncestor(funDeclWD->get_definition()->get_body(), *defIt) || *defIt == funDeclWD->get_definition()->get_body())
                  {
                    vars[decl].set_modified();
                    break;
                  }
                }
              }
              vars[decl].set_access_in_called_function();
              vars[decl].set_used_before();
            }
            else 
            {
              paramIndex = get_param_index(decl, params);
              if(paramIndex != -1)
              {
                if(modifiedParams.find(paramIndex) == modifiedParams.end())
                {
                  defsNodes = defuse->getDefFor(*bodyIt, decl);
                  for(std::vector<SgNode*>::iterator defIt = defsNodes.begin(); defIt != defsNodes.end(); defIt++)
                  {
                    if(SageInterface::isAncestor(funDeclWD->get_definition()->get_body(), *defIt) || *defIt == funDeclWD->get_definition()->get_body())
                    {
                      modifiedParams.insert(paramIndex);
                      break;
                    }
                  }
                }
              }
            }
          }
          delete defusePF;
          analyze_function_calls(funDeclWD->get_definition()->get_body(), vars, seenFun, globalModFun, false, defuse, isParallelized);
        }


        std::vector<SgExpression*> params = call->get_args()->get_expressions();
        std::vector<SgType*> paramTypes = funDecl->get_type()->get_arguments();
        SgFunctionDeclaration* enFunDecl = SageInterface::getEnclosingFunctionDefinition(call, true)->get_declaration();
        assert(enFunDecl);
        SgInitializedName* decl;
        int paramIndex;
        for(unsigned int i=0; i< paramTypes.size(); i++)
        {

          if(!SageInterface::isScalarType(paramTypes[i]) && (!SageInterface::isExtern(funDecl) || pure_thread_safe_funs.find(funDecl->get_name().getString()) == pure_thread_safe_funs.end())) // TODO  become more precise (e.g., take modifications into account)?
          {
            if(isSgPntrArrRefExp(params[i])) 
            {             
              decl = SageInterface::convertRefToInitializedName(params[i]);
             
              if(firstCall)
              {
                if(vars.find(decl) == vars.end())
                {
                  vars[decl] =  VariableAttributeClassification();
                  if(isParallelized)
                  {
                    determine_default_data_sharing(NULL, vars, decl);
                  }
                }
                if(SageInterface::isExtern(funDecl) || seenFun[funDecl->get_name().getString()].find(i) != seenFun[funDecl->get_name().getString()].end()) 
                {
                  vars[decl].set_modified();
                }
                vars[decl].set_used_before();
              }
              else
              {
                paramIndex = get_param_index(decl, enFunDecl->get_args());
                if(paramIndex != -1)
                {
                  seenFun[enFunDecl->get_name().getString()].insert(paramIndex);
                }
              }
            }
            else
            {
              std::vector<SgVarRefExp*> varRefs;
              SageInterface::collectVarRefs (params[i], varRefs);
              for(std::vector<SgVarRefExp*>::iterator paramIt = varRefs.begin(); paramIt != varRefs.end(); paramIt++)
              {
                decl = (*paramIt)->get_symbol()->get_declaration();

                if(firstCall)
                {
                  if(vars.find(decl) == vars.end())
                  {
                    vars[decl] =  VariableAttributeClassification();
                    if(isParallelized)
                    {
                      determine_default_data_sharing(NULL, vars, decl);
                    }
                  }
                  vars[decl].set_modified();
                  vars[decl].set_used_before();
                }
                else
                {
                  paramIndex = get_param_index(decl, enFunDecl->get_args());
                  if(paramIndex != -1)
                  {
                    seenFun[enFunDecl->get_name().getString()].insert(paramIndex);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  int DoAllChecker::get_param_index(SgInitializedName* decl, std::vector<SgInitializedName*>& params)
  {
    for(unsigned int i=0;i<params.size();i++)
    {
      if(params[i] == decl)
      {
        return i;
      }
    }
    return -1; // not a parameter
  }

  bool DoAllChecker::acceptable_external_function(std::string& funName)
  {
    if(funName.rfind("__VERIFIER_nondet_", 0) == 0 || pure_thread_safe_funs.find(funName) != pure_thread_safe_funs.end())
    {
      return true;
    }
    return false;
  }


  bool DoAllChecker::ensures_same_input(VariableAttributeClassification& varClass)
  { 
    SharingType data_attr = varClass.get_sharing_type();
    assert(data_attr != UNSET);

    if(varClass.must_initialize())
    { 
      if(varClass.is_written())
      {
        return data_attr == SHARED || (allows_reduction() && data_attr == REDUCTION); 
      }
      else
      {
        return (data_attr != PRIVATE|| varClass.is_loop_counter()) && data_attr != LASTPRIVATE && data_attr != REDUCTION;
      }
    }
    return true;
  }

  bool DoAllChecker::ensures_same_output(SgInitializedName* varName, VariableAttributeClassification& varClassPar, bool seqRequiresOutput)
  {
    if(seqRequiresOutput  || (varClassPar.is_live() && varClassPar.must_declare()))
    {
      SharingType data_attr = varClassPar.get_sharing_type();
      assert(data_attr != UNSET); 
      if(varClassPar.is_written())
      {
        return data_attr == SHARED || (allows_reduction() && data_attr == REDUCTION) || (SageInterface::isScalarType(varName->get_type()) && (data_attr == LASTPRIVATE || data_attr == FIRSTLASTPRIVATE));
      }
      else
      {
        return  data_attr != LASTPRIVATE;
      }
    }

    return true; 
  }

  void DoAllChecker::determine_data_sharing_from_pragma(SgPragma* omp_pragma, std::map<SgInitializedName*, VariableAttributeClassification>& nameToClass, std::map<std::string, SgInitializedName*>& nameToDecl)
  {
    // assume that it is only determined by clauses occurring before do all pattern, no usage of threadprivate directives or similar -> TODO check that no thread private directives exist

    std::string pragmaText = omp_pragma->get_pragma();
    std::string clauseName, redOp, varName;
    ReductionOp red_op_type;
    int nextStart, nextStartI, nextStartI2, nextEnd, nextEndI;

    nextStart = get_next_elem_pos(0, pragmaText);
    if(nextStart == -1)
    {
      return;
    }
    nextEnd = get_next_elem_end_pos(nextStart, pragmaText);

    if(pragmaText.substr(nextStart, nextEnd-nextStart+1).compare("omp") == 0)
    {
      SharingType clauseType;
      while((unsigned int) (nextEnd+2) < pragmaText.size())
      {
        nextStart = get_next_elem_pos(nextEnd+2, pragmaText);
        if(nextStart == -1)
        {
          break;
        }
        nextEnd = get_next_elem_end_pos(nextStart, pragmaText);
        clauseName = pragmaText.substr(nextStart, nextEnd-nextStart+1); 
        redOp = "";
        varName = "";

        nextStart = get_next_elem_pos(nextEnd+1, pragmaText);
        if(nextStart == -1)
        {
          continue;
        }

        if(clauseName.compare("private") == 0)
        {
          clauseType = PRIVATE; 
          assert(pragmaText[nextStart] == '(');
        } 
        else if(clauseName.compare("firstprivate") == 0)
        {
          clauseType = FIRSTPRIVATE; 
          assert(pragmaText[nextStart] == '(');
        }
        else if(clauseName.compare("shared") == 0)
        {
          clauseType = SHARED; 
          assert(pragmaText[nextStart] == '(');
        }
        else if(clauseName.compare("lastprivate") == 0)
        {
          clauseType = LASTPRIVATE;
          assert(pragmaText[nextStart] == '('); 
          
          nextStartI = get_next_elem_pos(nextStart+1, pragmaText);
          if(nextStartI == -1)
          {
            continue;
          }
          nextEndI = get_next_elem_end_pos(nextStartI, pragmaText);
          if((unsigned int) (nextEndI+1)<pragmaText.size())
          {
            if(pragmaText[nextEndI+1] == ':')
            {
              nextStart = nextEndI+1; 
            }
            else if(isspace(pragmaText[nextEndI+1]))
            {
              nextStartI = get_next_elem_pos(nextEndI+2, pragmaText);
              if(nextStartI == -1)
              {
                continue;
              }
              if(pragmaText[nextStartI] == ':')
              {
                nextStart = nextStartI;
              }
            }
          }
        }
        else if(clauseName.compare("reduction") == 0)
        {
          assert(allows_reduction());
          assert(pragmaText[nextStart] == '(');
          clauseType = REDUCTION; //reduction([reduction-modifier ,] reduction-identifier : list)

          nextStartI = nextStart;
          for(int i=0; i<2;i++)
          {
            nextStartI = get_next_elem_pos(nextStartI+1, pragmaText);
            if(nextStartI == -1)
            {
              break;
            }

            nextEndI = get_next_elem_end_pos(nextStartI, pragmaText);
            if((unsigned int) (nextEndI+1)<pragmaText.size())
            {
              if(pragmaText[nextEndI+1] == ':')
              {
                // store the reduction identifier
                redOp = pragmaText.substr(nextStartI, nextEndI-nextStartI+1);
                nextStart = nextEndI+1; 
              }
              else if(pragmaText[nextEndI+1] == ',')
              {
                nextStartI = nextEndI+1;
                continue;
              }
              else if(isspace(pragmaText[nextEndI+1]))
              {
                nextStartI2 = nextStartI;
                nextStartI = get_next_elem_pos(nextEndI+2, pragmaText);
                if(nextStartI == -1)
                {
                  break;
                }
                if(pragmaText[nextStartI] == ':')
                {
                  // store the reduction identifier
                  redOp = pragmaText.substr(nextStartI, nextEndI-nextStartI2+1); 
                  nextStart = nextStartI;
                }
                else if(pragmaText[nextStartI] == ',')
                {
                  continue;
                }
              }
              break;
            }
          }
          if(nextStartI == -1)
          {
            continue;
          }
        }
        else if(clauseName.compare("linear") == 0)
        {
          // TODO support linear(list[ : linear-step]) clauseType = ...; 
          assert(0);
          assert(pragmaText[nextStart] == '(');
        }
        else
        {
          assert(clauseName.compare("copyin") != 0); //copyin(list),
          if(pragmaText[nextStart] == '(')  // skip remaining part of clause 
          {
            nextEnd = get_next_elem_end_pos(nextStart, pragmaText) + 1;
            while((unsigned int) nextEnd < pragmaText.size() && pragmaText[nextEnd] != ')')
            {
              nextStart = get_next_elem_pos(nextEnd+1, pragmaText);
              if(nextStart == -1)
              {
                nextEnd = pragmaText.size();
                break;
              }
              nextEnd = get_next_elem_end_pos(nextStart, pragmaText) + 1;
            }
          }
          continue;
        }

        // convert to reduction operator
        red_op_type = convert_to_reduction_operator(redOp);

       // assume that nextStart is either ( , : )
        while(pragmaText[nextStart] != ')')
        {
          nextStart = get_next_elem_pos(nextStart+1, pragmaText);
          if(nextStart == -1)
          {
            break;
          }
          nextEnd = get_next_elem_end_pos(nextStart, pragmaText);

          varName = pragmaText.substr(nextStart, nextEnd-nextStart+1);
          assert(varName.compare("") != 0);
          if(nameToDecl.find(varName) != nameToDecl.end())
          {
            assert(nameToClass.find(nameToDecl[varName]) != nameToClass.end()); 
            nameToClass[nameToDecl[varName]].set_sharing_type(clauseType);
            if(clauseType == REDUCTION)
            {
              assert(red_op_type != INVALID);
              nameToClass[nameToDecl[varName]].set_reduction_op(red_op_type);
            }
          }

          if(pragmaText.size() > (unsigned int) (nextEnd +1))
          {
            if(pragmaText[nextEnd+1] == ',')
            {
              nextStart = nextEnd + 1;
            }
            else if (isspace(pragmaText[nextEnd+1]))
            { 
              nextStart = get_next_elem_pos(nextEndI+2, pragmaText);
              if(nextStart == -1)
              {
                break;
              }
            }
            else
            {
              nextStart = nextEnd+1;
            }
          }
          else
          {
            nextStart = -1;
            break;
          }
        }
        if(nextStart == -1)
        {
          nextEnd = pragmaText.size(); 
        } 
      }
    }
  }

  int DoAllChecker::get_next_elem_pos(int startPos, std::string& pragma)
  {
    assert(startPos >= 0);

    while((unsigned int)startPos<pragma.size())
    {
      if(!isspace(pragma[startPos]))
      {
        return startPos;
      }
      startPos++;
    }
    return -1;
  }

  int DoAllChecker::get_next_elem_end_pos(int start, std::string& pragma)
  {
    assert(start>=0 && (unsigned int)start<pragma.size());

    while((unsigned int)start<pragma.size())
    {
      if(pragma[start] == '(' || pragma[start] == ')' || pragma[start] == ',' || pragma[start] == ':' || isspace(pragma[start]))
      {
        break;
      }
      start++;
    }
    return start-1;
  }

  void DoAllChecker::determine_default_data_sharing(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& nameToClass, SgInitializedName* var)
  {
    assert(nameToClass.find(var) != nameToClass.end());

    if(parFor == NULL) // global variable added
    {
      nameToClass[var].set_sharing_type(SHARED);
    }
    else
    {
      if(SageInterface::getLoopIndexVariable(parFor)==var)
      {
        // TODO does not cover simd constructs
        nameToClass[var].set_sharing_type(PRIVATE);
        nameToClass[var].mark_as_loop_counter(detect_loop_type(parFor->get_increment(), var));
      } 
      else if(SageInterface::isAncestor(parFor->get_scope(), var->get_scope()))
      {
        if(SageInterface::isStatic(var->get_declaration())) // TODO does it work?
        {
          nameToClass[var].set_sharing_type(SHARED);
        }
        else
        {
          nameToClass[var].set_sharing_type(PRIVATE);
        }
      }
      else
      {
        // default shared
        nameToClass[var].set_sharing_type(SHARED);
      }
    }
  }
  LoopType DoAllChecker::detect_loop_type(SgExpression* incrStmt, SgInitializedName* loop_counter)
  {
    char constVal;

    if(isSgPlusPlusOp(incrStmt) || isSgMinusMinusOp(incrStmt))
    {
      if(isSgVarRefExp(isSgUnaryOp(incrStmt)->get_operand()) && isSgVarRefExp(isSgUnaryOp(incrStmt)->get_operand())->get_symbol()->get_declaration() == loop_counter)
      {
        return isSgPlusPlusOp(incrStmt) ? INCREASE: DECREASE;
      }
    }
    else if(isSgPlusAssignOp(incrStmt) || isSgMinusAssignOp(incrStmt))
    {
      if(isSgVarRefExp(isSgBinaryOp(incrStmt)->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(incrStmt)->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
      {
        constVal = is_positive_number(isSgBinaryOp(incrStmt)->get_rhs_operand());
        if(constVal != 'u')
        {
          return (isSgPlusAssignOp(incrStmt) && constVal=='p') || (isSgMinusAssignOp(incrStmt) && constVal=='n') ? INCREASE: DECREASE;
        }
      }
    }
    else if(isSgAssignOp(incrStmt) && isSgVarRefExp(isSgBinaryOp(incrStmt)->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(incrStmt)->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
    {
      constVal = 'u';
      if(isSgAddOp(isSgBinaryOp(incrStmt)->get_rhs_operand()))
      {
        if(isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
        {
          constVal = is_positive_number(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_rhs_operand());
        }
        else if(isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_rhs_operand()) && isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_rhs_operand())->get_symbol()->get_declaration() == loop_counter)
        {
          constVal = is_positive_number(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_lhs_operand());
        }
      }
      else if(isSgMinusOp(isSgBinaryOp(incrStmt)->get_rhs_operand()) && isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_lhs_operand()) && isSgVarRefExp(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_lhs_operand())->get_symbol()->get_declaration() == loop_counter)
      {
        constVal = is_positive_number(isSgBinaryOp(isSgBinaryOp(incrStmt)->get_rhs_operand())->get_rhs_operand());
      }
      if(constVal != 'u')
      {
        return (isSgAddOp(isSgBinaryOp(incrStmt)->get_rhs_operand()) && constVal=='p') || (isSgMinusOp(isSgBinaryOp(incrStmt)->get_rhs_operand()) && constVal=='n') ? INCREASE: DECREASE;
      }
    }

    return UNDETERMINED;
  }

  char DoAllChecker::is_positive_number(SgExpression* expr)
  {
    if(isSgIntVal(expr))
    {
      if(isSgIntVal(expr)->get_value() > 0)
      {
        return 'p';
      }
      if(isSgIntVal(expr)->get_value() < 0)
      {
        return 'n';
      }
    }
    if(isSgCharVal(expr))
    {
      if(isSgCharVal(expr)->get_value() > 0)
      {
        return 'p';
      }
      if(isSgCharVal(expr)->get_value() < 0)
      {
        return 'n';
      }
    }
    if(isSgLongIntVal(expr))
    {
      if(isSgLongIntVal(expr)->get_value() > 0)
      {
        return 'p';
      }
      if(isSgLongIntVal(expr)->get_value() < 0)
      {
        return 'n';
      }
    }
    if(isSgLongLongIntVal(expr))
    {
      if(isSgLongLongIntVal(expr)->get_value() > 0)
      {
        return 'p';
      }
      if(isSgLongLongIntVal(expr)->get_value() < 0)
      {
        return 'n';
      }
    }
    if(isSgShortVal(expr))
    {
      if(isSgShortVal(expr)->get_value() > 0)
      {
        return 'p';
      }
      if(isSgShortVal(expr)->get_value() < 0)
      {
        return 'n';
      }
    }
    if(isSgUnsignedCharVal(expr) || isSgUnsignedIntVal(expr) ||isSgUnsignedLongLongIntVal(expr) || isSgUnsignedLongVal(expr) || isSgUnsignedShortVal(expr))
    {
      return 'p';
    }
    /*if(isSgChar16Val(expr) || isSgChar32Val(expr) || isSgWcharVal(expr))
    {
      return 'u';
    }*/

    return 'u';
  }

  void DoAllChecker::insert_eq_pragmas(SgStatement* loopBody, int id)
  {
    SageBuilder::pushScopeStack(loopBody->get_scope()); 
    SageInterface::insertStatementBefore(loopBody, SageBuilder::buildPragmaDeclaration(startMarkerPrefix+std::to_string(id)));
    SageInterface::insertStatementAfter(loopBody, SageBuilder::buildPragmaDeclaration(endMarkerPrefix+std::to_string(id)));
    SageBuilder::popScopeStack();
  }

  bool DoAllChecker::is_parallelized_for(std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& seqSeg, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& parSeq, SgForStatement** parFor, std::vector<SgPragma*>& omp_pragmas)
  {
    *parFor = NULL;
    SgForStatement* stmtSeq = isSgForStatement(SageInterface::getNextStatement(seqSeg.first));
    if(stmtSeq == NULL || SageInterface::getNextStatement(stmtSeq) != seqSeg.second)
    {
      return false;
    }
  
    SgForStatement* stmtPar = check_do_all_parallelization_and_return_for(SageInterface::getNextStatement(parSeq.first), parSeq.second, omp_pragmas);

    if(!stmtPar || !is_omp_pragma_free(stmtPar))
    {
      return false;
    }

    *parFor = stmtPar;

    return identical_for_header(stmtSeq, stmtPar);
  }

  bool DoAllChecker::is_omp_pragma_free(SgForStatement* forLoop)
  {
    std::set<std::string> seenFun;
    return is_omp_pragma_free(forLoop, seenFun);
  }

  bool DoAllChecker::is_omp_pragma_free(SgNode* stmt, std::set<std::string>& seenFun)
  {
    Rose_STL_Container<SgNode*> stmts = NodeQuery::querySubTree (stmt, V_SgPragmaDeclaration);
    for (Rose_STL_Container<SgNode*>::iterator i = stmts.begin(); i != stmts.end(); i++)
    {
      SgPragmaDeclaration* pragmaStmt = isSgPragmaDeclaration(*i);

      if(pragmaStmt->get_pragma()->get_pragma().rfind("omp", 0) == 0)
      {
        return false;
      }
    }
    // check called functions
    SgFunctionCallExp* call;
    SgFunctionDeclaration* funDecl;
    stmts = NodeQuery::querySubTree (stmt, V_SgFunctionCallExp);
    for (Rose_STL_Container<SgNode*>::iterator i = stmts.begin(); i != stmts.end(); i++)
    {
      call = isSgFunctionCallExp(*i); 
      if(!call) continue;

      funDecl = SageInterface::getFunctionDeclaration(call); // typically a prototype
      if(funDecl) // funDecl == NULL should be a function pointer
      {

        if(seenFun.find(funDecl->get_name().getString()) != seenFun.end())
        {
          continue;
        }
        seenFun.insert(funDecl->get_name().getString());
        if(!SageInterface::isExtern(funDecl))
        {         
          if(funDecl->get_definition() == NULL)
          { // function calls refers to prototype 
            funDecl = SageInterface::findFunctionDeclaration(SageInterface::getProject(funDecl), funDecl->get_name().getString(), NULL, true);
          }
          if(!is_omp_pragma_free(funDecl->get_definition()->get_body(), seenFun))
          {
            return false;
          }
        }
      }
    }

    return true;
  }

  SgForStatement* DoAllChecker::check_do_all_parallelization_and_return_for(SgStatement* startOfSegment, SgStatement* endSegmentPragma, std::vector<SgPragma*>& omp_pragmas)
  { 
    // currently support omp parallel for, omp simd, omp parallel followed by omp for (simd)
    assert(startOfSegment); 
    SgPragmaDeclaration* pragma = isSgPragmaDeclaration(startOfSegment);
    std::string pragma_part;
    std::istringstream pragma_stream;
    bool singlePragma = false;
    SgForStatement* forStmt = NULL;
    SgStatement* stmt;
    SgBasicBlock* block;

    if(pragma)
    {
      pragma_stream = std::istringstream(pragma->get_pragma()->get_pragma()); 
      stmt = SageInterface::getNextStatement(startOfSegment);
 
      if(pragma_stream.eof() || stmt == NULL|| SageInterface::getNextStatement(stmt) != endSegmentPragma)
      {
        return NULL;
      }
      pragma_stream >> pragma_part;
      if(pragma_part.compare("omp")!=0)
      {
        return NULL;
      }

      if(pragma_stream.eof())
      {
        return NULL;
      }

      omp_pragmas.push_back(pragma->get_pragma());

      pragma_stream >> pragma_part;
      if(pragma_part.compare("simd")==0)
      {
        singlePragma = true;
      }
      else if(pragma_part.compare("parallel")==0)
      {
        if(!pragma_stream.eof())
        {
           pragma_stream >> pragma_part;
           if(pragma_part.compare("for")==0)
           {
             singlePragma = true;
           }
           else 
           {
             assert(pragma_part.compare("distribute")!=0);
           }
        }
        if(!singlePragma)
        {
          while(isSgBasicBlock(stmt))
          {
            block = isSgBasicBlock(stmt);
            if(block->get_statements().size() == 1)
            {
              stmt = (block->get_statements())[0];
              break;
            }
            if(block->get_statements().size() != 1)
            {
              return NULL;
            }
            stmt = (block->get_statements())[0];
          }
          pragma = isSgPragmaDeclaration(stmt);
          if(!pragma)
          {
            return NULL;
          }
          pragma_stream = std::istringstream(pragma->get_pragma()->get_pragma());
          if(pragma_stream.eof() )
          {
            return NULL;
          }
          pragma_stream >> pragma_part;
          if(pragma_part.compare("omp")!=0)
          {
            return NULL;
          }
          if(pragma_stream.eof() )
          {
            return NULL;
          }
          pragma_stream >> pragma_part;
          if(pragma_part.compare("for")!=0)
          {
            return NULL;
          }
          stmt = SageInterface::getNextStatement(stmt);
          if(stmt == NULL)
          {
            return NULL;
          }
          while(isSgBasicBlock(stmt))
          {
            block = isSgBasicBlock(stmt);
            if(block->get_statements().size() != 1)
            {
              return NULL;
            }
            stmt = (block->get_statements())[0];
          }

          omp_pragmas.push_back(pragma->get_pragma());

          return isSgForStatement(stmt);
        }
      }
      else
      {
        return NULL;
      }

      if(singlePragma)
      {
        while(isSgBasicBlock(stmt))
        {
          block = isSgBasicBlock(stmt);
          if(block->get_statements().size() != 1)
          {
            return NULL;
          }
          stmt = (block->get_statements())[0];
        }
        return isSgForStatement(stmt);
      }

    }

    return forStmt;
  }

  bool DoAllChecker::identical_for_header(SgForStatement* seqFor, SgForStatement* parFor)
  {
    // check that init statement, test expression, and increment expression are the same
    assert(seqFor && parFor);

    if(seqFor->get_test_expr()->unparseToString().compare(parFor->get_test_expr()->unparseToString())==0 && !(isSgExprListExp(seqFor->get_increment())) && seqFor->get_increment()->unparseToString().compare(parFor->get_increment()->unparseToString())==0)
    {
      // only one init statement
      std::vector<SgStatement*>& seqInits = seqFor->get_init_stmt();
      std::vector<SgStatement*>& parInits = parFor->get_init_stmt();     
      if(seqInits.size() == 1 && parInits.size() == 1)
      {
        return seqInits[0]->unparseToString().compare(parInits[0]->unparseToString()) == 0;
      }
      
    }
    return false;
  }

  bool DoAllChecker::is_fixed_loop_iteration(SgForStatement* forStmt, std::map<SgInitializedName*, VariableAttributeClassification> vars)
  {

    std::vector<SgVarRefExp*> varsInTest;
    SageInterface::collectVarRefs(forStmt->get_test_expr(), varsInTest);

    SgInitializedName* varName;
    varName = SageInterface::getLoopIndexVariable(forStmt);
    if(vars.find(varName) != vars.end() && vars[varName].is_written()) // loop counter written in body
    {
      std::cout<<"Loop counter is written in loop body."<<std::endl;
      return false;
    }
    for(std::vector<SgVarRefExp*>::iterator it = varsInTest.begin(); it!=varsInTest.end(); it++)
    {
      varName = (*it)->get_symbol()->get_declaration();
      if(vars.find(varName) != vars.end() && vars[varName].is_written())
      {
        std::cout<<"Loop test expression is not constant during loop execution."<<std::endl;
        return false;
      }
    }

    return true;
  }

  bool DoAllChecker::is_syntactically_equal(SgStatement* seqBody, SgStatement* parBody)
  {
    assert(seqBody && parBody);
    if((seqBody->unparseToString()).compare(parBody->unparseToString())!=0)
    {
      return false;
    }
    // compare called functions
    std::map<std::string, SgBasicBlock*> called_seq, called_par;
    collect_called_functions(seqBody, called_seq);
    collect_called_functions(parBody, called_par);
    if(called_seq.size() != called_par.size())
    {
      return false;
    }
    for(std::map<std::string, SgBasicBlock*>::iterator it = called_seq.begin(); it!=called_seq.end();it++)
    {
      if(called_par.find(it->first) == called_par.end() || it->second->unparseToString().compare(called_par[it->first]->unparseToString()) != 0)
      {
        return false;
      }
    }
    return true;
  }

  void DoAllChecker::collect_called_functions(SgStatement* block, std::map<std::string, SgBasicBlock*>& collectionInfo)
  {
    SgFunctionCallExp* call;
    SgFunctionDeclaration* funDecl;
    Rose_STL_Container<SgNode*> stmts = NodeQuery::querySubTree (block, V_SgFunctionCallExp);
    for (Rose_STL_Container<SgNode*>::iterator i = stmts.begin(); i != stmts.end(); i++)
    {
      call = isSgFunctionCallExp(*i); 
      if(!call) continue;

      funDecl = SageInterface::getFunctionDeclaration(call); // typically a prototype
      if(funDecl) // funDecl == NULL should be a function pointer
      {

        if(collectionInfo.find(funDecl->get_name().getString()) != collectionInfo.end())
        {
          continue;
        }
  
        if(!SageInterface::isExtern(funDecl))
        {         
          if(funDecl->get_definition() == NULL)
          { // function calls refers to prototype 
            funDecl = SageInterface::findFunctionDeclaration(SageInterface::getProject(funDecl), funDecl->get_name().getString(), NULL, true);
          }

          collectionInfo[funDecl->get_name().getString()] = funDecl->get_definition()->get_body();
          collect_called_functions(funDecl->get_definition()->get_body(), collectionInfo);
        }
      }
    }
  }

  bool DoAllChecker::allows_reduction()
  {
    return false;
  }

  std::string DoAllChecker::get_pattern_name()
  {
    return "DoAll";
  }

  ReductionOp DoAllChecker::convert_to_reduction_operator(std::string& op)
  {
    if(op.compare("+") == 0)
    {
      return PLUS;
    }
    if(op.compare("-") == 0)
    {
      return MINUS;
    }
    if(op.compare("*") == 0)
    {
      return MUL;
    }
    if(op.compare("&&") == 0) // TODO may need to deal with white space in between
    {
      return AND;
    }
    if(op.compare("||") == 0) // TODO may need to deal with white space in between
    {
      return OR;
    }
    if(op.compare("min") == 0)
    {
      return MINIMUM;
    }
    if(op.compare("max") == 0)
    {
      return MAXIMUM;
    }
    if(op.compare("&") == 0)
    {
      return B_AND;
    }
    if(op.compare("|") == 0)
    {
      return B_OR;
    }
    if(op.compare("^") == 0)
    {
      return XOR;
    }
    return INVALID;
  }
