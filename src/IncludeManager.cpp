//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "IncludeManager.h"

  bool IncludeManager::contains_include(std::string name)
  {
    return includeInfos.find(name) != includeInfos.end();
  }

  bool IncludeManager::contains_stdlib()
  {
    return contains_include("stdlib.h");
  }

  bool IncludeManager::contains_math()
  {
    return contains_include("math.h");
  }

  bool IncludeManager::contains_complex()
  {
    return contains_include("complex.h");
  }

  void IncludeManager::insert_system(std::string name)
  {
    insert_include(name, true);
  }
  
  void IncludeManager::insert_include(std::string name, bool isSystem)
  {
    if(includeInfos.insert(std::pair<std::string, bool>(name, isSystem)).second)
    {
      includeOrder.push_back(name);
    }
  }

  void IncludeManager::include_assert()
  {
    insert_system("assert.h");
  }

  void IncludeManager::include_complex()
  {
   insert_system("complex.h");
  }

  void IncludeManager::include_stdlib()
  {
    insert_system("stdlib.h");
  }

  void IncludeManager::include_string()
  {
    insert_system("string.h");
  }

  void IncludeManager::include_stdio()
  {
    insert_system("stdio.h");
  }

  void IncludeManager::filter_includes(std::vector<PreprocessingInfo* >& incl_comm)
  {
    for(std::vector<PreprocessingInfo*>::iterator it = incl_comm.begin(); it!=incl_comm.end();it++)
    {
       if((*it)->getTypeOfDirective()== PreprocessingInfo::CpreprocessorIncludeDeclaration) 
       {
         int start, len;
         std::string include = ((*it)->getString());
         bool isSystem = include.find('<', 0) != std::string::npos;
         if(isSystem)
         {
             start=include.find('<', 0)+1;
             len = include.find('>', start) - start;
         }
         else
         {
             start=include.find('"', 0)+1;
             len = include.find('"', start) -start;             
         }
          insert_include(include.substr(start, len),isSystem);
       }
    }
  }

  void IncludeManager::get_includes_in(SgSourceFile* file)
  {
    std::map< std::string, ROSEAttributesList * >  ppDaC = file->get_preprocessorDirectivesAndCommentsList()->getList();
    for (std::map< std::string, ROSEAttributesList * >::iterator it = ppDaC.begin(); it != ppDaC.end(); it++ )
    {
        filter_includes(it->second->getList());
    }
  }

  /* method must be called after something is added to output file*/
  void IncludeManager::insert_includes(SgSourceFile* file)
  {

    for (std::vector<std::string>::iterator it = includeOrder.begin(); it != includeOrder.end(); it++)
    {
       SageInterface::insertHeader(file, *it, includeInfos[*it], true); 
    } 
  }

  void IncludeManager::reset()
  {
    includeInfos.clear();
    includeOrder.clear();
  }

