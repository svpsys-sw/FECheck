//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "PrototypeManager.h"
#include <assert.h>

  PrototypeManager::PrototypeManager()
  {
    for(int i=0; i<NondetTypeSize; i++)
    {
      addedNondets[i] = false;
    }
  }

  void PrototypeManager::add_prototype(SgFunctionDeclaration* prototype)
  {
    assert(prototype != NULL);
    if(seenFunProtoNames.insert(prototype->get_name().getString()).second)
    { 
      required_prototypes.insert(prototype);
    }
  }

  void PrototypeManager::add_verifier_nondet(NondetType type)
  {
    if(!addedNondets[type])
    {
      addedNondets[type] = true;
      const std::string funcName = std::string(vnondetPrefix)+ std::string(helper.get_nondet_fun_suffix(type));
      SgFunctionDeclaration* prototype = SageBuilder::buildNondefiningFunctionDeclaration (SgName(funcName), helper.get_type(type), SageBuilder::buildFunctionParameterList_nfi());
      SageInterface::setExtern(prototype);
      add_prototype(prototype); 
    }
  }



  void PrototypeManager::insert_function_declarations()
  {
    for(std::set<SgFunctionDeclaration*>::iterator protIt = required_prototypes.begin(); protIt != required_prototypes.end(); protIt++)
    {
      SageInterface::prependStatement(*protIt); 
    }
  }

  void PrototypeManager::reset()
  {
    for(int i=0; i<NondetTypeSize; i++)
    {
      addedNondets[i] = false;
    }

    required_prototypes.clear();
    seenFunProtoNames.clear();
  }
