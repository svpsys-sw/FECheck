//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "AbstractChecker.h"
#include "PECConstants.h"
#include <cctype>

  AbstractChecker::AbstractChecker(bool requireOutputVars)
  {
    require_output_variables = requireOutputVars;
    std::string syncConstr[] = {"atomic", "barrier", "critical", "depend", "depobj", "flush", "ordered", "taskgroup", "taskwait"};
    openmp_synch_stmts = std::set<std::string>(syncConstr, syncConstr+8);
  }

  bool AbstractChecker::contains_openmp_synch_stmt(SgSourceFile* parProg)
  {
    
    // get pragma like open mp, etc., not the includes
    Rose_STL_Container<SgNode*> pragmas = NodeQuery::querySubTree (parProg, V_SgPragma);
    for (Rose_STL_Container<SgNode*>::iterator i = pragmas.begin(); i != pragmas.end(); i++)
    {
      SgPragma* pragma = isSgPragma(*i);
      if(openmp_synch_stmts.find(extract_omp_directive_name(pragma->get_pragma())) != openmp_synch_stmts.end())
      {
        return true;
      }
    } 

    return false;
  }

  std::string AbstractChecker::extract_omp_directive_name(std::string pragmaText)
  {
    if(pragmaText.rfind(ompPragma, 0) == 0)
    {
      unsigned int directiveIndexStart = 3, directiveIndexEnd; 
      while(directiveIndexStart<pragmaText.size() && isspace(pragmaText[directiveIndexStart]))
      {
        directiveIndexStart++;
      }
      directiveIndexEnd = directiveIndexStart;
      while(directiveIndexEnd<pragmaText.size() && !isspace(pragmaText[directiveIndexEnd]))
      {
        directiveIndexEnd++;
      }
      if(directiveIndexStart < pragmaText.size())
      {
        return pragmaText.substr(directiveIndexStart, directiveIndexEnd - directiveIndexStart); 
      }
    }
    return std::string();
  }


  int AbstractChecker::get_num_checks(SgSourceFile* file)
  {
    int num = 0;

    // get pragma like open mp, etc., not the includes
    Rose_STL_Container<SgNode*> pragmas = NodeQuery::querySubTree (file, V_SgPragma);
    for (Rose_STL_Container<SgNode*>::iterator i = pragmas.begin(); i != pragmas.end(); i++)
    {
      SgPragma* pragma = isSgPragma(*i);
      if(pragma->get_pragma().rfind(startMarkerPrefix, 0) == 0)
      {
        num = num + 1;
      }
    }

    return num;
  }


  std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> AbstractChecker::get_start_end_pragmas(SgSourceFile* file, int checkID)
  {
    std::string startPragma = startMarkerPrefix+ std::to_string(checkID);
    std::string endPragma = endMarkerPrefix+ std::to_string(checkID);

    std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*> startEnd;
    bool startFound = false, endFound = false;
    // get pragma like open mp, etc., not the includes
    Rose_STL_Container<SgNode*> pragmaStmts = NodeQuery::querySubTree (file, V_SgPragmaDeclaration);
    for (Rose_STL_Container<SgNode*>::iterator i = pragmaStmts.begin(); i != pragmaStmts.end(); i++)
    {
      SgPragmaDeclaration* pragmaStmt = isSgPragmaDeclaration(*i);

      if(pragmaStmt->get_pragma()->get_pragma().rfind(startPragma, 0) == 0)
      {
        if(pragmaStmt->get_pragma()->get_pragma().size() <= startPragma.size() || isspace(pragmaStmt->get_pragma()->get_pragma()[startPragma.size()]))
        {
          assert(!startFound);
          startFound = true;
          startEnd.first=pragmaStmt;
        }
      }

      if(pragmaStmt->get_pragma()->get_pragma().rfind(endPragma, 0) == 0)
      {
        if(pragmaStmt->get_pragma()->get_pragma().size() <= endPragma.size() || isspace(pragmaStmt->get_pragma()->get_pragma()[endPragma.size()]))
        {
          assert(!endFound);
          endFound = true;
          startEnd.second=pragmaStmt;
        }
      }
    }

    assert(startFound && endFound);
    return startEnd;
  }

  bool AbstractChecker::missed_by_LV_analysis(SgType* type)
  {
    if(isSgArrayType(type) && SageInterface::getDimensionCount(type)>1)
    {
      return true;
    }
    return false;
  }
  
  bool AbstractChecker::require_output_vars()
  {
      return require_output_variables;
  }
