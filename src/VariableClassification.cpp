//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VariableClassification.h"
#include <assert.h>

  SgInitializedName* VariableClassification::get_decl()
  {
    return decl;
  }

  bool VariableClassification::is_live()
  {
    return isLiveAfter;
  }

  bool VariableClassification::is_used_before()
  {
    return isUsedBefore;
  }

  bool VariableClassification::is_written()
  {
    return isModified;
  }

  bool VariableClassification::must_compare()
  {
    return isLiveAfter && isModified;
  }

  bool VariableClassification::must_declare()
  {
    return requiresDecl;
  }

  bool VariableClassification::must_initialize()
  {
    return (!isModified || isUsedBefore) && must_declare();
  }

  bool VariableClassification::must_rename()
  {
    return isRename;
  }

  void VariableClassification::set_decl(SgInitializedName* pDecl)
  {
    decl = pDecl;
  }

  void VariableClassification::set_live_after()
  {
    isLiveAfter = true;
  }
     
  void VariableClassification::set_modified()
  {
    isModified = true;
  }

  void VariableClassification::set_rename()
  {
    isRename = true;
  }

  void VariableClassification::set_used_before()
  {
    isUsedBefore = true;
  }

  void VariableClassification::unset_used_before()
  {
    isUsedBefore = false;
  }

  void VariableClassification::unset_declaration()
  {
    requiresDecl = false;
  }
