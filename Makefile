# SPDX-License-Identifier: Apache-2.0

# Copyright 2021 Marie-Christine Jakobs
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Z3PATH = ../z3-4.8.10-x64-ubuntu-18.04

CXX = g++

CXXFLAGS = -Wall

LDFLAGS = -L/usr/rose/lib -L/usr/lib/x86_64-linux-gnu/ -L/usr/lib -L$(Z3PATH)/bin

LDLIBS = -lrose -lboost_date_time -lboost_thread -lboost_filesystem -lboost_program_options -lboost_regex -lboost_system -lboost_serialization -lboost_wave -lboost_iostreams -lboost_chrono -ldl  -lpthread -ldl -lm -lquadmath -pthread -lz3

INC =-I/usr/rose/include/rose -Iinclude -I$(Z3PATH)/include

SRC := $(wildcard src/*.cpp)

OBJ := $(patsubst src/%.cpp,%.o,$(SRC))


build: compile link

build-and-clean: build clean

link: $(OBJ)
	$(CXX) $(OBJ) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) -Xlinker -export-dynamic -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-rpath,$(Z3PATH)/bin/ -o FECheck

compile: $(SRC)
	$(CXX) $(CXXFLAGS) $(INC) $(SRC) -c

clean: 
	rm *.o

clean-all:
	rm *.o FECheck

