//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_ReductionChecker_H
#define PEC_ReductionChecker_H

#include "DoAllChecker.h"

  class ReductionChecker : public DoAllChecker {
    public:      
      ReductionChecker(bool requireOutputVars) : DoAllChecker(requireOutputVars){}
    protected:
      virtual bool allows_reduction();
      virtual std::string get_pattern_name();
      virtual bool check_parallelized_loop(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, std::set<std::string>& seqVarsForOutput);

    private:
      bool check_assignment(SgInitializedName* var, ReductionOp opType, SgAssignOp* assign);
      bool check_compound_assignment(SgInitializedName* var, ReductionOp opType, SgCompoundAssignOp* assign);
      bool does_not_contain_reduction_variable(SgInitializedName* reductionVar, SgExpression* expr);
      bool is_reduction_variable(SgInitializedName* reductionVar, SgExpression* expr);

   };

#endif
