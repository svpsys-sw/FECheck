//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_EqualityCheckGenerator_H
#define PEC_EqualityCheckGenerator_H

#include "rose.h"
#include "PECHelperManager.h"
#include "TypeManager.h"

enum EqualType {ARRAY, POINTER, SIMPLE, STRUCT, UNSUPPORTED}; // extend if more types need to be distinguished

class EqualityCheckGen {
    public:
      void add_eq_checks(std::vector<std::tuple<std::string, std::string, SgType*>>& varPairs, bool assertPerPair, std::map<std::string, std::string>& pVarNameToSizeVar, TypeManager* pTypeMgr);

    private:
      unsigned int count;
      PECHelperManager helper;
      SgVarRefExp* varEq = SageBuilder::buildVarRefExp(SgName("equal"));
      std::map<std::string, std::string>* varNameToSizeVar;

      void add_array_eq_check(SgExpression* exprS, SgExpression* exprP, SgArrayType* type, TypeManager* pTypeMgr);
      void add_assert();
      void add_equal_block(SgExpression* exprS, SgExpression* exprP, SgType* type, TypeManager* pTypeMgr);
      void add_equal_init();
      void add_equality_expr(SgExpression* lhs, SgExpression* rhs);
      void add_equality_ptr(SgExpression* varS, SgExpression* varP, SgType* type, TypeManager* pTypeMgr);
      void add_equality_struct(SgExpression* exprS, SgExpression* exprP, SgType* type, TypeManager* pTypeMgr);
      EqualType get_eq_type(SgType* varType, TypeManager* pTypeMgr);
      bool is_simple_eq_type(SgType* varType);
      SgType* unwrapType(SgType* type);

  };
#endif
