//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_DoAllChecker_H
#define PEC_DoAllChecker_H

#include "AbstractChecker.h"
#include "VariableAttributeClassification.h"
#include "PECHelperManager.h"
#include"z3++.h"

  class DoAllChecker : public AbstractChecker {
    public:
      DoAllChecker(bool requireOutputVars) : AbstractChecker(requireOutputVars){}
      void perform_pattern_checks(SgSourceFile* seqProg, SgSourceFile* parProg);

    protected:
      PECHelperManager helper;

      virtual bool allows_reduction();
      virtual bool check_parallelized_loop(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, std::set<std::string>& seqVarsForOutput);
      bool contains_unallowed_loop_exits(SgForStatement* forStmt);
      virtual std::string get_pattern_name();
      bool identical_for_header(SgForStatement* seqFor, SgForStatement* parFor);
      void insert_eq_pragmas(SgStatement* loopBody, int id);
      bool is_parallelized_for(std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& seqSeg, std::pair<SgPragmaDeclaration*, SgPragmaDeclaration*>& parSeg, SgForStatement** parFor, std::vector<SgPragma*>& omp_pragmas);
      bool is_syntactically_equal(SgStatement* seqBody, SgStatement* parBody);
      
    private:
      std::set<std::string> pure_thread_safe_funs = {"abs", "cexp", "conj", "cos", "div", "exp", "fabs", "pow", "sin", "sqrt"};

      bool acceptable_external_function(std::string& funName);
      void analyze_function_calls(SgStatement* stmt, std::map<SgInitializedName*, VariableAttributeClassification>& vars, std::map<std::string, std::set<int>>& seenFun, bool* globalModFun, bool firstCall, DefUseAnalysis* defuse, bool isParallelized);
      void analyze_vars(std::vector<SgVarRefExp*>& varRefs, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized, SgForStatement* forStmt, DefUseAnalysis* defuse, LivenessAnalysis* liveA);
      bool are_array_accesses_independent(std::vector<SgPntrArrRefExp*>& varArrIndices, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgForStatement* parFor);
      bool are_iterations_independent(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar);
      SgForStatement* check_do_all_parallelization_and_return_for(SgStatement* startOfSegment, SgStatement* endSegmentPragma, std::vector<SgPragma*>& omp_pragmas);
      bool checkIndependenceOfIdenticalIndices(SgExpression* index, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter);
      bool checkIndependenceOfIndexExpressions(SgExpression* index1, SgExpression* index2, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter, SgForStatement* parFor);
      void collect_called_functions(SgStatement* block, std::map<std::string, SgBasicBlock*>& collectionInfo);
      void collectIndices(SgForStatement* parFor, std::map<SgInitializedName*, std::vector<SgPntrArrRefExp*>>& allArrIndices);
      bool consider_live_vars(std::map<SgInitializedName*, VariableAttributeClassification>& vars, std::vector<SgInitializedName*>& liveVars, SgInitializedName* loop_counter);
      ReductionOp convert_to_reduction_operator(std::string& op);
      unsigned int count_reference_depth(SgPntrArrRefExp* arr);
      LoopType detect_loop_type(SgExpression* incrStmt, SgInitializedName* loop_counter);
      void determine_data_sharing_from_pragma(SgPragma* omp_pragma, std::map<SgInitializedName*, VariableAttributeClassification>& nameToClass, std::map<std::string, SgInitializedName*>& nameToDecl);
      void determine_default_data_sharing(SgForStatement* parFor, std::map<SgInitializedName*, VariableAttributeClassification>& nameToClass, SgInitializedName* var);
      bool differentAccessesPerIteration(SgPntrArrRefExp* arr, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, SgInitializedName* loop_counter);
      bool ensures_same_input(VariableAttributeClassification& varClass);
      bool ensures_same_output(SgInitializedName* varName, VariableAttributeClassification& varClassPar, bool seqRequiresOutput);
      int get_next_elem_pos(int startPos, std::string& pragma);
      int get_next_elem_end_pos(int start, std::string& pragma);
      int get_param_index(SgInitializedName* decl, std::vector<SgInitializedName*>& params);
      void insert_all_global_vars(SgGlobal* global_scope, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized);
      bool inspect_loop_code(SgForStatement* forStmt, SgPragmaDeclaration* end, std::map<SgInitializedName*, VariableAttributeClassification>& vars, bool isParallelized);
      void inspect_pointer_usage(SgForStatement* parFor, std::map<SgInitializedName*, bool>& onlyArrayRefs);
      bool is_fixed_loop_iteration(SgForStatement* forStmt, std::map<SgInitializedName*, VariableAttributeClassification> vars);
      bool is_omp_pragma_free(SgForStatement* forLoop);
      bool is_omp_pragma_free(SgNode* stmt, std::set<std::string>& seenFun);
      char is_positive_number(SgExpression* expr);
      bool is_proper_parallelization_setup(SgForStatement* seqFor, SgPragmaDeclaration* seqEnd, SgForStatement* parFor, SgPragmaDeclaration* parEnd, std::vector<SgPragma*>& omp_pragmas);
      z3::expr translateIntoZ3BoolExpression(z3::context& ctx, SgInitializedName* loop_counter, SgExpression* codeExpr, bool rename);
      z3::expr translateIntoZ3Expression(z3::context& ctx, SgExpression* codeExpr, SgInitializedName* loop_counter, std::map<SgInitializedName*, VariableAttributeClassification>& varsPar, bool renameIfModified);
      z3::expr translateIntoZ3ValExpression(z3::context& ctx, SgValueExp* codeExpr);
      bool useOfDefinitionAfterRedefinedEachIteration(SgInitializedName* var, SgNode* definition, SgStatement* loop_body_pattern, std::set<SgInitializedName*>& liveAtIterationStart);
      bool useOfInitializedForLoopCounter(SgVarRefExp* varRef, SgStatement* loop_body_pattern);
   };

#endif
