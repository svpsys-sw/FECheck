//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_IncludeManager_H
#define PEC_IncludeManager_H

#include <string>
#include "rose.h"

class IncludeManager {
    public:
      bool contains_complex();
      bool contains_math();
      bool contains_stdlib();
      void include_assert();
      void include_complex();
      void include_stdio();
      void include_stdlib();
      void include_string();
      void insert_includes(SgSourceFile* file);
      void get_includes_in(SgSourceFile* file);
      void reset();

    private:
      std::map<std::string, bool> includeInfos;
      std::vector<std::string> includeOrder;

      bool contains_include(std::string name);
      void filter_includes(std::vector<PreprocessingInfo* >& incl_comm);
      void insert_system(std::string name);
      void insert_include(std::string name, bool isSystem);

  };
#endif
