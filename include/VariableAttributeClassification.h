//SPDX-License-Identifier: Apache-2.0

/*
Copyright 2021 Marie-Christine Jakobs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef PEC_VariableAttributeClassification_H
#define PEC_VariableAttributeClassification_H

#include "PECConstants.h"
#include "VariableClassification.h"

class VariableAttributeClassification : public VariableClassification {
    public:
      ReductionOp get_reduction_operation();
      SharingType get_sharing_type();
      bool is_accessed_in_called_function();
      bool is_loop_counter();
      LoopType get_loop_type();
      void mark_as_loop_counter(LoopType pLoopType);
      void set_access_in_called_function();
      void set_reduction_op(ReductionOp op);
/* A list item may not appear in more than one clause on the same directive, except that it may be specified in both firstprivate and lastprivate clauses.*/
      void set_sharing_type(SharingType newSharing);
      void set_used();

    
    private:
      bool is_counter = false;
      bool isRead = false;
      bool accessInCalledFunction = false;
      SharingType sharingAttr = UNSET;
      ReductionOp red_op = INVALID;
      LoopType loopType = UNDETERMINED;

  };
#endif
